enum A {}

enum B {
    Unit,
    EmptyTuple(),
    FullTuple(i32),
    MupltipleTuple(i32, String),
    EmptyStruct {},
    FullStruct { a: i32 },
    MultipleStruct {
        a: i32,
        b: String,
    },
    StructWithPath { a: foo::bar::Baz },
    SimpleDiscriminant = 13,
    ComplexDiscriminant = const_fn(13),
}

enum C {
    A
}

