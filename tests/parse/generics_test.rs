fn my_fun1<'a, T>(y: T) {
    printf();
}

fn my_fun2<const X: usize>(y: X) {
    printf();
}

fn my_fun3<A, B>(x: A, y: B) {
    printf();
}

trait MyTrait1<'a, T> {
    fn fun1(x: T);
}

trait MyTrait2<A, B> {
    fn fun1(x: A, y: B);
}

trait MyTrait3<const X: usize> {
    fn fun1(y: X);
}

struct MyStruct1<'a, T> {
    x: T,
}

struct MyStruct2<A, B> {
    x: A,
    y: B,
}

struct MyStruct3<const X: usize> {
    y: X,
}

struct MyTuple1<'a, T>(T);

struct MyTuple2<A, B>(A, B);

struct MyTuple3<const X: usize>(X);
