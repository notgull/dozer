// exitcode:1

fn do_nothing() {}

fn do_something(x: i64) {
    add_two_numbers(1, 2);
}

fn rust_main() -> i32 {
    let two = 2i32;
    let my_str = unsafe {
        let foo = two;
        foo;

        c"abcd🐬\" \u{1b}[31m é 举 \x1b[0m"

        // b"hello world: \x55\nyay\0"

    };
    unsafe {
        puts(my_str);
    };
    let res = two.sub(1);

    let one_byte = 'a'; // 97
    let two_bytes = 'é'; // 233
    let three_bytes = '举'; // 20030
    let four_bytes_expected = '\u{1f42c}';
    let four_bytes = '🐬'; // 128044

    res
}

impl i32 {
    fn add(this: i32, other: i32) -> i32 {
        this + other
    }

    fn sub(self, other: i32) -> i32 {
        self - other
    }
}

extern "C" {
    fn puts(data: *const u8);
}

fn add_two_numbers(x: i32, y: i32) -> i32 {
    x + y
}
