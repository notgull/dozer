#!/bin/sh

set -eux

./dozer "$1" /tmp/out.nhad --crate-name=compiler_test
./dozer-qbe /tmp/out.nhad > /tmp/out.qbe
"${QBE:-qbe}" /tmp/out.qbe > /tmp/out.S
"${CC:-cc}" /tmp/out.S ./objs/libdozerrt.a -o /tmp/out

set +ex
/tmp/out
echo $?

