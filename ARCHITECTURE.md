# dozer Architecture

This document contains a high-level explanation of `dozer`'s current
architecture, in order to make it easier for new contributors to familiarize
themselves with `dozer`.

## Files

- `parser/` - Lexer and parser.
- `parser/parser.h` - Public API for parser.
- `json/` - [`rust2json`](https://github.com/taiki-e/syn-serde/tree/v0.2.4/examples/rust2json) compatible output of AST.
- `src/dozer.h` - High level compiler function declarations.
- `src/util/` - Simple utility functions and flexible-sized arrays.
- `src/map.c` - Hash map implemntation.
- `src/main.c` - Entry point for the `dozer` executable; the top-level compiler.
- `src/typemap.c` - Translate types are parsed by the parser into concrete
  types.
- `src/traverse.c` - Utility functions for traversing the AST.
- `src/scope.c` - Scope that contains all active items and is able to resolve
  paths.
- `src/resolve.c` - Resolve parsed expressions to typed nodes.
- `src/dir.h`, `src/dir.c` - Describes Dozer IR, a bytecode format.
- `src/emit.c` - Traverse the AST and emit metadata, as well as Dozer IR.
- `nhad` - Emits labeled metadata in the form of the NHAD format (Notgull
  Has All the Data).
- `src/qbe.c` - Entry point for `dozer-qbe`, translates Dozer IR to QBE IR.
- `src/dozerrt/` - `libdozerrt`, a small runtime for programs compiled with `dozer`.

## Code Style

Publicly exported types and functions are prefixed by a special code identifying
which file they are from. For example, types/functions originating from the
`src/lex.c` file have the prefix `L_`. After the prefix, types are written in
`CamelCase` and functions are written in `snake_case`. This way we can
immediately tell that `L_Lexer` is a type and `L_lex` is a function.

Code is autoformatted with `clang-format`, using the `--style=Chromium` preset.

## Phases

### Lexing

Lexing Rust code is relatively trivial and can be accomplished without any
special lexing tools. See `src/lex.c`, which is less than 1 kLOC and can handle
pretty much the entire Rust token grammar. It also supports UTF-8, although
there are currently bugs with multi-character codepoints (see [#3](https://codeberg.org/notgull/dozer/issues/3)).

After creating an `L_Lexer`, we can call `L_lex` to advance and get the next
token.

### Parsing

Parsing is significantly less trivial. The parser is split among several files,
and operates entirely on tokens. `src/parser.c` provides the `P_Parser` type,
which is a handy interface for operating on tokens.

The current parser is heavily derived from the [`syn`] crate. It creates an AST
which is later used to define the program structure.

[`syn`]: https://crates.io/crates/syn

### Macro/Module/Attribute Expansion

*TODO: We haven't started here yet.*

### Resolution

*TODO: Expand this section once resolution does more than basic typechecking.*

### Emission

Once we have resolved all of our items and expressions, we can now start
emitting the final bytecode.

All metadata is bundled into a filetype named NHAD, for **N**otgull **H**as
**A**ll the **D**ata. It is vaguely based on the "WAD" format used by the DOOM
engine, but with modifications for storing larger files with longer filenames.
It consists of several differentiated blobs of data, each given a name of
arbitrary length.

Bytecode takes the format of Dozer Intermediate Representation, or DIR. DIR is
intended to be very simple to both translate to QBE IR, and to be processed by a
VM as part of `const` resolution. Expressions are translated down into DIR by
`src/emit.c`.

The following lumps are emitted by Dozer:

- `STR_<id>` - String literals.
- `FN_<function name>` - Represents a function. It consists of signature
  information followed by the DIR code.

### QBE Translation

This part is relatively straight-forwards and consists of translating DIR to QBE
IR. It uses `libdozerrt`, which means most operations consist of some form of
dynamic dispatch. Every expression is represented as a `DZ_Object`.

At the moment we don't support cross-compilation. This could probably be pretty
trivially added, but doesn't offer much benefit.

## Anti-Architecture

Rejected ideas and why they were rejected.

### miniyacc

[`miniyacc`] is a tiny Yacc implementation written in pure C. It can be compiled
with both TinyCC and `cproc`, just like QBE. If we could use [`miniyacc`] in our
parser, we could potentially simplify a significant amount of our parsing code.

However, as far as I know Rust cannot be parsed with an unambiguous LALR(1)
grammar, which is a requirement for [`miniyacc`]. This is because Rust is not
a context-free language. In addition [`miniyacc`] lacks many of the features
included in modern Yacc implementations like Bison.

At the moment the hand written parser works well enough, even if it is rather
verbose. I would accept a merge request porting the parser to Yacc, as long as
it does all of the following:

- It needs to handle macros. Unlike in C (which Yacc is pretty much designed
  for), macros in Rust are handled at the parser level rather than the token
  level. If I recall correctly this is what keeps Rust from being a context-free
  grammar.
- It needs to be able to be compiled with [`miniyacc`]. Bison requires
  autotools, `flex`, and a convoluted [bootstrap](https://gitlab.com/giomasce/bison-bootstrap)
  process.
- The resulting parser has fewer lines of code than the current parser.

[`miniyacc`]: https://c9x.me/yacc/


