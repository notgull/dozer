#!/bin/sh

# This file is part of Dozer.
#
# Dozer is free software; you can redistribute it and/or modify it
# under the terms of one of the following licenses:
#
# - The MIT License
# - The Apache License, version 2.0
#
# Dozer is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
# License for more details.
#
# You should have received a copy of the MIT License and the Apache
# License 2.0 along with Dozer. If not, see:
#
# - https://opensource.org/license/mit
# - https://www.apache.org/licenses/LICENSE-2.0

set -eux

rm dozer
rm nhaddump
rm dozer-qbe
rm objs/*.o
rm objs/*.a

