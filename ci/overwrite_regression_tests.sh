#!/usr/bin/env bash

set -euEo pipefail
cd "$(dirname -- "$(realpath "$0")")/.."

# shellcheck disable=SC2154
trap 's=$?; echo >&2 "$0: error on line "${LINENO}": ${BASH_COMMAND}"; exit ${s}' ERR

for i in ./tests/parse/*.rs; do
    ./dozer $i parse > $i.txt
done

