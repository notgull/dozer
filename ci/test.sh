#!/bin/bash

set -euEo pipefail
cd "$(dirname -- "$(realpath "$0")")/.."

# shellcheck disable=SC2154
trap 's=$?; echo >&2 "$0: error on line "${LINENO}": ${BASH_COMMAND}"; exit ${s}' ERR

rx() {
  local cmd="$1"
  shift
  (
    set -x
    "$cmd" "$@"
  )
}
compile() {
  ./dozer "$1" /tmp/out.nhad
  ./dozer-qbe /tmp/out.nhad > /tmp/out.qbe
  qbe /tmp/out.qbe > /tmp/out.S
  "${CC:-cc}" /tmp/out.S ./objs/libdozerrt.a -o /tmp/out
}

# Test parsing on UI tests.
for parse_test in $(find . -name '*.rs' -type f); do
  ret=0

  if grep -q "$parse_test" ./tests/blocklist.txt; then
      continue
  fi

  OUTPUT=$(./dozer "$parse_test" parse || ret=$?)
  if [ $ret -eq 0 ]; then
    if echo -n "${OUTPUT}" | cmp "$parse_test.json" - > /dev/null 2>&1; then
        echo ">> $parse_test OK"
    else
        echo ">> $parse_test FAILED COMPARISON"
        failed=1
        echo "$parse_test" >> failed_comparison.txt
    fi
  else
    echo ">> $parse_test FAILED PARSING"
    echo "$parse_test" >> failed_parsing.txt
    failed=1
  fi
done

# Test compilation.
for compile_test in ./tests/compile/*.rs; do
  exitcode="$(grep 'exitcode:' "$compile_test" | cut -d: -f2)"
  compile "$compile_test"
  realcode="$({ sh -c '/tmp/out; echo $?'; } | tail -n 1)"

  if [ "$realcode" -eq "$exitcode" ]; then
    echo ">> $compile_test is OK"
  else
    echo ">> $compile_test in FAILED"
    failed=1
  fi
done

if [ -n "${failed:-}" ]; then
  echo ">> encountered failures above"
  exit 1
fi

