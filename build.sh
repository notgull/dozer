#!/bin/sh

# This file is part of Dozer.
#
# Dozer is free software; you can redistribute it and/or modify it
# under the terms of one of the following licenses:
#
# - The MIT License
# - The Apache License, version 2.0
#
# Dozer is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
# License for more details.
#
# You should have received a copy of the MIT License and the Apache
# License 2.0 along with Dozer. If not, see:
#
# - https://opensource.org/license/mit
# - https://www.apache.org/licenses/LICENSE-2.0

set -eu

cc=${CC:-cc}
cflags="-g -D_GCC_MAX_ALIGN_T"
mkdir -p objs/parser/lexer
mkdir -p objs/dozerrt
mkdir -p objs/json
mkdir -p objs/nhad
mkdir -p objs/util

set -x

# Compile object files.
"$cc" -Isrc $cflags -c src/util/byteops.c -o objs/util/byteops.o
"$cc" -Isrc $cflags -c src/dir.c -o objs/dir.o
"$cc" -Isrc $cflags -c src/emit.c -o objs/emit.o
"$cc" -Isrc $cflags -c src/parser/expr.c -o objs/parser/expr.o
"$cc" -Isrc $cflags -c src/global.c -o objs/global.o
"$cc" -Isrc $cflags -c src/parser/item.c -o objs/parser/item.o
"$cc" -Isrc $cflags -c src/parser/lexer/lex.c -o objs/parser/lexer/lex.o
"$cc" -Isrc $cflags -c src/dozerrt/libdozer.c -o objs/dozerrt/libdozer.o
"$cc" -Isrc $cflags -c src/main.c -o objs/main.o
"$cc" -Isrc $cflags -c src/util/map.c -o objs/util/map.o
"$cc" -Isrc $cflags -c src/nhad/nhad.c -o objs/nhad/nhad.o
"$cc" -Isrc $cflags -c src/nhad/nhaddump.c -o objs/nhad/nhaddump.o
"$cc" -Isrc $cflags -c src/parser/parser.c -o objs/parser/parser.o
"$cc" -Isrc $cflags -c src/parser/pattern.c -o objs/parser/pattern.o
"$cc" -Isrc $cflags -c src/parser/path.c -o objs/parser/path.o
"$cc" -Isrc $cflags -c src/qbe.c -o objs/qbe.o
"$cc" -Isrc $cflags -c src/parser/resolve.c -o objs/parser/resolve.o
"$cc" -Isrc $cflags -c src/parser/stmt.c -o objs/parser/stmt.o
"$cc" -Isrc $cflags -c src/parser/lexer/token.c -o objs/parser/lexer/token.o
"$cc" -Isrc $cflags -c src/traverse.c -o objs/traverse.o
"$cc" -Isrc $cflags -c src/parser/type.c -o objs/parser/type.o
"$cc" -Isrc $cflags -c src/util/util.c -o objs/util/util.o
"$cc" -Isrc $cflags -c src/json/json_dump.c -o objs/json/json_dump.o

# Link it all together.
"$cc" objs/util/byteops.o objs/dir.o objs/emit.o objs/parser/expr.o objs/global.o objs/parser/item.o \
  objs/parser/lexer/lex.o objs/dozerrt/libdozer.o objs/main.o objs/util/map.o objs/nhad/nhad.o objs/parser/pattern.o \
  objs/parser/path.o objs/parser/parser.o objs/parser/resolve.o objs/parser/stmt.o objs/parser/lexer/token.o objs/traverse.o \
  objs/parser/type.o objs/util/util.o objs/json/json_dump.o -o dozer
"$cc" objs/util/byteops.o objs/dir.o objs/global.o objs/util/map.o objs/nhad/nhad.o objs/parser/path.o \
  objs/qbe.o objs/parser/parser.o objs/parser/lexer/lex.o objs/parser/lexer/token.o objs/parser/item.o objs/parser/type.o objs/parser/pattern.o \
  objs/parser/stmt.o objs/parser/expr.o objs/traverse.o objs/util/util.o -o dozer-qbe
"$cc" objs/nhad/nhaddump.o objs/nhad/nhad.o objs/util/util.o -o nhaddump

test -f objs/libdozerrt.a && rm objs/libdozerrt.a
ar r objs/libdozerrt.a objs/dozerrt/libdozer.o objs/util/util.o
