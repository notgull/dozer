# dozer

WIP Rust compiler, written in C

## Motivation

The goal is to be able to compile an early version of the Rust compiler,
without needing to go through the following:

- C++; this is the current canonical bootstrap strategy via [`mrustc`]. This way
  Rust can be used as a viable bootstrap language for C++.
  - Note that this means we can't use the LLVM backend, as LLVM is written in
    C++.
- The entire Rust bootstrap chain starting from OCaml in `rustc` v0.7 and going
  forwards from there. This takes a significant amount of time and we would like
  to avoid this.

See [my blogpost](https://notgull.net/announcing-dozer/) announcing Dozer for
more information.

## Goals

As part of this goal `dozer` should be able to be compiled with very simple C
compilers, like [TinyCC] and [`cproc`]. Care should be taken to avoid more
complicated GNU extensions.

Eventually I would like the compiler to be able to compile an earlier version of
`rustc`. I hope that it is possible to compile a version of `rustc` that
supports the [Cranelift backend], but this may be infeasible with a simple
compiler. So for now we are targeting `rustc` v1.0.

## Build

The build process for `dozer` aims to be very simple. We don't even use a
Makefile, just to open the door to the possibility that `make` could be
implemented in Rust. We only assume that we have a C compiler (which depends on
an assembler, archiver and linker) as well as a very basic scripting tool (at
the moment we are targeting [`kaem`]).

Thankfully [`kaem`] scripts are POSIX-shell compatible, so we can just use
`bash` to run the build script.

```
$ bash build.sh
```

This script builds three important targets:

- `dozer`, the bootstrap compiler.
- `dozer-qbe`, the QBE backend for Dozer's intermediate language.
- `libdozerrt.a`, a small runtime that is linked into programs compiled with
  Dozer.

## Running

*Note: dozer is still in an early state so the command line interface is heavily
subject to change.*

To compile a Rust program, call Dozer with the path to the root Rust module and
the path to the output file.

```
$ ./dozer hello.rs hello.nhad
```

The "nhad" file contains metadata and bytecode for the compiled program. Use
`dozer-qbe` to strip metadata and translate the bytecode to QBE IR.

```
$ ./dozer-qbe hello.nhad > hello.qbe
```

At this point [QBE] and the system C compiler can be used to translate the QBE
IR to an executable. Make sure that `libdozerrt.a` is linked into the final
executable.

```
$ qbe hello.qbe > hello.S
$ cc hello.S ./src/libdozerrt.a -o hello
```

[QBE]: https://c9x.me/compile/
[`mrustc`]: https://github.com/thepowersgang/mrustc
[TinyCC]: https://en.wikipedia.org/wiki/Tiny_C_Compiler
[`cproc`]: https://sr.ht/~mcf/cproc
[Cranelift backend]: https://github.com/rust-lang/rustc_codegen_cranelift
[`kaem`]: https://github.com/oriansj/mescc-tools/tree/master/Kaem

## Things left to do

- The parser is still incomplete. Expanding the parser is probably the
  lowest-hanging fruit for new contributors.
- We only support typechecking and emitting basic `i32` operations. We need to
  be able to support more of Rust.
  - My current plan is to "eat the frog" and get trait resolution working as
    soon as possible, as most of Rust's language features require traits to
    work.
- The rest of the owl.

## License

MIT/Apache2

