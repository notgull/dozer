// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

/*
NHAD - Notgull Has All the Data

A data format inspired by DOOM's WAD format. Used to store data in a
platform-independent way. Format is as follows:

- First four bytes is "NHAD".
- Next four bytes are reserved.
- Next eight bytes is the total number of lumps (LE).
- Next eight bytes is the index into the file that the NHADDIR is (LE).

Each lump is specified in the NHADDIR. Each lump consists of these fields.

- First eight bytes is the file index of the lump (LE).
- Next eight bytes is the size of the lump (LE).
- Next eight bytes is the length of the name (LE).
- Next [N padded to multiple of eight] bytes is the name.

Lumps should be multiples of eight, but this is not forced by the format.

(For the record, this was written without referencing DOOM's source code.)

This data format is used as Dozer's "object" format. After a crate is compiled,
it creates an NHAD file containing crate metadata, macro definitions and
low-level bytecode for each function. It is then fed into "dozer-qbe" to
translate the bytecodes down to QBE IR.
*/

#ifndef DOZER_NHAD_H
#define DOZER_NHAD_H

#include <stddef.h>
#include <stdint.h>

typedef struct lump N_Lump;
typedef struct nhad N_Nhad;

struct lump {
  uint64_t file_pos;
  uint64_t size;
  char* name;
  void* data;
};

N_Nhad* N_new();
N_Nhad* N_open(const char* filename);
N_Lump* N_entry(N_Nhad* nhad, const char* name, const char* label);
void* N_load(N_Nhad* nhad, const N_Lump* lump);
void N_push(N_Nhad* nhad, const char* name, void* buf, size_t len);
void N_write(N_Nhad* nhad, const char* filename);

size_t N_lumpcount(const N_Nhad* nhad);
const N_Lump* N_lumpat(const N_Nhad* nhad, size_t index);

void N_free(N_Nhad* nhad);
void N_dump(N_Nhad* nhad);

#endif
