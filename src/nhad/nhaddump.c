#include <stdio.h>

#include "nhad.h"

extern char* program_name;

int main(int argc, char** argv) {
  N_Nhad* input;

  program_name = argv[0];
  if (!program_name)
    program_name = "nhaddump";

  if (argc < 2) {
    fprintf(stderr, "usage: %s [file.nhad]\n", program_name);
    return 1;
  }

  input = N_open(argv[1]);
  N_dump(input);
  N_free(input);

  return 0;
}
