// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

#include "nhad.h"
#include "util/util.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct nhad {
  FILE* file;
  N_Lump* lumps;
};

static uint64_t read_u64_le(FILE* file, const char* filename) {
  uint64_t x;

  if (fread(&x, 1, 8, file) != 8)
    U_die("fread %s:", filename);

  if (U_bigendian())
    // Byteswap the number.
    U_byteswap64(&x);

  return x;
}

static uint64_t pad_to_8(uint64_t in) {
  return ((in + 7) & ~7) - in;
}

N_Nhad* N_new() {
  N_Nhad* nhad = U_xmalloc(sizeof(*nhad));

  nhad->file = NULL;
  nhad->lumps = U_mkarray(NULL, 16, sizeof(*nhad->lumps));

  return nhad;
}

N_Nhad* N_open(const char* filename) {
  N_Nhad* nhad = N_new();
  uint8_t magic[4];
  uint64_t lump_count, nhaddir, namelen, i, j;
  N_Lump lump;
  char* name;

  nhad->file = fopen(filename, "rb");
  if (!nhad->file)
    U_die("fopen %s:", filename);

  // Read in the magic.
  if (fread(magic, 1, sizeof(magic), nhad->file) != sizeof(magic))
    U_die("fread %s:", filename);
  if (magic[0] != 'N' && magic[1] != 'H' && magic[2] != 'A' && magic[3] != 'D')
    U_die("file %s does not have the magic NHAD magic");

  // Skip four bytes.
  if (fseek(nhad->file, 4, SEEK_CUR) != 0)
    U_die("fseek %s:", filename);

  // Read in other values.
  lump_count = read_u64_le(nhad->file, filename);
  nhaddir = read_u64_le(nhad->file, filename);

  // Read the NHADDIR.
  if (fseek(nhad->file, nhaddir, SEEK_SET) != 0)
    U_die("fseek %s:", filename);
  for (i = 0; i < lump_count; i++) {
    lump.file_pos = read_u64_le(nhad->file, filename);
    lump.size = read_u64_le(nhad->file, filename);
    namelen = read_u64_le(nhad->file, filename);

    lump.name = U_xmalloc(namelen + 1);
    if (fread(lump.name, 1, namelen, nhad->file) != namelen)
      U_die("fseek %s:", filename);
    lump.name[namelen] = '\0';
    lump.data = NULL;

    // Pad to an integer of four.
    for (j = 0; j < pad_to_8(namelen); j++)
      fgetc(nhad->file);

    U_addmem(&nhad->lumps, &lump, 1);
  }

  return nhad;
}

N_Lump* N_entry(N_Nhad* nhad, const char* name, const char* label) {
  bool seen_label = label == NULL;
  size_t i;
  N_Lump* lump;

  // TODO: A hash-based algorithm would be better here.
  for (i = 0; i < U_len(nhad->lumps); i += 1) {
    lump = &nhad->lumps[i];
    if (seen_label && strcmp(name, lump->name) == 0)
      return lump;
    else if (label && strcmp(label, lump->name) == 0)
      seen_label = true;
  }

  return NULL;
}

void* N_load(N_Nhad* nhad, const N_Lump* lump) {
  void* buf = U_xmalloc(lump->size);

  if (lump->data) {
    memcpy(buf, lump->data, lump->size);
    return buf;
  }

  if (!nhad->file)
    U_die("tried to load file lump from an in-memory NHAD");

  if (fseek(nhad->file, lump->file_pos, SEEK_SET) != 0)
    U_die("fseek:");
  if (fread(buf, 1, lump->size, nhad->file) != lump->size)
    U_die("fread:");

  return buf;
}

void N_push(N_Nhad* nhad, const char* name, void* buf, size_t len) {
  N_Lump entry;

  entry.name = strdup(name);
  entry.file_pos = 0;
  entry.size = len;
  entry.data = U_xmalloc(len);

  memcpy(entry.data, buf, len);
  U_addmem(&nhad->lumps, &entry, 1);
}

static void write_u64_le(FILE* file, const char* filename, uint64_t x) {
  if (U_bigendian())
    // Byteswap the number.
    U_byteswap64(&x);

  if (fwrite(&x, 1, 8, file) != 8)
    U_die("fwrite %s:", filename);
}

static N_Lump* entry_at(const N_Nhad* nhad, size_t i) {
  return &nhad->lumps[i];
}

void N_write(N_Nhad* nhad, const char* filename) {
  FILE* out;
  N_Lump* entry;
  uint64_t i, j;
  void* buffer;
  long filepos, nhaddir_pos;

  out = fopen(filename, "wb");
  if (!out)
    U_die("fopen %s:", filename);

  if (fwrite("NHAD1234", 1, strlen("NHAD1234"), out) != strlen("NHAD1234"))
    U_die("fwrite %s:", filename);

  write_u64_le(out, filename, U_len(nhad->lumps));
  // We will fill this in later.
  write_u64_le(out, filename, 0);

  // Begin writing entries.
  for (i = 0; i < U_len(nhad->lumps); i += 1) {
    entry = entry_at(nhad, i);

    filepos = ftell(out);
    if (filepos == -1L)
      U_die("ftell %s:", filename);

    buffer = N_load(nhad, entry);
    if (!buffer)
      U_die("failed to load entry for lump %s", entry->name);
    entry->file_pos = filepos;
    if (fwrite(buffer, 1, entry->size, out) != entry->size)
      U_die("fwrite %s:", filename);

    free(buffer);

    // Pad to eight bytes.
    for (j = 0; j < pad_to_8(filepos + entry->size); j++)
      fputc(0, out);
  }

  // Begin writing the NHADDIR.
  nhaddir_pos = ftell(out);
  if (nhaddir_pos == -1L)
    U_die("ftell %s:", filename);

  for (i = 0; i < U_len(nhad->lumps); i += 1) {
    entry = entry_at(nhad, i);
    write_u64_le(out, filename, entry->file_pos);
    write_u64_le(out, filename, entry->size);
    write_u64_le(out, filename, strlen(entry->name));

    if (fwrite(entry->name, 1, strlen(entry->name), out) != strlen(entry->name))
      U_die("fwrite %s:", filename);

    // Pad to eight bytes.
    for (j = 0; j < pad_to_8(strlen(entry->name)); j++)
      fputc(0, out);
  }

  // Write the position of the NHADDIR back at the start.
  if (fseek(out, 16, SEEK_SET) != 0)
    U_die("fseek %s:", filename);
  write_u64_le(out, filename, nhaddir_pos);
  fclose(out);
}

size_t N_lumpcount(const N_Nhad* nhad) {
  return U_len(nhad->lumps);
}

const N_Lump* N_lumpat(const N_Nhad* nhad, size_t index) {
  if (index > U_len(nhad->lumps))
    U_die("index %d out of range", index);
  return entry_at(nhad, index);
}

void N_free(N_Nhad* nhad) {
  N_Lump* lump;
  size_t i;

  if (nhad->file)
    fclose(nhad->file);

  for (i = 0; i < U_len(nhad->lumps); i += 1) {
    lump = entry_at(nhad, i);
    free(lump->name);
    if (lump->data)
      free(lump->data);
  }

  free(nhad);
}

static void hexdump(const uint8_t* data, size_t len) {
  size_t i, j;
  char ascii[17];
  bool end_of_data;

  ascii[16] = '\0';

  for (i = 0; i < len; i++) {
    printf("%02X ", data[i]);
    if (data[i] >= ' ' && data[i] <= '~')
      ascii[i % 16] = data[i];
    else
      ascii[i % 16] = '.';

    end_of_data = i + 1 == len;
    if ((i + 1) % 8 == 0 || end_of_data) {
      printf(" ");

      if (end_of_data) {
        ascii[(i + 1) % 16] = '\0';
        if ((i + 1) % 16 <= 8)
          printf(" ");
        for (j = (i + 1) % 16; j < 16; j++)
          printf("   ");
      }

      if ((i + 1) % 16 == 0 || end_of_data) {
        printf("|  %s \n", ascii);
      }
    }
  }
}

void N_dump(N_Nhad* input) {
  size_t i;
  const N_Lump* lump;

  for (i = 0; i < N_lumpcount(input); i++) {
    lump = N_lumpat(input, i);
    printf("%s\n", lump->name);
    puts(
        "======================================================================"
        "=====");
    hexdump(N_load(input, lump), lump->size);
  }
}
