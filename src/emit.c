// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

#include "dir.h"
#include "dozer.h"
#include "util/util.h"

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static uint64_t counter = 0;

struct emitter {
  N_Nhad* output;

  uint32_t nextreg;
  R_TypeStatus** regtypes;
  R_TypeStatus unit_type;
  R_TypeStatus ptr_type;
};

static uint32_t compile_charlit(const char* lit) {
  if (*lit == 'b')
    lit++;  // skip 'b' prefix
  lit++;    // skip first quote
  uint32_t res;
  U_decode_char((const uint8_t*)lit, &res);
  return res;
}

static void compile_strlit(const char* literal,
                           const char* name,
                           struct emitter* em) {
  size_t n = strlen(literal);
  // motivation: an escape is always
  // bigger than the data it represents
  uint8_t* data = U_xmalloc(n);
  uint8_t* c = data;
  const uint8_t* lit = (const uint8_t*)literal;

  bool is_cstring = false, is_rstring = false, is_bstring = false;
  int sharp_count = 0;  // number of '#' characters around rstring
  uint32_t value;
  size_t read, written;
  int i;

  if (*lit == 'b') {
    lit++;
    is_bstring = true;
  } else if (*lit == 'c') {
    lit++;
    is_cstring = true;
  }
  if (*lit == 'r') {
    lit++;
    is_rstring = true;
    while (*lit == '#') {
      lit++;
      sharp_count += 1;
    }
  }

  if (*lit++ != '"')
    U_die("invalid string");

  if (is_rstring) {
    read = 0;
    while (1) {
      if (lit[read] == '"') {
        read++;
        i = 0;
        while (i < sharp_count && lit[read + i] == '#')
          i++;
        if (i >= sharp_count)
          break;
      } else
        read++;
    }

    read--;  // remove last quote
    memcpy(data, lit, read);
    c += read;
  } else
    while (1) {
      if (*lit == '"')
        break;

      read = U_decode_char((const uint8_t*)lit, &value);

      if (value != UINT32_MAX) {
        if (is_bstring) {
          *c++ = value;
        } else if (is_cstring && read == 4 && lit[0] == '\\' && lit[1] == 'x') {
          *c++ = value;  // \x escape
        } else {
          written = U_encode_char_utf8(value, c);
          c += written;
        }
      }

      lit += read;
    }

  if (is_cstring) {
    *c++ = '\0';
  }

  written = (size_t)c - (size_t)data;
  N_push(em->output, name, data, written);
  U_xfree(data);
}

static void mkprim(R_TypeStatus* status, G_Primitive primitive) {
  status->kind = RT_SPECIFIED;
  status->sym = G_primitive(primitive);
}

static void mkroot(R_TypeStatus* status) {
  R_TypeStatus* current = status;

  while (current->kind == RT_REF)
    current = current->other;

  memcpy(status, current, sizeof(R_TypeStatus));
}

static void assert_pathed(const G_Sym* sym) {
  if (!sym->full_path.segs)
    U_die("symbol %s does not have a full path", sym->name);
}

static void assert_specified(const R_TypeStatus* status) {
  if (status->kind != RT_SPECIFIED)
    U_die("type status was not specified, got %d", status->kind);
  if (!status->sym)
    U_die("uninhabited symbol");
  assert_pathed(status->sym);
}

static void emit_call_opcode(struct emitter* em,
                             E_Expr* expr,
                             E_Expr* receiver,
                             E_Expr** args,
                             uint32_t funcreg,
                             G_Sym* fn_type_sym) {
  DI_Register* regs;
  DI_Code code;

  code.opcode = fn_type_sym->type->fnptr.return_type ? DI_CALLRET : DI_CALL;
  code.args[0].reg = funcreg;
  code.args[1].regs.len = 0;
  code.args[2].reg = expr->reg;

  // Get a list of all of the registers we use to call.
  regs = U_mkarray(NULL, 0, sizeof(*regs));
  if (receiver) {
    U_addmem(&regs, &receiver->reg, 1);
    code.args[1].regs.len++;
  }
  for (; (*args)->kind != E_NONE; args++) {
    U_addmem(&regs, &(*args)->reg, 1);
    code.args[1].regs.len++;
  }
  code.args[1].regs.items = regs;

  DI_writecode(&expr->dir, code);
  U_addptr(&em->regtypes, &expr->status);
}

static void compile_expr(AST_Kind kind, void* ast, void* c) {
  E_Expr *expr = ast, **args;
  struct emitter* em = c;
  DI_Code code;
  DI_Register reg;

  char buffer[128] = {0};

  if (kind != AST_EXPR)
    return;

  B_mkwriter(&expr->dir);
  expr->reg = em->nextreg++;

  switch (expr->kind) {
    case E_EMPTY:
      break;
    case E_LIT:
      // Force resolution to actual types.
      mkroot(&expr->status);
      if (expr->status.kind == RT_INTEGER)
        mkprim(&expr->status, GP_I32);
      else if (expr->status.kind == RT_FLOAT)
        mkprim(&expr->status, GP_F64);
      assert_specified(&expr->status);

      if (expr->lit.kind == EL_BOOL) {
        // Booleans are 1 for true, 0 for false.
        code.opcode = DI_LOAD;
        code.args[0].reg = expr->reg;
        code.args[1].literal = strcmp(expr->lit.lit, "true") == 0;
      } else if (expr->lit.kind == EL_INT) {
        // Compile the integer literal.
        code.opcode = DI_LOAD;
        code.args[0].reg = expr->reg;
        if (expr->status.sym->type->kind != GT_PRIMITIVE)
          U_die("integer literal should always be primitive");
        if (expr->status.sym->type->primitive.is_float)
          U_compile_floatlit(expr->lit.lit, &code.args[1].literal);
        else
          U_compile_intlit(expr->lit.lit, &code.args[1].literal);
      } else if (expr->lit.kind == EL_FLOAT) {
        code.opcode = DI_LOAD;
        code.args[0].reg = expr->reg;
        U_compile_floatlit(expr->lit.lit, &code.args[1].literal);
      } else if (expr->lit.kind == EL_BYTE || expr->lit.kind == EL_CHAR) {
        code.opcode = DI_LOAD;
        code.args[0].reg = expr->reg;
        code.args[1].literal = compile_charlit(expr->lit.lit);
      } else {
        // The only other types of literals are strings.
        // Emit these as raw data lumps.
        snprintf(buffer, sizeof(buffer), "STR_%lu", ++counter);
        compile_strlit(expr->lit.lit, buffer, em);

        // Then, load from the resulting global.
        code.opcode = DI_GLOBAL;
        code.args[0].reg = expr->reg;
        code.args[1].name = buffer;
      }

      DI_writecode(&expr->dir, code);
      U_addptr(&em->regtypes, &expr->status);
      break;
    case E_PAREN:
      // Just replace the register with the one our parenthesized expression
      // used.
      expr->reg = expr->paren.expr->reg;
      em->nextreg--;
      break;
    case E_PATH:
      if (expr->path.kind == PTK_LOCAL) {
        // Replace the register with the one we point to.
        expr->reg = expr->path.local->reg;
        em->nextreg--;
      } else {
        // This loads from an item.
        code.opcode = DI_GLOBAL;
        code.args[0].reg = expr->reg;
        code.args[1].name = (char*)expr->path.sym->obj_name;
        if (!code.args[1].name)
          U_die("failed to read in OBJ name for symbol");
        DI_writecode(&expr->dir, code);
        U_addptr(&em->regtypes, &expr->status);
      }
      break;
    case E_BINARY:
      code.opcode = DI_BINOP;
      code.args[0].reg = expr->binary.left->reg;
      code.args[1].reg = expr->binary.right->reg;
      code.args[2].reg = expr->reg;
      code.args[3].optype = expr->binary.op;
      DI_writecode(&expr->dir, code);
      U_addptr(&em->regtypes, &expr->status);
      break;
    case E_METHOD_CALL:
      // Emit a code to get the symbol to load with.
      code.opcode = DI_GLOBAL;
      reg = expr->reg++;
      code.args[0].reg = reg;
      code.args[1].name = expr->method_call.method_sym->obj_name;
      if (!code.args[1].name)
        U_die("failed to read in OBJ name for symbol");
      DI_writecode(&expr->dir, code);
      U_addptr(&em->regtypes, &em->ptr_type);

      em->nextreg++;
      emit_call_opcode(em, expr, expr->method_call.receiver,
                       expr->method_call.args, reg,
                       expr->method_call.method_sym->fn.fn_type);
      break;
    case E_CALL:
      emit_call_opcode(em, expr, NULL, expr->call.args, expr->call.func->reg,
                       expr->call.func->status.sym);
      break;
    case E_BLOCK:
      if (expr->block.evals_to) {
        expr->reg = expr->block.evals_to->reg;
        em->nextreg--;
      } else {
        code.opcode = DI_LOAD;
        code.args[0].reg = expr->reg;
        code.args[1].literal = 0;
        DI_writecode(&expr->dir, code);
        U_addptr(&em->regtypes, &em->unit_type);
      }
      break;
    default:
      U_die("expr not yet supported: %d\n", expr->kind);
  }
}

struct concatenator {
  B_Writer writer;
};

// Used by AST traverser to concatenate all bytecode into a single lump.
static void concat_bytecode(AST_Kind kind, void* ast, void* c) {
  struct concatenator* cat = c;
  E_Expr* expr;

  if (kind != AST_EXPR)
    return;
  expr = ast;

  B_write(&cat->writer, expr->dir.data, expr->dir.len);
}

#define MAX_EMITNAME 128

static char* item_emitname(I_Item* item) {
  char* emitname = U_xmalloc(MAX_EMITNAME);

  switch (item->kind) {
    case I_FN:
      // TODO: Mangling
      snprintf(emitname, MAX_EMITNAME, "FN_%s", item->sym->obj_name);
      break;
    case I_TRAIT:
      // TODO: Mangling
      snprintf(emitname, MAX_EMITNAME, "TRAIT_%s", item->trait.name);
      break;
  }

  return emitname;
}

static char* impl_item_emitname(I_ImplItem* item) {
  char* emitname = U_xmalloc(MAX_EMITNAME);

  switch (item->kind) {
    case II_FN:
      // TODO: Mangling
      snprintf(emitname, MAX_EMITNAME, "FN_%s", item->sym->obj_name);
      break;
  }

  return emitname;
}

static const G_Sym* type_of_fnarg(const I_FnArg* arg) {
  switch (arg->kind) {
    case FNARG_TYPED:
      return arg->typed.ty->sym;
    case FNARG_RECEIVER:
      // TODO: Make sure receiver type is resolved.
      return arg->receiver.ty->sym;
    default:
      U_die("invalid fnarg");
  }
}

static void compile_fn(N_Nhad* output,
                       const char* name,
                       I_Signature* sig,
                       ST_Stmt* stmts,
                       uint8_t** bytecode,
                       size_t* len) {
  ST_Stmt *stmt, *last;
  struct concatenator cat;
  struct emitter em;
  size_t cursor;
  DI_Code code;
  I_FnArg* arg;
  uint32_t argcount = 0, i;
  R_TypeStatus **tr, *status, *argstatus;

  argstatus = U_mkarray(NULL, 0, sizeof(*argstatus));

  em.nextreg = 0;
  em.regtypes = U_mkarray(NULL, 0, sizeof(*em.regtypes));
  em.output = output;
  em.unit_type.kind = RT_SPECIFIED;
  em.unit_type.sym = G_primitive(GP_UNIT);
  em.ptr_type.kind = RT_SPECIFIED;
  em.ptr_type.sym = G_primitive(GP_USIZE);

  // First X registers are arguments.
  for (arg = sig->fn_args; arg->kind != FNARG_NONE; arg++) {
    argcount++;
    status = U_expand(&argstatus, 1);
    status->kind = RT_SPECIFIED;
    status->sym = (G_Sym*)type_of_fnarg(arg);
    U_addptr(&em.regtypes, status);
  }
  em.nextreg = argcount;

  // Compile every expression.
  for (stmt = stmts; stmt->kind != ST_NONE; stmt++)
    AST_traversestmt(stmt, compile_expr, &em);

  // Emit a return expression if needed.
  last = ST_evalexpr(stmts);

  B_mkwriter(&cat.writer);

  // Add the number of registers this function will use, so we can allocate
  // stack space.
  B_write32(&cat.writer, em.nextreg);
  B_write32(&cat.writer, argcount);
  B_write32(&cat.writer, (uint32_t) !!sig->return_type);

  // Serialize all of the arguments.
  tr = em.regtypes;
  for (i = 0; i < U_len(tr); i++) {
    mkroot(tr[i]);
    assert_specified(tr[i]);
    PT_writepath(&cat.writer, &tr[i]->sym->full_path);
  }

  // Serialize the return type.
  if (sig->return_type) {
    assert_pathed(sig->return_type->sym);
    PT_writepath(&cat.writer, &sig->return_type->sym->full_path);
  }

  // Concatenate all of the bytecodes together.
  for (stmt = stmts; stmt->kind != ST_NONE; stmt++)
    AST_traversestmt(stmt, concat_bytecode, &cat);

  if (last && sig->return_type) {
    code.opcode = DI_RET;
    code.args[0].reg = last->expr.expr->reg;
    DI_writecode(&cat.writer, code);
  } else {
    code.opcode = DI_RETNONE;
    DI_writecode(&cat.writer, code);
  }

  *bytecode = cat.writer.data;
  *len = cat.writer.len;
}

static void compile_item(N_Nhad* output,
                         I_Item* item,
                         uint8_t** bytecode,
                         size_t* len) {
  switch (item->kind) {
    case I_FN:
      compile_fn(output, item->fn.sig.name, &item->fn.sig, item->fn.stmts,
                 bytecode, len);
      break;
    default:
      *bytecode = NULL;
      *len = 0;
  }
}

static void compile_impl_item(N_Nhad* output,
                              I_ImplItem* item,
                              uint8_t** bytecode,
                              size_t* len) {
  switch (item->kind) {
    case II_FN:
      compile_fn(output, item->fn.sig.name, &item->fn.sig, item->fn.stmts,
                 bytecode, len);
      break;
    default:
      *bytecode = NULL;
      *len = 0;
  }
}

struct visitor {
  N_Nhad* output;
};

// Compile an item into one or more lumps.
static void visit(AST_Kind kind, void* ast, void* c) {
  uint8_t* data;
  size_t len;
  I_Item* item;
  I_ImplItem* ii;
  char* emitname = NULL;
  N_Nhad* output = c;

  switch (kind) {
    case AST_ITEM:
      item = ast;
      emitname = item_emitname(item);
      compile_item(output, item, &data, &len);
      if (len != 0) {
        N_push(output, emitname, data, len);
      }
      break;
    case AST_IMPL_ITEM:
      ii = ast;
      emitname = impl_item_emitname(ii);
      compile_impl_item(output, ii, &data, &len);
      N_push(output, emitname, data, len);
      break;
  }

  if (emitname)
    free(emitname);
}

void EM_compile(N_Nhad* output, P_SourceFile* pf) {
  AST_traverse(pf, visit, output);
}
