// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

#include "dozer.h"
#include "util/util.h"

#include <stdlib.h>
#include <string.h>

#define PTR_SIZE sizeof(size_t)

const char* primitives_name = "Drust_Primitives";

// Top-level symbol table.
// Should only contain:
// - The implicit "primitives" module.
// - The current crate as a GS_CRATE.
// - Any extern crates.
G_SymTable* top = NULL;

// Subtables that are created by default.
G_Sym* primtable = NULL;
G_Sym* current_crate = NULL;
G_Sym* implied = NULL;
G_Sym* none = NULL;

struct symbol_table {
  M_Map symbols;
  const PT_Path* path;
};

// Primitive types.
#define PRIM(name, _prim, _size, signed, float) \
  {                                             \
    .ident = name, .tn = {                      \
      .kind = GT_PRIMITIVE,                     \
      .size = _size,                            \
      .align = _size,                           \
      .owner = NULL,                            \
      .primitive.is_signed = signed,            \
      .primitive.is_float = float,              \
      .primitive.kind = _prim                   \
    }                                           \
  }
// clang-format off
struct {
    const char *ident;
    G_TypeNode tn;
} primitives[] = {
    PRIM("()", GP_UNIT, 0, false, false),
    PRIM("i8", GP_I8, 1, true, false),
    PRIM("u8", GP_U8, 1, false, false),
    PRIM("i16", GP_I16, 2, true, false),
    PRIM("u16", GP_U16, 2, false, false),
    PRIM("i32", GP_I32, 4, true, false),
    PRIM("u32", GP_U32, 4, false, false),
    PRIM("i64", GP_I64, 8, true, false),
    PRIM("u64", GP_U64, 8, false, false),
    PRIM("i128", GP_I128, 16, true, false),
    PRIM("u128", GP_U128, 16, false, false),

    PRIM("isize", GP_ISIZE, UNKNOWN, true, false),
    PRIM("usize", GP_USIZE, UNKNOWN, false, false),

    PRIM("f16", GP_F16, 2, true, true),
    PRIM("f32", GP_F32, 4, true, true),
    PRIM("f64", GP_F64, 8, true, true),
    PRIM("f128", GP_F128, 16, true, true),

    PRIM("bool", GP_BOOL, 1, false, false),
    PRIM("char", GP_CHAR, 4, false, false)
};
// clang-format on

static bool type_is_primitive(const G_TypeNode* tn) {
  const void* c = tn;
  return (c >= (void*)&primitives[0]) &&
         (c <= (void*)&primitives[sizeof(primitives) / sizeof(primitives[0])]);
}

G_SymTable* mksymtab() {
  G_SymTable* symtab = U_xmalloc(sizeof(G_SymTable));
  M_init(&symtab->symbols, 16);
  symtab->path = NULL;
  return symtab;
}

static void freesymtab(G_SymTable* symtab);
static void free_symbol(void* c);

static void free_trait_impl(void* c) {
  G_Sym* sym = c;
  if (!sym)
    return;

  free_symbol(sym);
}

static void free_symbol(void* c) {
  G_Sym* sym = c;
  if (!sym)
    return;

  U_xfree(sym->name);
  U_xfree(sym->obj_name);

  if (sym->subitems)
    freesymtab(sym->subitems);

  switch (sym->kind) {
    case GS_TYPE:
      M_free(&sym->type->trait_impls, free_trait_impl);
      if (type_is_primitive(sym->type))
        break;
      free(sym->type);
      break;
  }
}

void freesymtab(G_SymTable* symtab) {
  if (!symtab)
    return;

  M_free(&symtab->symbols, free_symbol);
  free(symtab);
}

static void initsubitems(G_Sym* sym) {
  sym->subitems = mksymtab();
  sym->subitems->path = &sym->full_path;
}

static G_Sym* mksymbol(int kind, const char* name, void* origin_ast) {
  G_Sym* sym = U_xmalloc(sizeof(G_Sym));

  sym->kind = kind;
  sym->name = strdup(name);
  sym->obj_name = NULL;
  sym->origin_ast = origin_ast;
  sym->parent = NULL;
  sym->subitems = NULL;

  return sym;
}

static void add_symbol(G_SymTable* st, G_Sym* sym, bool initpath) {
  M_Key key;
  G_Sym** slot;

  M_initkey(&key, sym->name, strlen(sym->name));
  slot = (G_Sym**)M_insert(&st->symbols, &key);
  *slot = sym;

  if (initpath) {
    if (!st->path)
      U_die("cannot auto-init path for toplevel scope");
    sym->full_path = PT_clone(st->path);
    PT_pushseg(&sym->full_path, sym->name);

    // By default, the object name is the mangled path.
    if (!sym->obj_name)
      sym->obj_name = PT_mangle(&sym->full_path);
  }
}

static G_Sym* mkcrate(const char* cratename) {
  G_Sym* sym = mksymbol(GS_CRATE, cratename, NULL);

  initsubitems(sym);
  sym->full_path = PT_single(cratename, true);
  add_symbol(top, sym, false);
  return sym;
}

static G_Sym* mkmod(G_SymTable* symtab, const char* modname) {
  G_Sym* sym = mksymbol(GS_MODULE, modname, NULL);

  initsubitems(sym);
  sym->full_path = PT_clone(symtab->path);
  PT_pushseg(&sym->full_path, modname);
  add_symbol(symtab, sym, false);
  return sym;
}

static G_Sym* mktype(G_SymTable* symtab, const char* typename, G_TypeNode* tn) {
  G_Sym* sym = mksymbol(GS_TYPE, typename, NULL);

  initsubitems(sym);
  sym->type = tn;
  add_symbol(symtab, sym, true);
  return sym;
}

static void mktypenode(G_TypeNode* tn) {
  M_init(&tn->trait_impls, 16);
}

void G_init(bool with_defaults) {
  G_Sym *prims, *prim;
  size_t i;

  none = mksymbol(GS_NONE, "NONE", NULL);
  if (!with_defaults)
    return;

  top = mksymtab();

  // Create the top-level "primitives" table.
  primtable = mkcrate(primitives_name);
  for (i = 0; i < sizeof(primitives) / sizeof(primitives[0]); i++) {
    // TODO: If we support cross-compilation in the future, make this
    // configurable!
    if (primitives[i].tn.size == UNKNOWN)
      primitives[i].tn.size = PTR_SIZE;
    if (primitives[i].tn.align == UNKNOWN)
      primitives[i].tn.align = PTR_SIZE;
    mktypenode(&primitives[i].tn);

    prim = mktype(primtable->subitems, primitives[i].ident, &primitives[i].tn);
    primitives[i].tn.owner = prim;
  }

  // Create one for the current crate as well.
  current_crate = mkcrate(dozer_cratename);

  // As part of the current crate create a table for "implied types" like arrays
  // and references.
  implied = mkmod(current_crate->subitems, "Drust_implied_types");
}

void G_free() {
  freesymtab(top);
}

static G_Sym* lookupsym(G_SymTable* symtab, const char* name) {
  M_Key key;
  G_Sym* sym;

  M_initkey(&key, name, strlen(name));
  sym = M_get(&symtab->symbols, &key);
  return sym;
}

G_Sym* G_currentcrate() {
  return current_crate;
}

static G_SymTable* primitives_tab() {
  return primtable->subitems;
}

enum lookupstage { LInitial = 0, LToplevel, LPrimitive };

static G_Sym* lookup(G_SymTable* current,
                     const PT_Path* path,
                     enum lookupstage stage) {
  G_SymTable* symtab;
  size_t i;
  PT_PathSegment* seg;
  G_Sym* sym = NULL;

  switch (stage) {
    case LInitial:
      symtab = current;
      break;
    case LToplevel:
      symtab = top;
      break;
    case LPrimitive:
      symtab = primitives_tab();
      break;
  }

  if (!symtab)
    return NULL;

  for (i = 0; i < U_len(path->segs); i++) {
    seg = &path->segs[i];

    // Lookup this symbol.
    sym = lookupsym(symtab, seg->ident);
    if (!sym)
      return NULL;

    // If this is the end of the path, then we are done here.
    if (i + 1 >= U_len(path->segs))
      break;
    if ((seg + 1)->kind == PTY_NONE)
      break;

    // Get the next symbol table to lookup in.
    if (!sym->subitems) {
      PT_dump(stderr, path);
      U_die("item %s does not have subitems", seg->ident);
    }
    symtab = sym->subitems;
  }

  if (!sym) {
    PT_dump(stderr, path);
    U_warn("no traversal at all for symbol?");
  }
  return sym;
}

G_Sym* G_lookup(G_SymTable* current, const PT_Path* path) {
  G_Sym* sym;

  if (!path->has_leading_colon && current) {
    sym = lookup(current, path, LInitial);
    if (sym)
      return sym;
  }

  sym = lookup(NULL, path, LToplevel);
  if (sym)
    return sym;

  return lookup(NULL, path, LPrimitive);
}

G_Sym* G_primitive(G_Primitive kind) {
  size_t i;

  for (i = 0; i < sizeof(primitives) / sizeof(primitives[0]); i++) {
    if (primitives[i].tn.primitive.kind == kind)
      return primitives[i].tn.owner;
  }

  U_die("invalid primitive kind %d", kind);
  return NULL;
}

// Helper function for creating subtypes.
static G_Sym* mksubtype(G_Sym* type,
                        char* obj_name,
                        const char* cache_name,
                        void (*create_subtype)(G_Sym*, G_TypeNode*, void*),
                        void* c) {
  G_Sym *sym, *subcache;
  G_TypeNode* tn;
  PT_Path path;
  char* newname;

  if (!type && !obj_name)
    U_die("type is not resolved yet");
  if (type) {
    if (type->kind != GS_TYPE)
      U_die("mksubtype only works with type symbols");
    if (obj_name)
      U_die("type and obj_name can't both be defined");
    newname = type->obj_name;
  } else
    newname = obj_name;
  if (!newname)
    U_die("obj_name is not populated");

  // Fast path: look it up in the table and see if it already exists.
  path = PT_single(cache_name, false);
  PT_pushseg(&path, newname);
  sym = lookup(implied->subitems, &path, LInitial);
  if (sym) {
    if (obj_name)
      free(obj_name);
    return sym;
  }

  // Create the subtable if it doesn't exist already.
  subcache = lookupsym(implied->subitems, cache_name);
  if (!subcache)
    subcache = mkmod(implied->subitems, cache_name);

  // Slow path: create the symbol and then insert it into the table.
  tn = U_xmalloc(sizeof(G_TypeNode));
  mktypenode(tn);
  create_subtype(type, tn, c);
  sym = mktype(subcache->subitems, newname, tn);

  if (obj_name)
    free(obj_name);
  return sym;
}

static void mkpointer_helper(G_Sym* type, G_TypeNode* tn, void* c) {
  bool* is_mut = c;

  // TODO: Derive PTR_SIZE from current target.
  tn->kind = GT_REFERENCE;
  tn->size = PTR_SIZE;
  tn->align = PTR_SIZE;
  tn->reference.is_mut = *is_mut;
  tn->reference.subtype = type;
}

G_Sym* G_mkpointer(G_Sym* type, bool is_mut) {
  return mksubtype(type, NULL, "refs", mkpointer_helper, &is_mut);
}

static void mksig_helper(G_Sym* type, G_TypeNode* tn, void* c) {
  I_Signature* sig = c;
  I_FnArg* arg;
  G_Sym** args;

  args = U_mkarray(NULL, 0, sizeof(*args));

  tn->kind = GT_FNPTR;
  tn->size = PTR_SIZE;
  tn->align = PTR_SIZE;
  tn->fnptr.sig = sig;
  if (sig->return_type)
    tn->fnptr.return_type = sig->return_type->sym;

  for (arg = sig->fn_args; arg->kind != FNARG_NONE; arg++)
    switch (arg->kind) {
      case FNARG_RECEIVER:
        U_addptr(&args, arg->receiver.ty->sym);
        break;
      case FNARG_TYPED:
        U_addptr(&args, arg->typed.ty->sym);
        break;
    }
  U_addptr(&args, none);
  tn->fnptr.arguments = args;
}

static G_Sym* mkfuncptrtype(I_Signature* signature) {
  U_StringBuilder sb;
  I_FnArg* args;
  G_Sym* sym;

  for (U_mksbuilder(&sb); U_strready(&sb);) {
    U_pushstr(&sb, "FNPTR_");

    if (signature->return_type) {
      U_pushstr(&sb, "RET");
      if (!signature->return_type->sym)
        // Not resolved yet.
        return NULL;
      U_pushstr(&sb, signature->return_type->sym->obj_name);
    } else
      U_pushstr(&sb, "NORET");

    U_pushstr(&sb, "__");

    for (args = signature->fn_args; args->kind != FNARG_NONE; args++) {
      if (args->kind == FNARG_RECEIVER)
        sym = args->receiver.ty->sym;
      else
        sym = args->typed.ty->sym;

      if (!sym)
        // Not resolved yet.
        return NULL;
      U_pushstr(&sb, sym->obj_name);
      U_pushstr(&sb, "_");
    }
  }

  return mksubtype(NULL, U_finishstr(&sb), "fn_ptrs", mksig_helper, signature);
}

struct populator {
  G_Sym* current;
  bool made_progress;
  G_Sym** self_types;  // NULL means type not yet resolved, none means no type.
};

static void push_self_type(struct populator* populator, G_Sym* self_type) {
  U_addptr(&populator->self_types, self_type);
}

static void pop_self_type(struct populator* populator) {
  U_contract(&populator->self_types, 1);
}

static G_Sym* get_self_type(struct populator* populator) {
  G_Sym** ptrs = populator->self_types;
  size_t len = U_len(ptrs);
  return ptrs[len - 1];
}

static int receiver_type(I_Signature* sig) {
  I_FnArg* arg = sig->fn_args;

  switch (arg->kind) {
    case FNARG_NONE:
    case FNARG_TYPED:
      return GRECV_NONE;
    case FNARG_RECEIVER:
      // TODO: Receiver type.
      return GRECV_SELF;
  }
}

static G_Sym* populate_item(struct populator* populator, I_Item* item) {
  G_Sym *sym, *fn_type, **slot;
  char* mangled;
  M_Key key;

  if (item->sym)
    return NULL;

  switch (item->kind) {
    case I_FN:
      fn_type = mkfuncptrtype(&item->fn.sig);
      if (!fn_type)
        // Needed types are not resolved yet.
        return NULL;

      sym = mksymbol(GS_FN, item->fn.sig.name, item);
      sym->fn.sig = &item->fn.sig;
      sym->fn.fn_type = fn_type;
      sym->fn.receiver = receiver_type(&item->fn.sig);
      break;
    case I_FOREIGN_MOD:
      // Foreign modules do not emit a symbol in Rust.
      return NULL;
    case I_IMPL:
      // We populate impl blocks after we've populated types.
      if (!item->impl.self_ty->sym)
        return NULL;
      if (item->impl.self_ty->sym->kind != GS_TYPE)
        U_die("symbol is not GS_TYPE");
      item->parent = populator->current;
      if (item->impl.trait.segs) {
        // Do trait implementation.
        sym = G_lookup(populator->current->subitems, &item->impl.trait);
        if (!sym && sym->kind != GS_TRAIT) {
          PT_dump(stderr, &item->impl.trait);
          if (sym)
            U_die("^ not a trait");
          else
            U_die("^ could not find this trait");
        }
        item->sym = mksymbol(GS_TRAIT_IMPL, sym->obj_name, item);

        // Add to the trait items list.
        M_initkey(&key, sym->obj_name, strlen(sym->obj_name));
        slot = (G_Sym**)M_insert(&sym->type->trait_impls, &key);
        if (*slot)
          U_die("slot is already filled with trait impl");
        *slot = item->sym;
      } else
        // Impl blocks add to the existing type symbol.
        item->sym = item->impl.self_ty->sym;
      return NULL;
    case I_TRAIT:
      sym = mksymbol(GS_TRAIT, item->trait.name, item);
      break;
    default:
      U_die("unsupported item kind for semantic analysis: %d", item->kind);
  }

  sym->subitems = mksymtab();
  sym->subitems->path = &sym->full_path;
  item->sym = sym;

  // TODO: check for no_mangle and avoid mangling if it is set

  return sym;
}

static G_Sym* populate_foreign_item(I_ForeignItem* fi) {
  G_Sym *sym = NULL, *fn_type;
  const char* name;

  if (fi->sym)
    return NULL;

  switch (fi->kind) {
    case FI_FN:
      fn_type = mkfuncptrtype(&fi->fn.sig);
      if (!fn_type)
        // Needed types are not resolved yet.
        return NULL;

      sym = mksymbol(GS_FN, fi->fn.sig.name, fi);
      sym->fn.sig = &fi->fn.sig;
      sym->fn.fn_type = fn_type;
      sym->fn.receiver = GRECV_NONE;
      name = fi->fn.sig.name;
      break;
    default:
      U_die("unsupported foreign item kind for semantic analysis: %d",
            fi->kind);
  }

  if (sym) {
    // Foreign item's names are identical to their actual name.
    sym->obj_name = strdup(name);
    fi->sym = sym;
  }

  return sym;
}

static G_Sym* populate_impl_item(I_ImplItem* ii) {
  G_Sym *sym, *fn_type;

  if (ii->sym)
    return NULL;

  switch (ii->kind) {
    case II_FN:
      fn_type = mkfuncptrtype(&ii->fn.sig);
      if (!fn_type)
        // Needed types are not resolved yet.
        return NULL;

      sym = mksymbol(GS_FN, ii->fn.sig.name, ii);
      sym->fn.sig = &ii->fn.sig;
      sym->fn.fn_type = fn_type;
      sym->fn.receiver = receiver_type(&ii->fn.sig);
      break;
    default:
      U_die("unsupported impl item kind for semantic analysis: %d", ii->kind);
  }

  sym->subitems = mksymtab();
  sym->subitems->path = &sym->full_path;
  ii->sym = sym;

  return sym;
}

static void populate_type(struct populator* populator, TY_Type* type) {
  G_Sym* sym;
  G_SymTable* symtab = populator->current->subitems;
  char* name;

  if (type->sym)
    return;

  if (!symtab)
    U_die("tried populating type with symbol without table");

  switch (type->kind) {
    case TY_PATH:
      // If the type is literally "Self", make it the current "Self" type, if
      // one exists.
      if (type->path.qself.position < 0 &&
          (name = PT_single_segment(&type->path.path)) &&
          strcmp(name, "Self") == 0) {
        type->sym = get_self_type(populator);
        if (type->sym && type->sym->kind == GS_NONE)
          U_die("used 'Self' outside of appropriate context");
        break;
      }

      // Lookup the symbol in the global table if it exists.
      // TODO: qself
      type->sym = G_lookup(symtab, &type->path.path);
      if (type->sym && type->sym->kind != GS_TYPE) {
        PT_dump(stderr, &type->path.path);
        U_die("^ this does not refer to a type");
      }
      break;
    case TY_PAREN:
      // Note: subtypes are always looked up first.
      type->sym = type->paren.ty->sym;
      break;
    case TY_PTR:
      if (type->ptr.ty->sym)
        type->sym =
            G_mkpointer(type->ptr.ty->sym, type->ptr.mutability == TYP_MUT);
      break;
    default:
      U_die("unsupported type for semantic analysis: %d", type->kind);
  }

  if (type->sym)
    populator->made_progress = true;
}

static G_Sym* item_selftype(I_Item* item) {
  if (item->kind == I_IMPL)
    return item->impl.self_ty->sym;

  return none;
}

static void update_populator(struct populator* populator,
                             AST_Kind kind,
                             void* ast) {
  I_Item* item;
  I_ImplItem* ii;
  enum { AUpSym, ADownSym } action = AUpSym;
  G_Sym *sym = NULL, *parent = NULL;

  switch (kind) {
    case AST_ITEM_B:
      item = ast;
      sym = item->sym;
      push_self_type(populator, item_selftype(item));
      break;
    case AST_ITEM:
      item = ast;
      sym = item->sym;
      action = ADownSym;
      parent = item->parent;
      pop_self_type(populator);
      break;
    case AST_IMPL_ITEM_B:
      ii = ast;
      sym = ii->sym;
      break;
    case AST_IMPL_ITEM:
      ii = ast;
      sym = ii->sym;
      action = ADownSym;
      break;
  }

  if (sym)
    switch (action) {
      case AUpSym:
        populator->current = sym;
        break;
      case ADownSym:
        if (!parent)
          parent = populator->current->parent;
        populator->current = parent;
        if (!populator->current)
          U_die("undid too many scopes");
        break;
    }
}

static void populate_symbols(AST_Kind kind, void* ast, void* c) {
  struct populator* populator = c;
  G_Sym* sym_to_add = NULL;

  switch (kind) {
    case AST_ITEM_B:
      sym_to_add = populate_item(populator, ast);
      break;
    case AST_FOREIGN_ITEM_B:
      sym_to_add = populate_foreign_item(ast);
      break;
    case AST_IMPL_ITEM_B:
      sym_to_add = populate_impl_item(ast);
      break;
    case AST_TYPE:
      populate_type(populator, ast);
      break;
  }

  if (sym_to_add) {
    if (!populator->current->subitems)
      U_die("tried to add symbol to other symbol with no subitems");
    add_symbol(populator->current->subitems, sym_to_add, true);
    sym_to_add->parent = populator->current;
    populator->made_progress = true;
  }

  update_populator(populator, kind, ast);
}

static void check_populate(AST_Kind kind, void* ast, void* c) {
  TY_Type* type;
  I_Item* item;

  switch (kind) {
    case AST_ITEM:
      item = ast;
      if (item->kind == GS_FN && !item->sym)
        U_die("did not generate symbol for function %s", item->fn.sig.name);
      break;
    case AST_TYPE:
      type = ast;
      if (!type->sym) {
        TY_dump(stderr, type, 0);
        U_die("unresolved type");
      }
      break;
  }
}

void G_populate(P_SourceFile* sf) {
  struct populator populator;

  populator.current = G_currentcrate();
  populator.self_types = U_mkarray(NULL, 1, sizeof(*populator.self_types));
  push_self_type(&populator, NULL);

  do {
    populator.made_progress = false;
    AST_traverse(sf, populate_symbols, &populator);
    if (populator.current != G_currentcrate())
      U_die("symbol level mismatch");
  } while (populator.made_progress);

  AST_traverse(sf, check_populate, NULL);
}

// We need a placeholder symbol for this. We can go back later and replace all
// of these.
static G_Sym* readtempsym(B_Reader* reader) {
  G_Sym* sym;

  sym = U_xmalloc(sizeof(G_Sym));
  sym->kind = GS_TEMPPATH;
  if (!PT_readpath(reader, &sym->full_path)) {
    free(sym);
    return NULL;
  }

  return sym;
}

// Traverse tree and remove tempsyms.
static void removetempsyms(G_Sym** slot) {
  G_Sym *sym = *slot, **arg;
  size_t i;

  if (!sym)
    return;
  if (sym->kind == GS_TEMPPATH) {
    // Replace the temporary symbol.
    *slot = G_lookup(NULL, &sym->full_path);
    free(sym);
    return;
  }

  // Recurse into subsymbols.
  if (sym->subitems)
    for (i = 0; i < sym->subitems->symbols.cap; i++)
      if (sym->subitems->symbols.keys[i].str)
        removetempsyms((G_Sym**)&sym->subitems->symbols.values[i]);
  if (sym->kind == GS_TYPE)
    switch (sym->type->kind) {
      case GT_REFERENCE:
        removetempsyms(&sym->type->reference.subtype);
        break;
      case GT_FNPTR:
        if (sym->type->fnptr.return_type)
          removetempsyms(&sym->type->fnptr.return_type);
        for (arg = sym->type->fnptr.arguments; (*arg)->kind != GS_NONE; arg++)
          removetempsyms(arg);
        break;
    }
}

static bool readtype(B_Reader* reader, G_TypeNode** tn) {
  uint32_t kind, size, align, slot, i;
  G_Sym *sym, **syms;

  *tn = U_xmalloc(sizeof(G_TypeNode));
  mktypenode(*tn);

  // Read kind, size and aign.
#define TRY(x)    \
  if (!x) {       \
    U_xfree(*tn); \
    return false; \
  }

  TRY(B_read32(reader, &kind));
  TRY(B_read32(reader, &size));
  TRY(B_read32(reader, &align));

  (*tn)->kind = kind;
  (*tn)->size = (size_t)size;
  (*tn)->align = (size_t)align;

  switch (kind) {
    case GT_FNPTR:
      (*tn)->fnptr.sig = NULL;
      TRY(B_read32(reader, &slot));
      if (!!slot)
        TRY(((*tn)->fnptr.return_type = readtempsym(reader)))
      else
        (*tn)->fnptr.return_type = NULL;

      TRY(B_read32(reader, &slot));
      syms = U_mkarray(NULL, 0, sizeof(*syms));
      for (i = 0; i < slot; i++) {
        TRY((sym = readtempsym(reader)));
        U_addptr(&syms, sym);
      }
      U_addptr(&syms, none);
      (*tn)->fnptr.arguments = syms;
      break;
    case GT_PRIMITIVE:
      TRY(B_read32(reader, &slot));
      (*tn)->primitive.is_float = !!slot;
      TRY(B_read32(reader, &slot));
      (*tn)->primitive.is_signed = !!slot;
      TRY(B_read32(reader, &slot));
      (*tn)->primitive.kind = slot;
      break;
    case GT_REFERENCE:
      TRY(B_read32(reader, &slot));
      (*tn)->reference.is_mut = !!slot;
      (*tn)->reference.subtype = readtempsym(reader);
      break;
  }

#undef TRY
  return true;
}

static bool readsymtab(B_Reader* reader, G_SymTable** symtab);
static void writesymtab(B_Writer* writer, const G_SymTable* symtab);

static bool readsym(B_Reader* reader, G_Sym** sym) {
  uint32_t kind, has_subitems;

  *sym = U_xmalloc(sizeof(G_Sym));
  (*sym)->subitems = NULL;

#define TRY(x)                                                         \
  if (!x) {                                                            \
    fprintf(stderr, "readsym failed at %s, %d\n", __FILE__, __LINE__); \
    U_xfree((*sym)->name);                                             \
    free(*sym);                                                        \
    return false;                                                      \
  }

  // Read the kind first.
  TRY(B_read32(reader, &kind));
  (*sym)->kind = (int)kind;
  if ((*sym)->kind == GS_NONE) {
    free(*sym);
    *sym = none;
    return true;
  }

  // Read symbol subtable.
  TRY(B_read32(reader, &has_subitems));
  if (has_subitems)
    TRY(readsymtab(reader, &(*sym)->subitems))

  switch ((*sym)->kind) {
    case GS_FN:
      break;
    case GS_TYPE:
      TRY(readtype(reader, &(*sym)->type));
      (*sym)->type->owner = *sym;
      break;
    case GS_NONE:
    case GS_CRATE:
    case GS_MODULE:
      break;
    default:
      U_die("invalid type %d", (*sym)->kind);
  }

#undef TRY
  return true;
}

static bool readsymtab(B_Reader* reader, G_SymTable** symtab) {
  uint32_t len, i;
  char* name;
  G_Sym *sym, **slot;
  M_Key key;

  if (!*symtab)
    *symtab = mksymtab();

#define TRY(x)                                                                 \
  if (!x) {                                                                    \
    fprintf(stderr, "readsymtab failed at %s, line %d\n", __FILE__, __LINE__); \
    freesymtab(*symtab);                                                       \
    return false;                                                              \
  }

  // Read the number of items.
  TRY(B_read32(reader, &len));

  // Read the items.
  for (i = 0; i < len; i++) {
    TRY(B_readstr(reader, &name));
    if (!readsym(reader, &sym)) {
      fprintf(stderr, "failed to parse symbol %s\n", name);
      TRY(false);
    }

    // Insert into the map.
    M_initkey(&key, name, strlen(name));
    slot = (G_Sym**)M_insert(&(*symtab)->symbols, &key);
    if (*slot)
      U_die("multiple symtab items named %s", name);
    *slot = sym;

    // Store name as symbol's name.
    sym->name = name;
    sym->origin_ast = NULL;
  }

#undef TRY
  return true;
}

bool G_readsyms(B_Reader* reader) {
  size_t i;

  if (!readsymtab(reader, &top))
    return false;

  // Comb temp symbols.
  for (i = 0; i < top->symbols.cap; i++)
    if (top->symbols.keys[i].str)
      removetempsyms((G_Sym**)&top->symbols.values[i]);

  return true;
}

static void writetype(B_Writer* writer, const G_TypeNode* tn) {
  uint32_t count;
  G_Sym** args;

  B_write32(writer, (uint32_t)tn->kind);
  B_write32(writer, (uint32_t)tn->size);
  B_write32(writer, (uint32_t)tn->align);

  switch (tn->kind) {
    case GT_FNPTR:
      if (tn->fnptr.return_type) {
        B_write32(writer, 1);
        PT_writepath(writer, &tn->fnptr.return_type->full_path);
      } else
        B_write32(writer, 0);

      count = 0;
      for (args = tn->fnptr.arguments; (*args)->kind != GS_NONE; args++)
        count++;
      B_write32(writer, count);
      for (args = tn->fnptr.arguments; (*args)->kind != GS_NONE; args++)
        PT_writepath(writer, &(*args)->full_path);

      break;
    case GT_PRIMITIVE:
      B_write32(writer, tn->primitive.is_float);
      B_write32(writer, tn->primitive.is_signed);
      B_write32(writer, tn->primitive.kind);
      break;
    case GT_REFERENCE:
      B_write32(writer, tn->reference.is_mut);
      PT_writepath(writer, &tn->reference.subtype->full_path);
      break;
  }
}

static void writesym(B_Writer* writer, const G_Sym* sym) {
  B_write32(writer, (uint32_t)sym->kind);

  if (sym->subitems) {
    B_write32(writer, 1);
    writesymtab(writer, sym->subitems);
  } else
    B_write32(writer, 0);

  switch (sym->kind) {
    case GS_TYPE:
      writetype(writer, sym->type);
  }
}

static void writesymtab(B_Writer* writer, const G_SymTable* symtab) {
  size_t i, count = 0;
  G_Sym* sym;

  B_write32(writer, (uint32_t)symtab->symbols.len);

  // Write all map items.
  for (i = 0; i < symtab->symbols.cap; i++) {
    if (!(symtab->symbols.keys[i].str))
      continue;
    sym = symtab->symbols.values[i];
    B_writestr(writer, sym->name);
    writesym(writer, sym);
    count++;
  }
}

void G_writesyms(B_Writer* writer) {
  writesymtab(writer, top);
}

// Easy test for reader.
#if 0
int main(int argc, char **argv) {
  B_Writer writer;
  FILE *file;

  if (argc < 2) {
    fprintf(stderr, "usage: %s [outpath]", argv[0]);
    return 1;
  }
  program_name = argv[0];

  B_mkwriter(&writer);
  top = mksymtab();
  mkcrate("crate1");

  G_writesyms(&writer);

  file = fopen(argv[1], "wb");
  if (!file)
    U_die("fopen:");

  if (fwrite(writer.data, 1, writer.len, file) != writer.len)
    U_die("fwrite:");
  fclose(file);

  free(writer.data);
  return 0;
}
#endif
