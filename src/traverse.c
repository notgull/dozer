// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

#include "dozer.h"

static void traverse_expr(E_Expr* expr, AST_Traverser tr, void* c);
static void traverse_pat(PN_Pat* pat, AST_Traverser tr, void* c);
static void traverse_path(PT_Path* path, AST_Traverser tr, void* c);

static void traverse_type(TY_Type* type, AST_Traverser tr, void* c) {
  TY_Type** elem;

  if (!type)
    return;

  tr(AST_TYPE_B, type, c);

  switch (type->kind) {
    case TY_ARRAY:
      traverse_type(type->array.ty, tr, c);
      traverse_expr(type->array.len, tr, c);
      break;
    case TY_PAREN:
      traverse_type(type->paren.ty, tr, c);
      break;
    case TY_PATH:
      traverse_path(&type->path.path, tr, c);
      break;
    case TY_PTR:
      traverse_type(type->ptr.ty, tr, c);
      break;
    case TY_SLICE:
      traverse_type(type->slice.ty, tr, c);
      break;
    case TY_TUPLE:
      for (elem = type->tuple.elems; (*elem)->kind != TY_NONE; elem++)
        traverse_type(*elem, tr, c);
      break;
  }

  tr(AST_TYPE, type, c);
}

static void traverse_path(PT_Path* path, AST_Traverser tr, void* c) {
  // TODO: Path elements

  tr(AST_PATH_B, path, c);
  tr(AST_PATH, path, c);
}

static void traverse_expr(E_Expr* expr, AST_Traverser tr, void* c) {
  E_Expr** elem;
  ST_Stmt* stmt;

  if (!expr)
    return;

  tr(AST_EXPR_B, expr, c);

  switch (expr->kind) {
    case E_ARRAY:
      for (elem = expr->array.elems; (*elem)->kind != E_NONE; elem++)
        traverse_expr(*elem, tr, c);
      break;
    case E_ASSIGN:
      traverse_expr(expr->assign.left, tr, c);
      traverse_expr(expr->assign.right, tr, c);
      break;
    case E_ASYNC:
      for (stmt = expr->async.stmts; stmt->kind != ST_NONE; stmt++)
        AST_traversestmt(stmt, tr, c);
      break;
    case E_AWAIT:
      traverse_expr(expr->await.expr, tr, c);
      break;
    case E_BINARY:
      traverse_expr(expr->binary.left, tr, c);
      traverse_expr(expr->binary.right, tr, c);
      break;
    case E_BLOCK:
      for (stmt = expr->block.stmts; stmt->kind != ST_NONE; stmt++)
        AST_traversestmt(stmt, tr, c);
      break;
    case E_BREAK:
      traverse_expr(expr->break_.expr, tr, c);
      break;
    case E_CALL:
      traverse_expr(expr->call.func, tr, c);
      for (elem = expr->call.args; (*elem)->kind != E_NONE; elem++)
        traverse_expr(*elem, tr, c);
      break;
    case E_CAST:
      traverse_expr(expr->cast.expr, tr, c);
      traverse_type(expr->cast.type, tr, c);
      break;
    case E_FIELD:
      traverse_expr(expr->field.expr, tr, c);
      break;
    case E_FOR_LOOP:
      traverse_expr(expr->for_loop.expr, tr, c);
      traverse_pat(expr->for_loop.pat, tr, c);
      for (stmt = expr->block.stmts; stmt->kind != ST_NONE; stmt++)
        AST_traversestmt(stmt, tr, c);
      break;
    case E_LOOP:
      for (stmt = expr->loop.stmts; stmt->kind != ST_NONE; stmt++)
        AST_traversestmt(stmt, tr, c);
      break;
    case E_METHOD_CALL:
      traverse_expr(expr->method_call.receiver, tr, c);
      for (elem = expr->method_call.args; (*elem)->kind != E_NONE; elem++)
        traverse_expr(*elem, tr, c);
      break;
    case E_PAREN:
      traverse_expr(expr->paren.expr, tr, c);
      break;
    case E_PATH:
      traverse_path(&expr->path.path, tr, c);
      break;
    case E_RANGE:
      traverse_expr(expr->range.left, tr, c);
      traverse_expr(expr->range.right, tr, c);
      break;
    case E_REFERENCE:
      traverse_expr(expr->reference.expr, tr, c);
      break;
    case E_REPEAT:
      traverse_expr(expr->repeat.expr, tr, c);
      traverse_expr(expr->repeat.len, tr, c);
      break;
    case E_RETURN:
      traverse_expr(expr->return_.expr, tr, c);
      break;
    case E_TUPLE:
      for (elem = expr->tuple.elems; (*elem)->kind != E_NONE; elem++)
        traverse_expr(*elem, tr, c);
      break;
    case E_TRY_BLOCK:
      for (stmt = expr->try_block.stmts; stmt->kind != ST_NONE; stmt++)
        AST_traversestmt(stmt, tr, c);
      break;
    case E_UNARY:
      traverse_expr(expr->unary.expr, tr, c);
      break;
    case E_WHILE:
      traverse_expr(expr->while_.cond, tr, c);
      for (stmt = expr->while_.stmts; stmt->kind != ST_NONE; stmt++)
        AST_traversestmt(stmt, tr, c);
      break;
  }

  tr(AST_EXPR, expr, c);
}

static void traverse_pat(PN_Pat* pat, AST_Traverser tr, void* c) {
  PN_Pat** subpat;

  if (!pat)
    return;

  tr(AST_PATTERN_B, pat, c);

  switch (pat->kind) {
    case PN_IDENT:
      traverse_pat(pat->ident.subpattern, tr, c);
      break;
    case PN_OR:
      for (subpat = pat->or.patterns; (*subpat)->kind != PN_NONE; subpat++)
        traverse_pat(*subpat, tr, c);
      break;
    case PN_PATH:
      traverse_path(&pat->path.path, tr, c);
      break;
  }

  tr(AST_PATTERN, pat, c);
}

void AST_traversestmt(ST_Stmt* st, AST_Traverser tr, void* c) {
  tr(AST_STMT_B, st, c);

  switch (st->kind) {
    case ST_EXPR:
      traverse_expr(st->expr.expr, tr, c);
      break;
    case ST_LOCAL:
      traverse_pat(st->local.pat, tr, c);
      traverse_type(st->local.type, tr, c);
      traverse_expr(st->local.expr, tr, c);
      break;
  }

  tr(AST_STMT, st, c);
}

static void traverse_sig(I_Signature* sig, AST_Traverser tr, void* c) {
  I_FnArg* arg;

  for (arg = sig->fn_args; arg->kind != FNARG_NONE; arg++)
    switch (arg->kind) {
      case FNARG_RECEIVER:
        if (arg->receiver.ty)
          traverse_type(arg->receiver.ty, tr, c);
        break;
      case FNARG_TYPED:
        traverse_pat(arg->typed.pat, tr, c);
        traverse_type(arg->typed.ty, tr, c);
        break;
    }

  if (sig->variadic)
    traverse_pat(sig->variadic, tr, c);
  if (sig->return_type)
    traverse_type(sig->return_type, tr, c);
}

static void traverse_trait_item(I_TraitItem* it, AST_Traverser tr, void* c) {
  ST_Stmt* stmt;

  tr(AST_TRAIT_ITEM_B, it, c);

  switch (it->kind) {
    case TI_FN:
      traverse_sig(&it->fn.sig, tr, c);
      if (it->fn.defaults)
        for (stmt = it->fn.defaults; stmt->kind != ST_NONE; stmt++)
          AST_traversestmt(stmt, tr, c);
      break;
  }

  tr(AST_TRAIT_ITEM, it, c);
}

static void traverse_foreign_item(I_ForeignItem* it,
                                  AST_Traverser tr,
                                  void* c) {
  tr(AST_FOREIGN_ITEM_B, it, c);

  switch (it->kind) {
    case FI_FN:
      traverse_sig(&it->fn.sig, tr, c);
      break;
  }

  tr(AST_FOREIGN_ITEM, it, c);
}

static void traverse_impl_item(I_ImplItem* ii, AST_Traverser tr, void* c) {
  ST_Stmt* stmt;

  tr(AST_IMPL_ITEM_B, ii, c);

  switch (ii->kind) {
    case II_FN:
      traverse_sig(&ii->fn.sig, tr, c);
      for (stmt = ii->fn.stmts; stmt->kind != ST_NONE; stmt++)
        AST_traversestmt(stmt, tr, c);
      break;
  }

  tr(AST_IMPL_ITEM, ii, c);
}

static void traverse_item(I_Item* it, AST_Traverser tr, void* c) {
  ST_Stmt* stmt;
  I_TraitItem* item;
  I_ForeignItem* fi;
  I_ImplItem* ii;

  tr(AST_ITEM_B, it, c);

  switch (it->kind) {
    case I_FN:
      traverse_sig(&it->fn.sig, tr, c);
      for (stmt = it->fn.stmts; stmt->kind != ST_NONE; stmt++)
        AST_traversestmt(stmt, tr, c);
      break;
    case I_TRAIT:
      for (item = it->trait.items; item->kind != TI_NONE; item++)
        traverse_trait_item(item, tr, c);
      break;
    case I_FOREIGN_MOD:
      for (fi = it->foreign_mod.items; fi->kind != FI_NONE; fi++)
        traverse_foreign_item(fi, tr, c);
      break;
    case I_IMPL:
      if (it->impl.trait.segs)
        traverse_path(&it->impl.trait, tr, c);
      traverse_type(it->impl.self_ty, tr, c);
      for (ii = it->impl.items; ii->kind != II_NONE; ii++)
        traverse_impl_item(ii, tr, c);
      break;
  }

  tr(AST_ITEM, it, c);
}

void AST_traverse(P_SourceFile* sf, AST_Traverser traverse, void* context) {
  I_Item** item;

  for (item = sf->items; (*item)->kind != I_NONE; item++)
    traverse_item(*item, traverse, context);
}
