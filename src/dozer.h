// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

#ifndef DOZER_H
#define DOZER_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include "nhad/nhad.h"
#include "util/byteops.h"
#include "util/util.h"

#include "parser/lexer/lex.h"
#include "parser/parser.h"

extern char* dozer_cratename;

//>> parser (parser.c)

// Open or close a parser.
void P_freeparser(P_Parser* parser);

const T_Token* P_cur(const P_Parser* parser);
void P_next(P_Parser* parser);
const T_Token* P_peek(const P_Parser* parser);
const T_Token* P_peek2(const P_Parser* parser);
bool P_consume(P_Parser* parser, T_TokenKind tk);
char* P_expect(P_Parser* parser, T_TokenKind tk, const char* msg);

P_Parser* P_fork(P_Parser* parser);
void P_unfork(P_Parser* src, P_Parser* forked);

//>> paths (path.c)

void PT_qpath(P_Parser* parser,
              PT_QSelf* qself,
              PT_Path* path,
              bool expr_style);
PT_Path PT_single(const char* ident, bool has_leading_colon);
PT_Path PT_clone(const PT_Path* path);
char* PT_mangle(const PT_Path* path);
void PT_pushseg(PT_Path* path, const char* ident);
bool PT_is_modstyle(const PT_Path* path);
char* PT_single_segment(const PT_Path* path);
void PT_dump(FILE* f, const PT_Path* path);

bool PT_readpath(B_Reader* reader, PT_Path* path);
void PT_writepath(B_Writer* writer, const PT_Path* path);

//>> global symbol table (global.c)

void G_init(bool with_defaults);
void G_free();

bool G_readsyms(B_Reader* reader);
void G_writesyms(B_Writer* writer);

G_Sym* G_currentcrate();
G_Sym* G_lookup(G_SymTable* current, const PT_Path* path);
void G_populate(P_SourceFile* sf);

G_Sym* G_primitive(G_Primitive kind);
G_Sym* G_mkpointer(G_Sym* type, bool is_mut);

//>> expr resolution (resolve.c)

void R_resolve(P_SourceFile* sf);

//>> expressions (expr.c)

// Read a single expression.
E_Expr* E_read(P_Parser* parser);
E_Expr* E_read_earlier_boundary_rule(P_Parser* parser);

bool E_requires_semi_to_be_stmt(const E_Expr* expr);
bool E_requires_comma_to_be_match_arm(const E_Expr* expr);

void E_dump(FILE* f, const E_Expr* expr, int indent);

//>> patterns (pattern.c)

// Parse patterns.
PN_Pat* PN_read_one(P_Parser* parser);
PN_Pat* PN_read_multi(P_Parser* parser, bool leading_vert);
PN_PatType PN_read_pattype(P_Parser* parser);

void PN_dump(FILE* f, const PN_Pat* pat, int indent);

//>> types (type.c)

TY_Type* TY_read(P_Parser* parser);
TY_Type* TY_read_caststyle(P_Parser* parser);
TY_Type* TY_returntype_read(P_Parser* parser);
TY_Type* TY_returntype_read_noplus(P_Parser* parser);
void TY_dump(FILE* f, const TY_Type* ty, int indent);

//>> items (item.c)

I_Item* I_read(P_Parser* parser);

//>> statements (stmt.c)

ST_Stmt* ST_block(P_Parser* parser);
ST_Stmt* ST_evalexpr(ST_Stmt* stmts);
void ST_dumpblock(FILE* f, const ST_Stmt* stmts, int indent);

//>> AST traversal (traverse.c)

typedef enum astkind AST_Kind;
typedef void (*AST_Traverser)(AST_Kind, void*, void*);

enum astkind {
  AST_NONE = 0,

  // These are emitted after all children are processed.
  AST_EXPR,
  AST_STMT,
  AST_ITEM,
  AST_TYPE,
  AST_PATH,
  AST_PATTERN,
  AST_TRAIT_ITEM,
  AST_FOREIGN_ITEM,
  AST_IMPL_ITEM,

  // These are emitted before all children are processed.
  AST_EXPR_B,
  AST_STMT_B,
  AST_ITEM_B,
  AST_TYPE_B,
  AST_PATH_B,
  AST_PATTERN_B,
  AST_TRAIT_ITEM_B,
  AST_FOREIGN_ITEM_B,
  AST_IMPL_ITEM_B
};

void AST_traverse(P_SourceFile* sf, AST_Traverser traverse, void* context);
void AST_traversestmt(ST_Stmt* st, AST_Traverser travers, void* context);

//>> code emissions (emit.c)

void EM_compile(N_Nhad* output, P_SourceFile* pf);

#endif
