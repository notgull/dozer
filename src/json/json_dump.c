#include "json_dump.h"
#include <stdarg.h>

static void json_dump_expr(FILE* f,
                           E_Expr* expr,
                           int indent,
                           char* overwrite_name,
                           bool end_with_comma);

static void jprintf(FILE* f, int indent, char* format, ...) {
  U_indent(f, indent);

  va_list args;
  va_start(args, format);

  if (vfprintf(f, format, args) < 0) {
    U_die("failed to write json\n");
  }

  va_end(args);
}

static void json_dump_path(FILE* f, const PT_Path* path, int indent) {
  jprintf(f, indent, "\"segments\": [\n");

  PT_PathSegment* segments = path->segs;
  for (size_t i = 0; i < U_len(path->segs); ++i) {
    jprintf(f, indent + 1, "{\n");

    jprintf(f, indent + 2, "\"ident\": \"%s\"\n", segments[i].ident);

    jprintf(f, indent + 1, "}");
    if (i != U_len(path->segs) - 1) {
      jprintf(f, 0, ",");
    }
    jprintf(f, 0, "\n");
  }

  jprintf(f, indent, "]\n");
}

static void json_dump_type(FILE* f,
                           const TY_Type* type,
                           int indent,
                           char* overwrite_header_name,
                           bool comma_after_last_brace) {
  jprintf(f, indent, "\"%s\": {\n",
          overwrite_header_name ? overwrite_header_name : "ty");

  if (type->kind == TY_PATH) {
    jprintf(f, indent + 1, "\"path\": {\n");
    json_dump_path(f, &type->path.path, indent + 2);
    jprintf(f, indent + 1, "}\n");
  } else if (type->kind == TY_PTR) {
    jprintf(f, indent + 1, "\"ptr\": {\n");
    jprintf(f, indent + 2, "\"const\": %s,\n",
            type->ptr.mutability == TYP_CONST ? "true" : "false");

    json_dump_type(f, type->ptr.ty, indent + 2, "elem", false);

    jprintf(f, indent + 1, "}\n");
  } else {
    U_die("unhandled type");
  }

  jprintf(f, indent, "}%s\n", comma_after_last_brace ? "," : "");
}

static void json_dump_generics(FILE* f,
                               const I_Generics* generics,
                               int indent) {
  jprintf(f, indent, "\"generics\": {\n");
  jprintf(f, indent + 1, "\"params\": [\n");

  bool first = true;
  for (I_GenericItem* gi = generics->items; gi->kind != GI_NONE; ++gi) {
    if (!first) {
      fprintf(f, ",\n");
    }
    first = false;

    jprintf(f, indent + 2, "{\n");

    if (gi->kind == GI_LIFETIME) {
      jprintf(f, indent + 3, "\"lifetime\": {\n");
      jprintf(f, indent + 4, "\"lifetime\": \"%s\"\n",
              gi->lifetime.name + 1); /* skip ' character */
      jprintf(f, indent + 3, "}\n");
    } else if (gi->kind == GI_TYPE) {
      jprintf(f, indent + 3, "\"type\": {\n");
      jprintf(f, indent + 4, "\"ident\": \"%s\"\n", gi->type.name);
      jprintf(f, indent + 3, "}\n");
    } else if (gi->kind == GI_CONST) {
      jprintf(f, indent + 3, "\"const\": {\n");
      jprintf(f, indent + 4, "\"ident\": \"%s\",\n", gi->const_.name);

      json_dump_type(f, gi->const_.type, indent + 4, NULL, false);

      jprintf(f, indent + 3, "}\n");
    } else {
      U_die("unhandled generic item kind %d", gi->kind);
    }

    jprintf(f, indent + 2, "}");
  }
  fprintf(f, "\n");

  jprintf(f, indent + 1, "]\n");
  jprintf(f, indent, "},\n");
}

static void json_dump_pattern(FILE* f, PN_Pat* pat, int indent) {
  jprintf(f, indent, "\"pat\": {\n");
  if (pat->kind == PN_IDENT) {
    jprintf(f, indent + 1, "\"ident\": {\n");
    jprintf(f, indent + 2, "\"ident\": \"%s\"\n", pat->ident.ident);
    jprintf(f, indent + 1, "}\n");
  } else {
    U_die("unhandled pattern kind");
  }

  jprintf(f, indent, "}");
}

static void json_dump_struct(FILE* f, const I_Item* item, int indent) {
  if (!(item->kind == I_UNIT_STRUCT || item->kind == I_TUPLE_STRUCT ||
        item->kind == I_ITEM_STRUCT)) {
    U_die("invalid item passed to ");
  }

  jprintf(f, indent, "\"struct\": {\n");

  char* name = NULL;
  const I_Generics* generics = NULL;
  if (item->kind == I_UNIT_STRUCT) {
    name = item->unit_struct.name;
  } else if (item->kind == I_TUPLE_STRUCT) {
    name = item->tuple_struct.name;
    if (item->tuple_struct.generics.items->kind != GI_NONE) {
      generics = &item->tuple_struct.generics;
    }
  } else {
    name = item->item_struct.name;
    if (item->item_struct.generics.items->kind != GI_NONE) {
      generics = &item->item_struct.generics;
    }
  }

  jprintf(f, indent + 1, "\"ident\": \"%s\",\n", name);

  if (generics != NULL) {
    json_dump_generics(f, generics, indent + 1);
  }

  jprintf(f, indent + 1, "\"fields\": ");

  if (item->kind == I_UNIT_STRUCT) {
    fprintf(f, "\"unit\"\n");
  } else {
    fprintf(f, "{\n");

    jprintf(f, indent + 2, "\"%s\": [\n",
            item->kind == I_TUPLE_STRUCT ? "unnamed" : "named");

    if (item->kind == I_TUPLE_STRUCT) {
      bool first = true;
      for (TY_Type** type = item->tuple_struct.fields; *type; type++) {
        if (!first) {
          fprintf(f, ",\n");
        }
        first = false;

        jprintf(f, indent + 3, "{\n");

        json_dump_type(f, *type, indent + 4, NULL, false);

        jprintf(f, indent + 3, "}");
      }
      fprintf(f, "\n");
    } else {
      bool first = true;
      for (I_StructField* sf = item->item_struct.fields; sf->type != TY_NONE;
           sf++) {
        if (!first) {
          fprintf(f, ",\n");
        }
        first = false;

        jprintf(f, indent + 3, "{\n");

        jprintf(f, indent + 4, "\"ident\": \"%s\",\n", sf->name);
        jprintf(f, indent + 4, "\"colon_token\": true,\n");

        json_dump_type(f, sf->type, indent + 4, NULL, false);

        jprintf(f, indent + 3, "}");
      }

      fprintf(f, "\n");
    }

    jprintf(f, indent + 2, "]\n");

    jprintf(f, indent + 1, "}\n");
  }

  jprintf(f, indent, "}\n");
}

static void json_dump_enum(FILE* f, const I_Item* item, int indent) {
  I_EnumVariant* ev;
  TY_Type** ty;
  I_StructField* sf;
  if (!(item->kind == I_ENUM)) {
    U_die("invalid item passed to ");
  }

  jprintf(f, indent, "\"enum\": {\n");

  jprintf(f, indent + 1, "\"ident\": \"%s\",\n", item->enum_.name);

  if (item->enum_.generics.items->kind != GI_NONE) {
    json_dump_generics(f, &item->enum_.generics, indent + 1);
  }

  jprintf(f, indent + 1, "\"variants\": [");

  if (item->enum_.variants->kind != IE_NONE) {
    jprintf(f, 0, "\n");
    for (ev = item->enum_.variants; ev->kind != IE_NONE; ev++) {
      jprintf(f, indent + 2, "{\n");
      jprintf(f, indent + 3, "\"ident\": \"%s\",\n", ev->name);
      jprintf(f, indent + 3, "\"fields\": ");
      switch (ev->kind) {
        case IE_STRUCT:
          jprintf(f, 0, "{\n");
          jprintf(f, indent + 4, "\"named\": [");
          if (ev->struct_fields->type != TY_NONE) {
            jprintf(f, 0, "\n");
            for (sf = ev->struct_fields; sf->type != TY_NONE; sf++) {
              jprintf(f, indent + 5, "{\n");

              jprintf(f, indent + 6, "\"ident\": \"%s\",\n", sf->name);
              jprintf(f, indent + 6, "\"colon_token\": true,\n");

              json_dump_type(f, sf->type, indent + 6, NULL, false);

              jprintf(f, indent + 5, "}");
              if ((sf + 1)->type != TY_NONE) {
                jprintf(f, 0, ",");
              }
              jprintf(f, 0, "\n");
            }
            jprintf(f, indent + 4, "]\n");
          } else {
            jprintf(f, 0, "]\n");
          }
          jprintf(f, indent + 3, "}");
          break;
        case IE_TUPLE:
          jprintf(f, 0, "{\n");
          jprintf(f, indent + 4, "\"unnamed\": [");
          if (*ev->tuple_fields != NULL) {
            jprintf(f, 0, "\n");
            for (ty = ev->tuple_fields; *ty; ty++) {
              jprintf(f, indent + 5, "{\n");
              json_dump_type(f, *ty, indent + 6, NULL, false);
              jprintf(f, indent + 5, "}");
              if (*(ty + 1) != NULL) {
                jprintf(f, 0, ",");
              }
              jprintf(f, 0, "\n");
            }
            jprintf(f, indent + 4, "]\n");
          } else {
            jprintf(f, 0, "]\n");
          }
          jprintf(f, indent + 3, "}");
          break;
        case IE_UNIT:
          jprintf(f, 0, "\"unit\"");
          break;
        case IE_NONE:
          break;
      }
      if (ev->disc == NULL) {
        jprintf(f, 0, "\n");
      } else {
        jprintf(f, 0, ",\n");
        jprintf(f, indent + 3, "\"discriminant\": {\n");
        json_dump_expr(f, ev->disc, indent + 4, NULL, false);
        jprintf(f, indent + 3, "}\n");
      }
      jprintf(f, indent + 2, "}");

      if ((ev + 1)->kind != IE_NONE) {
        jprintf(f, 0, ",");
      }
      jprintf(f, 0, "\n");
    }
    jprintf(f, indent + 1, "]\n");
  } else {
    jprintf(f, 0, "]\n");
  }
  jprintf(f, indent, "}\n");
}

static void json_dump_statement(FILE* f, ST_Stmt* stmt, int indent) {
  if (stmt->kind == ST_EXPR) {
    jprintf(f, indent, "\"%s\": {\n",
            stmt->expr.has_semicolon ? "semi" : "expr");
    json_dump_expr(f, stmt->expr.expr, indent + 1, NULL, false);

    jprintf(f, indent, "}\n");
  } else {
    U_die("unhandled statement");
  }
}

static void json_dump_expr(FILE* f,
                           E_Expr* expr,
                           int indent,
                           char* overwrite_name,
                           bool end_with_comma) {
  if (expr->kind == E_ARRAY) {
    jprintf(f, indent, "\"%s\": {\n",
            overwrite_name ? overwrite_name : "array");
    jprintf(f, indent + 1, "\"elems\": [\n");
    bool first = true;
    for (E_Expr** elem = expr->array.elems; (*elem)->kind != E_NONE; elem++) {
      if (!first) {
        fprintf(f, ",\n");
      }
      first = false;

      jprintf(f, indent + 2, "{\n");
      json_dump_expr(f, *elem, indent + 3, NULL, false);
      jprintf(f, indent + 2, "}");
    }
    if (!first) {
      fprintf(f, "\n");
    }

    jprintf(f, indent + 1, "]\n");

  } else if (expr->kind == E_ASSIGN) {
    jprintf(f, indent, "\"%s\": {\n",
            overwrite_name ? overwrite_name : "assign");
    jprintf(f, indent + 1, "\"left\": {\n");
    json_dump_expr(f, expr->assign.left, indent + 2, NULL, false);
    jprintf(f, indent + 1, "},\n");
    jprintf(f, indent + 1, "\"right\": {\n");
    json_dump_expr(f, expr->assign.right, indent + 2, NULL, false);
    jprintf(f, indent + 1, "}\n");

  } else if (expr->kind == E_AWAIT) {
    jprintf(f, indent, "\"%s\": {\n",
            overwrite_name ? overwrite_name : "await");
    jprintf(f, indent + 1, "\"base\": {\n");
    json_dump_expr(f, expr->await.expr, indent + 2, NULL, false);
    jprintf(f, indent + 1, "}\n");

  } else if (expr->kind == E_BINARY) {
    jprintf(f, indent, "\"%s\": {\n",
            overwrite_name ? overwrite_name : "binary");
    jprintf(f, indent + 1, "\"left\": {\n");
    json_dump_expr(f, expr->binary.left, indent + 2, NULL, false);
    jprintf(f, indent + 1, "},\n");
    jprintf(f, indent + 1, "\"op\": \"%s\",\n", T_desc(expr->binary.op));
    jprintf(f, indent + 1, "\"right\": {\n");
    json_dump_expr(f, expr->binary.right, indent + 2, NULL, false);
    jprintf(f, indent + 1, "}\n");

  } else if (expr->kind == E_CALL) {
    jprintf(f, indent, "\"%s\": {\n", overwrite_name ? overwrite_name : "call");

    jprintf(f, indent + 1, "\"func\": {\n");
    json_dump_expr(f, expr->call.func, indent + 2, NULL, false);
    jprintf(f, indent + 1, "},\n");

    jprintf(f, indent + 1, "\"args\": [");

    bool first = true;
    for (E_Expr** arg = expr->call.args; (*arg)->kind != E_NONE; arg++) {
      if (first) {
        fprintf(f, "\n");
      } else {
        fprintf(f, ",\n");
      }
      first = false;

      jprintf(f, indent + 2, "{\n");
      json_dump_expr(f, *arg, indent + 3, NULL, false);
      jprintf(f, indent + 2, "}");
    }

    if (first) {
      fprintf(f, "]\n");
    } else {
      fprintf(f, "\n");
      jprintf(f, indent + 1, "]\n");
    }
  } else if (expr->kind == E_CAST) {
    jprintf(f, indent, "\"%s\": {\n", overwrite_name ? overwrite_name : "cast");

    jprintf(f, indent + 1, "\"expr\": {\n");
    json_dump_expr(f, expr->cast.expr, indent + 2, NULL, false);
    jprintf(f, indent + 1, "},\n");

    json_dump_type(f, expr->cast.type, indent + 1, NULL, false);

  } else if (expr->kind == E_FIELD) {
    jprintf(f, indent, "\"%s\": {\n",
            overwrite_name ? overwrite_name : "field");

    jprintf(f, indent + 1, "\"base\": {\n");
    json_dump_expr(f, expr->field.expr, indent + 2, NULL, false);
    jprintf(f, indent + 1, "},\n");

    if (expr->field.member.kind == MEM_NAME) {
      jprintf(f, indent + 1, "\"ident\": \"%s\"\n", expr->field.member.name);
    } else {
      jprintf(f, indent + 1, "\"index\": %lu\n", expr->field.member.tuple);
    }

  } else if (expr->kind == E_FOR_LOOP) {
    jprintf(f, indent, "\"%s\": {\n",
            overwrite_name ? overwrite_name : "for_loop");

    json_dump_pattern(f, expr->for_loop.pat, indent + 1);
    fprintf(f, ",\n");

    jprintf(f, indent + 1, "\"expr\": {\n");

    json_dump_expr(f, expr->for_loop.expr, indent + 2, NULL, false);

    jprintf(f, indent + 1, "},\n");

    jprintf(f, indent + 1, "\"body\": [\n");
    bool first = true;
    for (ST_Stmt* stmt = expr->for_loop.stmts; stmt->kind != ST_NONE; stmt++) {
      if (!first) {
        fprintf(f, ",\n");
      }
      first = false;

      jprintf(f, indent + 2, "{\n");
      json_dump_statement(f, stmt, indent + 3);
      jprintf(f, indent + 2, "}");
    }

    if (!first) {
      fprintf(f, "\n");
    }

    jprintf(f, indent + 1, "]\n");

  } else if (expr->kind == E_LIT) {
    jprintf(f, indent, "\"%s\": {\n", overwrite_name ? overwrite_name : "lit");
    if (expr->lit.kind == EL_INT) {
      jprintf(f, indent + 1, "\"int\": ");
    } else {
      U_die("unhandled lit");
    }

    fprintf(f, "\"%s\"\n", expr->lit.lit);

  } else if (expr->kind == E_PAREN) {
    jprintf(f, indent, "\"%s\": {\n",
            overwrite_name ? overwrite_name : "paren");

    jprintf(f, indent + 1, "\"expr\": {\n");
    json_dump_expr(f, expr->paren.expr, indent + 2, NULL, false);

    jprintf(f, indent + 1, "}\n");

  } else if (expr->kind == E_PATH) {
    jprintf(f, indent, "\"%s\": {\n", overwrite_name ? overwrite_name : "path");
    json_dump_path(f, &expr->path.path, indent + 1);

  } else if (expr->kind == E_RANGE) {
    jprintf(f, indent, "\"%s\": {\n",
            overwrite_name ? overwrite_name : "range");

    jprintf(f, indent + 1, "\"from\": {\n");
    json_dump_expr(f, expr->range.left, indent + 2, NULL, false);
    jprintf(f, indent + 1, "},\n");

    jprintf(f, indent + 1, "\"limits\": \"%s\",\n",
            expr->range.is_closed ? "..=" : "..");

    jprintf(f, indent + 1, "\"to\": {\n");
    json_dump_expr(f, expr->range.right, indent + 2, NULL, false);
    jprintf(f, indent + 1, "}\n");

  } else if (expr->kind == E_TUPLE) {
    jprintf(f, indent, "\"%s\": {\n",
            overwrite_name ? overwrite_name : "tuple");
    jprintf(f, indent + 1, "\"elems\": [\n");

    bool first = true;
    for (E_Expr** elem = expr->tuple.elems; (*elem)->kind != E_NONE; elem++) {
      if (!first) {
        fprintf(f, ",\n");
      }
      first = false;

      jprintf(f, indent + 2, "{\n");
      json_dump_expr(f, *elem, indent + 3, NULL, false);
      jprintf(f, indent + 2, "}");
    }
    fprintf(f, "\n");

    jprintf(f, indent + 1, "]\n");

  } else {
    U_die("unhandled expr kind %d", expr->kind);
  }

  jprintf(f, indent, "}%s\n", end_with_comma ? "," : "");
}

static void json_dump_fn(FILE* f,
                         const I_Signature* sig,
                         ST_Stmt* stmts,
                         bool is_method,
                         int indent) {
  jprintf(f, indent, "\"%s\": {\n", is_method ? "method" : "fn");

  jprintf(f, indent + 1, "\"ident\": \"%s\",\n", sig->name);

  if (sig->generics.items->kind != GI_NONE) {
    json_dump_generics(f, &sig->generics, indent + 1);
  }

  jprintf(f, indent + 1, "\"inputs\": [");
  bool first = true;
  for (I_FnArg* arg = sig->fn_args; arg->kind != FNARG_NONE; arg++) {
    if (!first) {
      fprintf(f, ",\n");
    } else {
      fprintf(f, "\n");
    }
    first = false;

    jprintf(f, indent + 2, "{\n");

    if (arg->kind == FNARG_RECEIVER) {
      jprintf(f, indent + 3, "\"receiver\": {}\n");
    } else if (arg->kind == FNARG_TYPED) {
      jprintf(f, indent + 3, "\"typed\": {\n");
      json_dump_pattern(f, arg->typed.pat, indent + 4);
      fprintf(f, ",\n");
      json_dump_type(f, arg->typed.ty, indent + 4, NULL, false);
      jprintf(f, indent + 3, "}\n");
    } else {
      U_die("unhandled fn arg kind %d", arg->kind);
    }

    jprintf(f, indent + 2, "}");
  }
  if (!first) {
    fprintf(f, "\n");
    U_indent(f, indent + 1);
  }
  fprintf(f, "],\n");

  if (sig->return_type == NULL) {
    jprintf(f, indent + 1, "\"output\": null");
  } else {
    jprintf(f, indent + 1, "\"output\": {\n");

    if (sig->return_type->kind == TY_PATH) {
      jprintf(f, indent + 2, "\"path\": {\n");
      json_dump_path(f, &sig->return_type->path.path, indent + 3);
      jprintf(f, indent + 2, "}\n");
    } else {
      U_die("unhandled return type");
    }

    jprintf(f, indent + 1, "}");
  }

  if (stmts != NULL) {
    fprintf(f, ",\n");
    jprintf(f, indent + 1, "\"stmts\": [");
    first = true;
    for (ST_Stmt* stmt = stmts; stmt->kind != ST_NONE; stmt++) {
      if (!first) {
        fprintf(f, ",\n");
      } else {
        fprintf(f, "\n");
      }
      first = false;

      jprintf(f, indent + 2, "{\n");

      json_dump_statement(f, stmt, indent + 3);

      jprintf(f, indent + 2, "}");
    }
    if (!first) {
      fprintf(f, "\n");
      U_indent(f, indent + 1);
    }
    fprintf(f, "]\n");
  } else {
    fprintf(f, "\n");
  }

  jprintf(f, indent, "}\n");
}

static void json_dump_trait(FILE* f, const I_Item* item, int indent) {
  jprintf(f, indent, "\"trait\": {\n");

  jprintf(f, indent + 1, "\"ident\": \"%s\",\n", item->trait.name);

  if (item->trait.generics.items->kind != GI_NONE) {
    json_dump_generics(f, &item->trait.generics, indent + 1);
  }

  jprintf(f, indent + 1, "\"items\": [");

  bool first = true;
  for (I_TraitItem* ti = item->trait.items; ti->kind != TI_NONE; ti++) {
    if (!first) {
      fprintf(f, ",\n");
    } else {
      fprintf(f, "\n");
    }
    first = false;

    jprintf(f, indent + 2, "{\n");

    if (ti->kind == TI_FN) {
      json_dump_fn(f, &ti->fn.sig, ti->fn.defaults, true, indent + 3);
    }

    jprintf(f, indent + 2, "}");
  }

  if (!first) {
    fprintf(f, "\n");
    jprintf(f, indent + 1, "]\n");
  } else {
    fprintf(f, "]\n");
  }

  jprintf(f, indent, "}\n");
}

static const char* json_dump_foreign_mod_abi_escaped(const char* source) {
  // We just cheat for now and hardcode the string because otherwise we would
  // need to deal with properly allocating buffers and such.
  // This will likely only ever be "C"
  // but just rewrite it to properly escape things if that's necessary.
  if (source[0] == '"' && source[1] == 'C' && source[2] == '"' &&
      source[3] == '\0') {
    return "\\\"C\\\"";
  }

  U_die("invalid foreign mod abi escaped");
}

static void json_dump_foreign_mod(FILE* f, const I_Item* item, int indent) {
  jprintf(f, indent, "\"foreign_mod\": {\n");

  jprintf(f, indent + 1, "\"abi\": {\n");
  jprintf(f, indent + 2, "\"name\": \"%s\"\n",
          json_dump_foreign_mod_abi_escaped(item->foreign_mod.abi));
  jprintf(f, indent + 1, "},\n");

  jprintf(f, indent + 1, "\"items\": [\n");

  bool first = true;
  for (I_ForeignItem* fi = item->foreign_mod.items; fi->kind != FI_NONE; fi++) {
    if (!first) {
      fprintf(f, ",\n");
    }
    first = false;

    jprintf(f, indent + 2, "{\n");
    if (fi->kind == FI_FN) {
      json_dump_fn(f, &fi->fn.sig, NULL, false, indent + 3);
    } else {
      U_die("unhandled foreign item");
    }
    jprintf(f, indent + 2, "}");
  }
  fprintf(f, "\n");

  jprintf(f, indent + 1, "]\n");

  jprintf(f, indent, "}\n");
}

static void json_dump_impl(FILE* f, const I_Item* item, int indent) {
  jprintf(f, indent, "\"impl\": {\n");

  json_dump_type(f, item->impl.self_ty, indent + 1, "self_ty", true);

  jprintf(f, indent + 1, "\"items\": [\n");

  bool first = true;
  for (I_ImplItem* ii = item->impl.items; ii->kind != II_NONE; ii++) {
    if (!first) {
      fprintf(f, ",\n");
    }
    first = false;

    jprintf(f, indent + 2, "{\n");
    if (ii->kind == II_FN) {
      json_dump_fn(f, &ii->fn.sig, ii->fn.stmts, true, indent + 3);
    } else {
      U_die("unhandled impl item");
    }
    jprintf(f, indent + 2, "}");
  }
  fprintf(f, "\n");

  jprintf(f, indent + 1, "]\n");

  jprintf(f, indent, "}\n");
}

static void json_dump_item(FILE* f, const I_Item* item, int indent) {
  jprintf(f, indent, "{\n");

  switch (item->kind) {
    case I_FN:
      json_dump_fn(f, &item->fn.sig, item->fn.stmts, false, indent + 1);
      break;

    case I_FOREIGN_MOD:
      json_dump_foreign_mod(f, item, indent + 1);
      break;

    case I_IMPL:
      json_dump_impl(f, item, indent + 1);
      break;

    case I_TRAIT:
      json_dump_trait(f, item, indent + 1);
      break;

    case I_ITEM_STRUCT:
    case I_TUPLE_STRUCT:
    case I_UNIT_STRUCT:
      json_dump_struct(f, item, indent + 1);
      break;
    case I_ENUM:
      json_dump_enum(f, item, indent + 1);
      break;

    default:
      U_die("unhandled item kind %d", item->kind);
  }

  jprintf(f, indent, "}");
}

void JSON_dump(FILE* f, const P_SourceFile* source_file) {
  fprintf(f, "{\n");

  int indent = 1;
  jprintf(f, indent, "\"items\": [\n");

  bool first = true;
  for (I_Item** item = source_file->items; (*item)->kind != I_NONE; item++) {
    if (!first) {
      fprintf(f, ",\n");
    }
    first = false;

    json_dump_item(f, *item, indent + 1);
  }
  fprintf(f, "\n");

  jprintf(f, indent, "]\n");

  fprintf(f, "}");
}
