// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

// Entry point for QBE backend.

#include "dir.h"
#include "dozer.h"
#include "dozerrt/libdozer.h"
#include "nhad/nhad.h"
#include "util/util.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef __TINYC__
#define alignof(x) __alignof__(x)
#else
#include <stdalign.h>
#endif

char* dozer_cratename = NULL;

// TODO: Support platforms we aren't running on top of.
#define PTR (sizeof(size_t) == 4 ? "w" : "l")
#define DZOBJ_SIZE sizeof(DZ_Object)
#define DZOBJ_ALIGN alignof(DZ_Object)

#define BASE_TYPE(f, tr)            \
  if (!emit_base_type(f, tr)) {     \
    fprintf(stderr, "\n");          \
    U_die("no base type for type"); \
  }

static void usage() {
  fprintf(stderr, "usage: %s [filename]\n", program_name);
  exit(1);
}

static void assert_type(const G_Sym* sym) {
  if (!sym)
    U_die("symbol is NULL");
  if (sym->kind != GS_TYPE)
    U_die("symbol is note type");
  if (!sym->type)
    U_die("symbol has no type");
}

static bool emit_base_type(FILE* f, const G_Sym* sym) {
  assert_type(sym);

  switch (sym->type->kind) {
    case GT_PRIMITIVE:
      if (sym->type->primitive.is_float && sym->type->size <= 4)
        fprintf(f, "s");
      else if (sym->type->primitive.is_float && sym->type->size <= 8)
        fprintf(f, "d");
      else if (sym->type->size <= 4)
        fprintf(f, "w");
      else if (sym->type->size <= 8)
        fprintf(f, "l");
      else
        return false;
      break;
    case GT_REFERENCE:
    case GT_FNPTR:
      fprintf(f, PTR);
      break;
    default:
      return false;
  }

  return true;
}

static bool emit_ext_type(FILE* f, const G_Sym* sym) {
  assert_type(sym);

  switch (sym->type->kind) {
    case GT_PRIMITIVE:
      if (sym->type->size == 0)
        fprintf(f, "w");
      else if (sym->type->size <= 1)
        fprintf(f, "b");
      else if (sym->type->size <= 2)
        fprintf(f, "h");
      else
        return emit_base_type(f, sym);
      break;
    default:
      return emit_base_type(f, sym);
  }

  return true;
}

static void emit_abi_type(FILE* f, const G_Sym* sym) {
  assert_type(sym);

  switch (sym->type->kind) {
    default:
      if (!emit_ext_type(f, sym)) {
        U_die("no ABI type for this type: %d", sym->type->kind);
      }
  }
}

static const char* literal_load(const G_Sym* sym) {
  assert_type(sym);

  switch (sym->type->size) {
    case 1:
      if (sym->type->primitive.is_signed)
        return "extsb";
      else
        return "extub";
    case 2:
      if (sym->type->primitive.is_signed)
        return "extsw";
      else
        return "extuw";
    case 4:
      if (sym->type->primitive.is_float)
        return "truncd";
    default:
      return "copy";
  }
}

static bool type_is_signed(const G_Sym* sym) {
  assert_type(sym);

  if (sym->type->kind != GT_PRIMITIVE)
    return false;
  return sym->type->primitive.is_signed;
}

static void emit_binop(FILE* f, const DI_Code* code, G_Sym** regtypes) {
  // TODO: Floats and pretty much everything else.

  G_Sym* sym;

  fprintf(f, "  %%.%d =", code->args[2].reg);
  sym = regtypes[code->args[2].reg];
  BASE_TYPE(f, sym);
  fprintf(f, " ");

  switch (code->args[3].optype) {
    case T_PLUS:
      fprintf(f, "add");
      break;
    case T_MINUS:
      fprintf(f, "sub");
      break;
    case T_STAR:
      fprintf(f, "mul");
      break;
    case T_SLASH:
      if (type_is_signed(sym))
        fprintf(f, "div");
      else
        fprintf(f, "udiv");
      break;
    case T_PERCENT:
      if (type_is_signed(sym))
        fprintf(f, "rem");
      else
        fprintf(f, "urem");
      break;
    case T_CARET:
      fprintf(f, "xor");
      break;
    case T_AND:
      fprintf(f, "and");
      break;
    case T_OR:
      fprintf(f, "or");
      break;
    case T_SHL:
      fprintf(f, "shl");
      break;
    case T_SHR:
      fprintf(f, "shr");
      break;
    default:
      U_die("invalid binop in bytecode");
  }

  fprintf(f, " %%.%d, %%.%d\n", code->args[0].reg, code->args[1].reg);
}

static void emit_code(FILE* f, const DI_Code* code, G_Sym** regtypes) {
  G_Sym* sym;
  uint32_t i;

  switch (code->opcode) {
    case DI_NOP:
      break;
    case DI_LOAD:
      sym = regtypes[code->args[0].reg];

      fprintf(f, "  %%.%d =", code->args[0].reg);
      BASE_TYPE(f, sym);
      fprintf(f, " %s %ld\n", literal_load(sym), code->args[1].literal);

      break;
    case DI_BINOP:
      emit_binop(f, code, regtypes);
      break;
    case DI_GLOBAL:
      fprintf(f, "  %%.%d =", code->args[0].reg);
      BASE_TYPE(f, regtypes[code->args[0].reg]);
      fprintf(f, " copy $%s\n", code->args[1].name);
      break;
    case DI_RET:
      fprintf(f, "  ret %%.%d\n", code->args[0].reg);
      break;
    case DI_RETNONE:
      fputs("  ret\n", f);
      break;
    case DI_CALL:
    case DI_CALLRET:
      fprintf(f, "  %%.%d =", code->args[2].reg);
      emit_abi_type(f, regtypes[code->args[2].reg]);
      if (code->opcode == DI_CALLRET)
        fprintf(f, " ");
      else
        fprintf(f, " copy 0\n  ");
      fprintf(f, "call %%.%d (", code->args[0].reg);
      for (i = 0; i < code->args[1].regs.len; i++) {
        emit_abi_type(f, regtypes[code->args[1].regs.items[i]]);
        fprintf(f, " %%.%d, ", code->args[1].regs.items[i]);
      }
      fputs(")\n", f);
      break;
    default:
      U_die("bad opcode: %d", code->opcode);
  }
}

// clang-format off
struct {
  char escape;
  char value;
} escapes[] = {
  { .escape = '0', .value = 0 },
  { .escape = 'n', .value = '\n' },
  { .escape = 't', .value = '\t' },
  { .escape = '\'', .value = '\'' },
  { .escape = '"', .value = '"' },
  { .escape = '\\', .value = '\\' }
};
// clang-format on

static char escape_value(char escape) {
  size_t i;

  for (i = 0; i < sizeof(escapes) / sizeof(escapes[0]); i++)
    if (escape == escapes[i].escape)
      return escapes[i].value;

  U_die("invalid escape: %c", escape);
}

static void emit_string(FILE* f,
                        const char* name,
                        const uint8_t* data,
                        size_t len) {
  size_t i;
  uint8_t c;
  enum { OutsideString, InAsciiString } state = OutsideString;

  fprintf(f, "data $%s = {\n", name);

  for (i = 0; i < len; ++i) {
    c = data[i];
    // For ASCII-like strings, just emit ASCII.
    if (isprint(data[i]) && c != '"' && c != '\\') {
      if (state == OutsideString) {
        // Start a new ASCII string.
        fprintf(f, "  b \"");
        state = InAsciiString;
      }
      fprintf(f, "%c", c);
      continue;
    }

    // End the ASCII string if we are inside one.
    if (state == InAsciiString) {
      fprintf(f, "\",\n");
      state = OutsideString;
    }

    fprintf(f, "  b %d,\n", c);
  }

  if (state == InAsciiString)
    fprintf(f, "\",\n");

  fprintf(f, "}\n");
}

static G_Sym* read_abi_sym(B_Reader* reader) {
  PT_Path path;
  G_Sym* sym;

  if (!PT_readpath(reader, &path))
    U_die("failed to read path for ABI type");
  sym = G_lookup(NULL, &path);
  if (!sym) {
    PT_dump(stderr, &path);
    U_die("failed to lookup symbol");
  }

  return sym;
}

static void emit_fn(FILE* f, const char* name, uint8_t* data, size_t len) {
  DI_Code code;
  uint32_t register_count, arg_count, ret, i;
  B_Reader reader;
  PT_Path path;
  G_Sym *sym, **syms, **regtypes;

  regtypes = U_mkarray(NULL, register_count, sizeof(*regtypes));
  B_mkreader(&reader, data, len);

  // Read the total number of registers.
  if (!(B_read32(&reader, &register_count) && B_read32(&reader, &arg_count) &&
        B_read32(&reader, &ret)))
    U_die("invalid function format");

  // Read in the types of the registers (first arg_count registers are the
  // arguments).
  for (i = 0; i < register_count; i++)
    U_addptr(&regtypes, read_abi_sym(&reader));
  syms = regtypes;

  fprintf(f, "function ");
  if (ret) {
    emit_abi_type(f, read_abi_sym(&reader));
  }

  fprintf(f, " $%s(", name);
  for (i = 0; i < arg_count; i++) {
    emit_abi_type(f, syms[i]);
    fprintf(f, " %%.%d, ", i);
  }

  fputs(") {\n", f);

  fputs("@start\n", f);

  // Emit opcodes.
  while (DI_readcode(&reader, &code)) {
    emit_code(f, &code, syms);
    DI_free(code);
  }

  fputs("}\n", f);
}

static char* crate_name(N_Nhad* input) {
  N_Lump* lump;
  char* data;

  lump = N_entry(input, "CRATENAME", NULL);
  data = U_xmalloc(lump->size + 1);
  memcpy(data, N_load(input, lump), lump->size);
  data[lump->size] = '\0';

  dozer_cratename = data;
  return data;
}

static void readsymtab(N_Nhad* input) {
  N_Lump* lump;
  void* data;
  B_Reader reader;

  lump = N_entry(input, "SYMTAB", NULL);
  B_mkreader(&reader, N_load(input, lump), lump->size);

  G_init(false);
  if (!G_readsyms(&reader))
    U_die("parsing failed for symbol table");
}

static void emitall(FILE* f, N_Nhad* input) {
  const N_Lump* lump;
  uint8_t* data;
  size_t i;
  char *cratename, *mangled;
  PT_Path path;

  cratename = crate_name(input);
  readsymtab(input);

  // Emit all strings.
  for (i = 0; i < N_lumpcount(input); i++) {
    lump = N_lumpat(input, i);
    if (strncmp(lump->name, "STR_", strlen("STR_")) == 0) {
      data = N_load(input, lump);
      emit_string(f, lump->name, data, lump->size);
      fputs("\n", stdout);
    }
  }

  // Emit items data in this file.
  for (i = 0; i < N_lumpcount(input); i++) {
    lump = N_lumpat(input, i);
    if (strncmp(lump->name, "FN_", strlen("FN_")) == 0) {
      data = N_load(input, lump);
      emit_fn(f, lump->name + strlen("FN_"), data, lump->size);
      fputs("\n", stdout);
    }
  }

  // Get the path to this crate's rust_main.
  path = PT_single(cratename, true);
  PT_pushseg(&path, "rust_main");
  mangled = PT_mangle(&path);
  // PT_free(path); TODO

  // Emit the main function.
  fputs("export function w $main() {\n", f);
  fputs("@start\n", f);

  // Call the "rust_main" function.
  // TODO: Make this lang item start.
  fprintf(f, "  %%exit_code =w call $%s()\n", mangled);

  // TODO: Call Termination::exit_code()
  fputs("  ret %exit_code\n", f);

  fputs("}\n", f);

  free(cratename);
  free(mangled);
  G_free();
}

int main(int argc, char** argv) {
  N_Nhad* input;

  program_name = U_parse_program_name(argv[0]);
  if (!program_name)
    program_name = "dozer-qbe";

  if (argc < 2)
    usage();

  parsearena = U_mkarena();
  input = N_open(argv[1]);
  emitall(stdout, input);
  N_free(input);
  // U_freeall(parsearena);

  return 0;
}
