// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

#include "dozer.h"
#include "util/util.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define cur P_cur(parser)

PTR_BUFFER_FUNCS(trait_item, I_TraitItem, TI_NONE)

static I_Vis vis_read(P_Parser* parser) {
  I_Vis vis;

  if (!P_consume(parser, T_PUB)) {
    vis.kind = VIS_INHERITED;
    return vis;
  }

  if (P_consume(parser, T_LPAREN))
    U_die("TODO: restricted vis");

  vis.kind = VIS_PUBLIC;
  return vis;
}

static void parse_generic_item(P_Parser* parser, I_GenericItem* item) {
  switch (cur->kind) {
    case T_IDENT:
      // TODO Bounds
      item->kind = GI_TYPE;
      item->type.name = strdup(cur->literal);
      P_next(parser);
      break;

    case T_LIFETIME:
      item->kind = GI_LIFETIME;
      item->lifetime.name = strdup(cur->literal);
      P_next(parser);
      break;

    case T_CONST:
      item->kind = GI_CONST;
      P_next(parser);
      item->const_.name =
          strdup(P_expect(parser, T_IDENT, "expected const parameter"));
      P_expect(parser, T_COLON, "expected `:`");
      item->const_.type = TY_read(parser);
      break;
  }

  if (!P_consume(parser, T_COMMA) && cur->kind != T_GT)
    T_error(&cur->loc, "expected `,` or `>`");
}

static void parse_generics(P_Parser* parser, I_Generics* generics) {
  I_GenericItem item, *items;

  items = U_pmkarray(2, sizeof(*items));

  if (P_consume(parser, T_LT)) {
    while (!P_consume(parser, T_GT)) {
      parse_generic_item(parser, &item);
      U_addmem(&items, &item, 1);
    }
  }

  item.kind = GI_NONE;
  U_addmem(&items, &item, sizeof(item));
  generics->items = items;
}

static bool parse_receiver(P_Parser* original, I_FnArg* arg) {
  TY_Type* type;
  P_Parser* parser = P_fork(original);

  arg->kind = FNARG_RECEIVER;
  if (P_consume(parser, T_AND)) {
    arg->receiver.is_reference = true;
    if (cur->kind == T_LIFETIME) {
      arg->receiver.lifetime = strdup(cur->literal);
    } else
      arg->receiver.lifetime = NULL;
  } else {
    arg->receiver.is_reference = false;
    arg->receiver.lifetime = NULL;
  }

  arg->receiver.is_mut = P_consume(parser, T_MUT);
  if (!P_consume(parser, T_SELFVALUE)) {
    P_freeparser(parser);
    return false;
  }

  if (!arg->receiver.is_reference && P_consume(parser, T_COLON)) {
    type = TY_read(parser);
  } else {
    type = U_palloc(sizeof(TY_Type));
    type->sym = NULL;
    type->kind = TY_PATH;
    type->path.qself.position = -1;
    type->path.path = PT_single("Self", false);

    if (arg->receiver.is_reference)
      U_die("TODO: ref receiver");
  }
  arg->receiver.ty = type;

  P_unfork(original, parser);
  return true;
}

static I_FnArg* parse_fn_args(P_Parser* parser,
                              bool* is_variadic,
                              PN_Pat** variadic) {
  I_FnArg fnarg, *buffer;
  bool has_receiver = false;

  buffer = U_pmkarray(1, sizeof(*buffer));
  *is_variadic = false;
  *variadic = NULL;

  P_expect(parser, T_LPAREN, "expected left paren");

  while (cur->kind != T_RPAREN) {
    // TODO: Parse attributes

    if (P_consume(parser, T_DOTDOTDOT)) {
      *is_variadic = true;
      break;
    } else if (parse_receiver(parser, &fnarg)) {
      if (has_receiver)
        T_error(&cur->loc, "unexpected second receiver");
      else if (U_len(buffer) != 0)
        T_error(&cur->loc, "unexpected method receiver");
      else
        has_receiver = true;

      U_addmem(&buffer, &fnarg, 1);
    } else {
      // TODO: Parse ident then LT token

      fnarg.typed.pat = PN_read_one(parser);
      P_expect(parser, T_COLON, "expected `:`");

      // This may be a variadic.
      if (P_consume(parser, T_DOTDOTDOT)) {
        *is_variadic = true;
        *variadic = fnarg.typed.pat;
        break;
      }

      fnarg.kind = FNARG_TYPED;
      fnarg.typed.ty = TY_read(parser);
      U_addmem(&buffer, &fnarg, 1);
    }

    if (cur->kind == T_RPAREN)
      break;

    P_expect(parser, T_COMMA, "expected comma or right paren");
  }

  P_next(parser);
  fnarg.kind = FNARG_NONE;
  U_addmem(&buffer, &fnarg, 1);
  return buffer;
}

static I_Signature sig_read(P_Parser* parser) {
  I_Signature sig;

  sig.is_const = P_consume(parser, T_CONST);
  sig.is_async = P_consume(parser, T_ASYNC);
  sig.is_unsafe = P_consume(parser, T_UNSAFE);

  sig.is_extern = P_consume(parser, T_EXTERN);
  if (sig.is_extern)
    sig.abi = strdup(P_expect(parser, T_STRING_LITERAL, "expected ABI name"));
  else
    sig.abi = NULL;

  P_expect(parser, T_FN, "expected 'fn' token");
  sig.name = strdup(P_expect(parser, T_IDENT, "expected function name"));

  parse_generics(parser, &sig.generics);
  sig.fn_args = parse_fn_args(parser, &sig.has_variadic, &sig.variadic);
  sig.return_type = TY_returntype_read(parser);
  // TODO: where clause

  return sig;
}

static bool is_signature(P_Parser* parser) {
  switch (cur->kind) {
    case T_CONST:
    case T_ASYNC:
    case T_UNSAFE:
    case T_EXTERN:
    case T_FN:
      return true;
    default:
      return false;
  }
}

static void parse_trait_item(P_Parser* parser, I_TraitItem* ti) {
  I_Vis vis = vis_read(parser);

  if (is_signature(parser)) {
    ti->kind = TI_FN;
    ti->fn.sig = sig_read(parser);

    if (cur->kind == T_LBRACE)
      ti->fn.defaults = ST_block(parser);
    else {
      P_expect(parser, T_SEMI, "expected semicolon");
      ti->fn.defaults = NULL;
    }
  } else
    T_error(&cur->loc, "expected trait item");
}

static void parse_start_of_trait(P_Parser* parser, I_Item* item) {
  item->trait.name = strdup(P_expect(parser, T_IDENT, "expected trait id"));
  parse_generics(parser, &item->trait.generics);
}

static void parse_rest_of_trait(P_Parser* parser, I_Item* item) {
  I_TraitItem ti, *array;

  array = U_pmkarray(1, sizeof(*array));

  if (P_consume(parser, T_COLON))
    U_die("TODO: supertraits");

  // TODO: Where clause
  P_expect(parser, T_LBRACE, "expected left brace");
  // TODO: Inner attributes.
  while (!P_consume(parser, T_RBRACE)) {
    parse_trait_item(parser, &ti);
    U_addmem(&array, &ti, 1);
  }

  ti.kind = TI_NONE;
  U_addmem(&array, &ti, 1);

  item->trait.items = array;
}

static void parse_trait_or_trait_alias(P_Parser* parser,
                                       I_Item* item,
                                       I_Vis vis) {
  parse_start_of_trait(parser, item);

  switch (cur->kind) {
    case T_LBRACE:
    case T_COLON:
    case T_WHERE:
      // Parse a trait.
      item->kind = I_TRAIT;
      item->trait.vis = vis;
      parse_rest_of_trait(parser, item);
      break;
    case T_EQ:
      U_die("TODO: parse trait alias");
      break;
    default:
      T_error(&cur->loc, "expected trait start");
  }
}

static void parse_item_struct_field(P_Parser* parser, I_StructField* sf) {
  sf->name = strdup(P_expect(parser, T_IDENT, "expected field identifier"));
  P_expect(parser, T_COLON, "expected `:`");
  sf->type = TY_read(parser);
  P_consume(parser, T_COMMA);
}

static void parse_item_struct(P_Parser* parser, I_Item* item) {
  I_StructField sf, *array;

  array = U_pmkarray(1, sizeof(*array));

  P_expect(parser, T_LBRACE, "`{`");
  while (!P_consume(parser, T_RBRACE)) {
    parse_item_struct_field(parser, &sf);
    U_addmem(&array, &sf, 1);
  }

  memset(&sf, 0, sizeof(sf));
  U_addmem(&array, &sf, 1);

  item->item_struct.fields = array;
}

static void parse_tuple_struct(P_Parser* parser, I_Item* item) {
  TY_Type *t, **array;

  array = U_pmkarray(1, sizeof(*array));

  P_expect(parser, T_LPAREN, "`(`");
  while (!P_consume(parser, T_RPAREN)) {
    t = TY_read(parser);
    P_consume(parser, T_COMMA);
    U_addptr(&array, t);
  }

  P_expect(parser, T_SEMI, "expected `;`");

  U_addptr(&array, NULL);
  item->tuple_struct.fields = array;
}

static void parse_struct(P_Parser* parser, I_Item* item, I_Vis vis) {
  I_Generics generics;
  char* name = strdup(P_expect(parser, T_IDENT, "expected struct identifier"));

  parse_generics(parser, &generics);

  if (P_consume(parser, T_SEMI)) {
    item->kind = I_UNIT_STRUCT;
    item->unit_struct.vis = vis;
    item->unit_struct.name = name;
  } else if (cur->kind == T_LPAREN) {
    item->kind = I_TUPLE_STRUCT;
    item->tuple_struct.vis = vis;
    item->tuple_struct.name = name;
    item->tuple_struct.generics = generics;
    parse_tuple_struct(parser, item);
  } else if (cur->kind == T_LBRACE) {
    item->kind = I_ITEM_STRUCT;
    item->item_struct.vis = vis;
    item->item_struct.name = name;
    item->item_struct.generics = generics;
    parse_item_struct(parser, item);
  } else {
    T_error(&cur->loc, "expected struct body (one of `;`, `(`, `{`)");
  }
}

static void parse_tuple_variant(P_Parser* parser, I_EnumVariant* ev) {
  TY_Type *t, **array;

  array = U_pmkarray(1, sizeof(*array));

  P_expect(parser, T_LPAREN, "`(`");
  while (!P_consume(parser, T_RPAREN)) {
    t = TY_read(parser);
    P_consume(parser, T_COMMA);
    U_addptr(&array, t);
  }

  U_addptr(&array, NULL);
  ev->tuple_fields = array;
}

static void parse_struct_variant(P_Parser* parser, I_EnumVariant* ev) {
  I_StructField sf, *array;

  array = U_pmkarray(1, sizeof(*array));

  P_expect(parser, T_LBRACE, "`{`");
  while (!P_consume(parser, T_RBRACE)) {
    parse_item_struct_field(parser, &sf);
    U_addmem(&array, &sf, 1);
  }

  memset(&sf, 0, sizeof(sf));
  U_addmem(&array, &sf, 1);

  ev->struct_fields = array;
}

static void parse_enum_variant(P_Parser* parser,
                               I_Item* item,
                               I_EnumVariant* ev) {
  ev->name = strdup(P_expect(parser, T_IDENT, "expected variant name"));
  if (cur->kind == T_LPAREN) {
    ev->kind = IE_TUPLE;
    parse_tuple_variant(parser, ev);
  } else if (cur->kind == T_LBRACE) {
    ev->kind = IE_STRUCT;
    parse_struct_variant(parser, ev);
  } else {
    ev->kind = IE_UNIT;
  }

  if (P_consume(parser, T_EQ)) {
    ev->disc = E_read(parser);
  } else {
    ev->disc = NULL;
  }
  P_consume(parser, T_COMMA);
}

static void parse_enum(P_Parser* parser, I_Item* item, I_Vis vis) {
  I_Generics generics;
  I_EnumVariant ev, *array;
  char* name = strdup(P_expect(parser, T_IDENT, "expected enum identifier"));

  array = U_pmkarray(1, sizeof(*array));

  parse_generics(parser, &generics);

  item->kind = I_ENUM;
  item->enum_.vis = vis;
  item->enum_.name = name;
  item->enum_.generics = generics;

  P_expect(parser, T_LBRACE, "`{`");
  while (!P_consume(parser, T_RBRACE)) {
    parse_enum_variant(parser, item, &ev);
    U_addmem(&array, &ev, 1);
  }

  memset(&ev, 0, sizeof(ev));
  U_addmem(&array, &ev, 1);

  item->enum_.variants = array;
}

static void parse_foreign_item(P_Parser* parser, I_ForeignItem* fi) {
  I_Vis vis = vis_read(parser);
  fi->sym = NULL;

  if (is_signature(parser)) {
    fi->kind = FI_FN;
    fi->fn.vis = vis;
    fi->fn.sig = sig_read(parser);
    P_expect(parser, T_SEMI, "expected ';'");
  } else
    U_die("TODO: other foreign items");
}

static void parse_impl_item(P_Parser* parser, I_ImplItem* ii) {
  // TODO: defaultness
  bool is_default = false;
  I_Vis vis = vis_read(parser);

  ii->sym = NULL;

  if (is_signature(parser)) {
    ii->kind = II_FN;
    ii->fn.is_default = is_default;
    ii->fn.vis = vis;
    ii->fn.sig = sig_read(parser);
    ii->fn.stmts = ST_block(parser);
  } else {
    T_error(&cur->loc, "expected impl item");
  }
}

static void parse_extern_item(P_Parser* parser, I_Item* item, I_Vis vis) {
  I_ForeignItem fi, *items;

  items = U_pmkarray(1, sizeof(*items));

  item->kind = I_FOREIGN_MOD;

  P_expect(parser, T_EXTERN, "expected 'extern'");
  if (cur->kind == T_STRING_LITERAL) {
    item->foreign_mod.abi = strdup(cur->literal);
    P_next(parser);
  } else
    item->foreign_mod.abi = NULL;

  P_expect(parser, T_LBRACE, "expected '{'");

  // Read all foreign items.
  while (cur->kind != T_RBRACE) {
    parse_foreign_item(parser, &fi);
    U_addmem(&items, &fi, 1);
  }
  P_next(parser);
  fi.kind = FI_NONE;
  U_addmem(&items, &fi, 1);

  item->foreign_mod.items = items;
}

static void parse_fn(P_Parser* parser, I_Item* item, I_Vis vis) {
  item->kind = I_FN;
  item->fn.vis = vis;
  item->fn.sig = sig_read(parser);
  item->fn.stmts = ST_block(parser);
}

static void parse_impl(P_Parser* parser, I_Item* item) {
  TY_Type* first_type;
  I_ImplItem ii, *items;

  items = U_pmkarray(1, sizeof(*items));
  item->kind = I_IMPL;

  if (cur->kind == T_IDENT && strcmp(cur->literal, "default") != 0) {
    item->impl.is_default = true;
    P_next(parser);
  } else
    item->impl.is_default = false;

  item->impl.is_unsafe = P_consume(parser, T_UNSAFE);
  P_expect(parser, T_IMPL, "expected 'impl'");
  parse_generics(parser, &item->impl.generics);

  if (cur->kind == T_NOT && P_peek(parser)->kind != T_LBRACE) {
    P_next(parser);
    item->impl.is_not = true;
  } else
    item->impl.is_not = false;

  first_type = TY_read(parser);
  if (P_consume(parser, T_FOR)) {
    U_die("TODO: traits");
  } else {
    if (item->impl.is_not)
      T_error(&cur->loc, "invalid '!'");

    item->impl.self_ty = first_type;
    item->impl.trait.segs = NULL;
  }

  // TODO: where clause

  P_expect(parser, T_LBRACE, "expected '{'");
  items = U_pmkarray(1, sizeof(*items));
  while (cur->kind != T_RBRACE) {
    parse_impl_item(parser, &ii);
    U_addmem(&items, &ii, 1);
  }
  P_next(parser);

  ii.kind = II_NONE;
  U_addmem(&items, &ii, 1);
  item->impl.items = items;
}

I_Item* I_read(P_Parser* parser) {
  // TODO: Attributes
  I_Item* item = U_palloc(sizeof(*item));
  I_Vis vis = vis_read(parser);

  item->sym = NULL;
  item->parent = NULL;

  if (P_consume(parser, T_TRAIT)) {
    parse_trait_or_trait_alias(parser, item, vis);
  } else if (P_consume(parser, T_STRUCT)) {
    parse_struct(parser, item, vis);
  } else if (cur->kind == T_EXTERN) {
    if (P_peek(parser)->kind == T_CRATE)
      U_die("TODO: extern crate");
    else if (P_peek(parser)->kind == T_STRING_LITERAL ||
             P_peek(parser)->kind == T_LBRACE)
      parse_extern_item(parser, item, vis);
    else
      parse_fn(parser, item, vis);
  } else if (cur->kind == T_IMPL) {
    parse_impl(parser, item);
  } else if (is_signature(parser)) {
    parse_fn(parser, item, vis);
  } else if (P_consume(parser, T_ENUM)) {
    parse_enum(parser, item, vis);
  } else
    T_error(&cur->loc, "expected top-level item, got %s", T_desc(cur->kind));

  return item;
}
