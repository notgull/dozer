#ifndef DOZER_LEXER_H
#define DOZER_LEXER_H

#include <stddef.h>

typedef enum edition T_Edition;
typedef struct location T_Location;
typedef enum token_kind T_TokenKind;
typedef struct token T_Token;

// token kind
enum token_kind {
  T_NONE = 0,
  T_EOF,
  T_IDENT,
  T_INTEGER,
  T_FLOAT,
  T_LIFETIME,

  // text literals
  T_CHAR_LITERAL,
  T_BYTE_LITERAL,
  T_STRING_LITERAL,
  T_RAW_STRING_LITERAL,
  T_BYTE_STRING_LITERAL,
  T_RAW_BYTE_STRING_LITERAL,
  T_C_STRING_LITERAL,
  T_RAW_C_STRING_LITERAL,

  // punctuation
  T_PLUS,        // +
  T_MINUS,       // -
  T_STAR,        // *
  T_SLASH,       // /
  T_PERCENT,     // %
  T_CARET,       // ^
  T_NOT,         // !
  T_AND,         // &
  T_OR,          // |
  T_ANDAND,      // &&
  T_OROR,        // ||
  T_SHL,         // <<
  T_SHR,         // >>
  T_PLUSEQ,      // +=
  T_MINUSEQ,     // -=
  T_STAREQ,      // *=
  T_SLASHEQ,     // /=
  T_PERCENTEQ,   // %=
  T_CARETEQ,     // ^=
  T_ANDEQ,       // &=
  T_OREQ,        // |=
  T_SHLEQ,       // <<=
  T_SHREQ,       // >>=
  T_EQ,          // =
  T_EQEQ,        // ==
  T_NE,          // !=
  T_LT,          // <
  T_GT,          // >
  T_LE,          // <=
  T_GE,          // >=
  T_AT,          // @
  T_UNDERSCORE,  // _
  T_DOT,         // .
  T_DOTDOT,      // ..
  T_DOTDOTEQ,    // ..=
  T_DOTDOTDOT,   // ...
  T_COMMA,       // ,
  T_SEMI,        // ;
  T_COLON,       // :
  T_PATHSEP,     // ::
  T_RARROW,      // ->
  T_FATARROW,    // =>
  T_LARROW,      // <-
  T_POUND,       // #
  T_DOLLAR,      // $
  T_QUESTION,    // ?
  T_TILDE,       // ~

  // Delimiters
  T_LBRACE,    // {
  T_RBRACE,    // }
  T_LBRACKET,  // [
  T_RBRACKET,  // ]
  T_LPAREN,    // (
  T_RPAREN,    // )

  // strict 2015 keywords
  T_AS,
  T_BREAK,
  T_CONST,
  T_CONTINUE,
  T_CRATE,
  T_ELSE,
  T_ENUM,
  T_EXTERN,
  T_FALSE,
  T_FN,
  T_FOR,
  T_IF,
  T_IMPL,
  T_IN,
  T_LET,
  T_LOOP,
  T_MATCH,
  T_MOD,
  T_MOVE,
  T_MUT,
  T_PUB,
  T_REF,
  T_RETURN,
  T_SELFVALUE,
  T_SELFTYPE,
  T_STATIC,
  T_STRUCT,
  T_SUPER,
  T_TRAIT,
  T_TRUE,
  T_TYPE,
  T_UNSAFE,
  T_USE,
  T_WHERE,
  T_WHILE,

  // strict 2018 keywords
  T_ASYNC,
  T_AWAIT,
  T_DYN,

  // reserved keywords
  T_ABSTRACT,
  T_BECOME,
  T_BOX,
  T_DO,
  T_FINAL,
  T_MACRO,
  T_OVERRIDE,
  T_PRIV,
  T_TYPEOF,
  T_UNSIZED,
  T_VIRTUAL,
  T_YIELD,

  // reserved 2018 keywords
  T_TRY,

  // weak 2015 keywords
  T_MACRO_RULES,
  T_UNION,
  T_STATIC_LIFETIME
};

// Raise a tokenization error.
void T_error(const T_Location* loc, const char* fmt, ...);

// Convert a token to a keyword token.
void T_keyword(T_Token* tok, T_Edition ed);

// Get the description of a token.
const char* T_desc(T_TokenKind tk);

// location of a token in a file
struct location {
  const char* source_file;
  size_t line, col;
};

// edition
enum edition { ED_2015 = 0, ED_2018, ED_2021 };

struct token {
  T_TokenKind kind;
  T_Location loc;
  char* literal;
};
typedef struct lexer L_Lexer;

// Open or close a source file.
L_Lexer* L_mklexer(const char* source_file, T_Edition ed);
void L_freelexer(L_Lexer* lexer);

// Read a single token from a source file.
void L_lex(L_Lexer* lexer, T_Token* tok);

#endif /* DOZER_LEXER_H */
