// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

#include "dozer.h"
#include "util/util.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum token_type {
  NOK = 0,          // no keyword
  STRICT = 0x01,    // strict keyword
  RESERVED = 0x02,  // reserved keyword
  WEAK = 0x03,      // weak keyword
  E_2018 = 0x04,    // only in edition 2018
};

// clang-format off
struct {
  T_TokenKind kind;
  const char* name;
  const char* repr;
  enum token_type type;
} Tokens[] = {
  {T_NONE, "T_NONE", "", NOK},
  {T_EOF, "T_EOF", "", NOK},
  {T_IDENT, "T_IDENT", "", NOK},
  {T_INTEGER, "T_INTEGER", "", NOK},
  {T_FLOAT, "T_FLOAT", "", NOK},
  {T_LIFETIME, "T_LIFETIME", "", NOK},
  {T_CHAR_LITERAL, "T_CHAR_LITERAL", "", NOK},
  {T_BYTE_LITERAL, "T_BYTE_LITERAL", "", NOK},
  {T_STRING_LITERAL, "T_STRING_LITERAL", "", NOK},
  {T_RAW_STRING_LITERAL, "T_RAW_STRING_LITERAL", "", NOK},
  {T_BYTE_STRING_LITERAL, "T_BYTE_STRING_LITERAL", "", NOK},
  {T_RAW_BYTE_STRING_LITERAL, "T_RAW_BYTE_STRING_LITERAL", "", NOK},
  {T_C_STRING_LITERAL, "T_C_STRING_LITERAL", "", NOK},
  {T_RAW_C_STRING_LITERAL, "T_RAW_C_STRING_LITERAL", "", NOK},
  {T_PLUS, "T_PLUS", "+", NOK},
  {T_MINUS, "T_MINUS", "-", NOK},
  {T_STAR, "T_STAR", "*", NOK},
  {T_SLASH, "T_SLASH", "/", NOK},
  {T_PERCENT, "T_PERCENT", "%", NOK},
  {T_CARET, "T_CARET", "^", NOK},
  {T_NOT, "T_NOT", "!", NOK},
  {T_AND, "T_AND", "&", NOK},
  {T_OR, "T_OR", "|", NOK},
  {T_ANDAND, "T_ANDAND", "&&", NOK},
  {T_OROR, "T_OROR", "||", NOK},
  {T_SHL, "T_SHL", "<<", NOK},
  {T_SHR, "T_SHR", ">>", NOK},
  {T_PLUSEQ, "T_PLUSEQ", "+=", NOK},
  {T_MINUSEQ, "T_MINUSEQ", "-=", NOK},
  {T_STAREQ, "T_STAREQ", "*=", NOK},
  {T_SLASHEQ, "T_SLASHEQ", "/=", NOK},
  {T_PERCENTEQ, "T_PERCENTEQ", "%=", NOK},
  {T_CARETEQ, "T_CARETEQ", "^=", NOK},
  {T_ANDEQ, "T_ANDEQ", "&=", NOK},
  {T_OREQ, "T_OREQ", "|=", NOK},
  {T_SHLEQ, "T_SHLEQ", "<<=", NOK},
  {T_SHREQ, "T_SHREQ", ">>=", NOK},
  {T_EQ, "T_EQ", "=", NOK},
  {T_EQEQ, "T_EQEQ", "==", NOK},
  {T_NE, "T_NE", "!=", NOK},
  {T_LT, "T_LT", "<", NOK},
  {T_GT, "T_GT", ">", NOK},
  {T_LE, "T_LE", "<=", NOK},
  {T_GE, "T_GE", ">=", NOK},
  {T_AT, "T_AT", "@", NOK},
  {T_UNDERSCORE, "T_UNDERSCORE", "_", NOK},
  {T_DOT, "T_DOT", ".", NOK},
  {T_DOTDOT, "T_DOTDOT", "..", NOK},
  {T_DOTDOTEQ, "T_DOTDOTEQ", "..=", NOK},
  {T_DOTDOTDOT, "T_DOTDOTDOT", "...", NOK},
  {T_COMMA, "T_COMMA", ",", NOK},
  {T_SEMI, "T_SEMI", ";", NOK},
  {T_COLON, "T_COLON", ":", NOK},
  {T_PATHSEP, "T_PATHSEP", "::", NOK},
  {T_RARROW, "T_RARROW", "->", NOK},
  {T_FATARROW, "T_FATARROW", "=>", NOK},
  {T_LARROW, "T_LARROW", "<-", NOK},
  {T_POUND, "T_POUND", "#", NOK},
  {T_DOLLAR, "T_DOLLAR", "$", NOK},
  {T_QUESTION, "T_QUESTION", "?", NOK},
  {T_TILDE, "T_TILDE", "~", NOK},
  {T_LBRACE, "T_LBRACE", "{", NOK},
  {T_RBRACE, "T_RRBACE", "}", NOK},
  {T_LBRACKET, "T_LBRACKET", "[", NOK},
  {T_RBRACKET, "T_RBRACKET", "]", NOK},
  {T_LPAREN, "T_LPAREN", "(", NOK},
  {T_RPAREN, "T_RPAREN", ")", NOK},
  {T_AS, "T_AS", "as", STRICT},
  {T_BREAK, "T_BREAK", "break", STRICT},
  {T_CONST, "T_CONST", "const", STRICT},
  {T_CONTINUE, "T_CONTINUE", "continue", STRICT},
  {T_CRATE, "T_CRATE", "crate", STRICT},
  {T_ELSE, "T_ELSE", "else", STRICT},
  {T_ENUM, "T_ENUM", "enum", STRICT},
  {T_EXTERN, "T_EXTERN", "extern", STRICT},
  {T_FALSE, "T_FALESE", "false", STRICT},
  {T_FN, "T_FN", "fn", STRICT},
  {T_FOR, "T_FOR", "for", STRICT},
  {T_IF, "T_IF", "if", STRICT},
  {T_IMPL, "T_IMPL", "impl", STRICT},
  {T_IN, "T_IN", "in", STRICT},
  {T_LET, "T_LET", "let", STRICT},
  {T_LOOP, "T_LOOP", "loop", STRICT},
  {T_MATCH, "T_MATCH", "match", STRICT},
  {T_MOD, "T_MOD", "mod", STRICT},
  {T_MOVE, "T_MOVE", "move", STRICT},
  {T_MUT, "T_MUT", "mut", STRICT},
  {T_PUB, "T_PUB", "pub", STRICT},
  {T_REF, "T_REF", "ref", STRICT},
  {T_RETURN, "T_RETURN", "return", STRICT},
  {T_SELFVALUE, "T_SELFVALUE", "self", STRICT},
  {T_SELFTYPE, "T_SELFTYPE", "Self", STRICT},
  {T_STATIC, "T_STATIC", "static", STRICT},
  {T_STRUCT, "T_STRUCT", "struct", STRICT},
  {T_SUPER, "T_SUPER", "super", STRICT},
  {T_TRAIT, "T_TRAIT", "trait", STRICT},
  {T_TRUE, "T_TRUE", "true", STRICT},
  {T_TYPE, "T_TYPE", "type", STRICT},
  {T_UNSAFE, "T_UNSAFE", "unsafe", STRICT},
  {T_USE, "T_USE", "use", STRICT},
  {T_WHERE, "T_WHERE", "where", STRICT},
  {T_WHILE, "T_WHILE", "while", STRICT},
  {T_ASYNC, "T_ASYNC", "async", STRICT | E_2018},
  {T_AWAIT, "T_AWAIT", "await", STRICT | E_2018},
  {T_DYN, "T_DYN", "dyn", STRICT | E_2018},
  {T_ABSTRACT, "T_ABSTRACT", "abstract", RESERVED},
  {T_BECOME, "T_BECOME", "become", RESERVED},
  {T_BOX, "T_BOX", "box", RESERVED},
  {T_DO, "T_DO", "do", RESERVED},
  {T_FINAL, "T_FINAL", "final", RESERVED},
  {T_MACRO, "T_MACRO", "macro", RESERVED},
  {T_OVERRIDE, "T_OVERRIDE", "override", RESERVED},
  {T_PRIV, "T_PRIV", "priv", RESERVED},
  {T_TYPEOF, "T_TYPEOF", "typeof", RESERVED},
  {T_UNSIZED, "T_UNSIZED", "unsized", RESERVED},
  {T_VIRTUAL, "T_VIRTUAL", "virtual", RESERVED},
  {T_YIELD, "T_YIELD", "yield", RESERVED},
  {T_TRY, "T_TRY", "try", RESERVED | E_2018},
  {T_MACRO_RULES, "T_MACRO_RULES", "macro_rules", WEAK},
  {T_UNION, "T_UNION", "union", WEAK},
  {T_STATIC_LIFETIME, "T_STATIC_LIFETIME", "'static", WEAK}
};
// clang-format on

void T_error(const T_Location* loc, const char* fmt, ...) {
  va_list ap;

  fprintf(stderr, "%s:%zu:%zu: error: ", loc->source_file, loc->line, loc->col);
  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);
  va_end(ap);

  putc('\n', stderr);
  exit(1);
}

void T_keyword(T_Token* tok, T_Edition ed) {
  size_t i;

  if (tok->kind != T_IDENT)
    return;

  for (i = 0; i < sizeof(Tokens) / sizeof(Tokens[0]); i++) {
    if (Tokens[i].type == NOK)
      continue;
    if (ed == ED_2015 && (Tokens[i].type & E_2018) != 0)
      continue;
    if (strcmp(Tokens[i].repr, tok->literal) != 0)
      continue;

    tok->kind = Tokens[i].kind;
  }
}

const char* T_desc(T_TokenKind tk) {
  size_t i;

  for (i = 0; i < sizeof(Tokens) / sizeof(Tokens[0]); i++) {
    if (Tokens[i].kind == tk)
      if (strlen(Tokens[i].repr) != 0)
        return Tokens[i].repr;
      else
        return Tokens[i].name;
  }

  U_die("Unknown token");
  return NULL;
}
