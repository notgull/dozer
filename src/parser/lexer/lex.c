// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

// Known issues:
// - UTF-8 support needs some work non ASCII charsets.
// - Block comments may be broken.

#include <ctype.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dozer.h"
#include "util/util.h"

#define U_EOF UINT32_MAX
#define U_PEEK (UINT32_MAX - 1)

enum no_e_start {
  ALLOW_E = 0,
  FORBID_E,
};

enum allowed_escapes {
  ASCII_ESCAPES = 0x01,
  BYTE_ESCAPES = 0x02,
  UNICODE_ESCAPES = 0x04,
  STRING_CONTINUE = 0x08,
  FORBID_ZEROES = 0x10,
};

enum str_lit_type { REGULAR_STR = 0, BYTE_STR, C_STR };

enum char_lit_type { CHAR_LITERAL = 0, BYTE_LITERAL };

// lexer state
struct lexer {
  FILE* file;
  T_Edition ed;

  int by;
  uint32_t chr, peek;
  T_Location loc;

  uint8_t* buf;
  bool capture;
  bool trim_last_dot;
};

// Remove the string from the buffer.
static char* string_get(uint8_t** str) {
  char* result;
  size_t len;

  len = U_len(*str);
  U_addbyte(str, '\0');
  result = U_xmalloc(len);
  memcpy(result, *str, len);
  U_contract(str, len + 1);

  return result;
}

// Trim the last dot from a string.
static void trim_last_dot(char* str) {
  size_t len = strlen(str);

  if (str[len - 1] == '.')
    str[len - 1] = '\0';
}

// read the next byte from a lexer, this does not handle utf8
static void lexer_getc(L_Lexer* lexer) {
  if (lexer->capture)
    U_addbyte(&lexer->buf, lexer->by);

  lexer->by = fgetc(lexer->file);
}

// read the next char from a lexer, this decodes utf-8
static size_t lexer_utf8_char(L_Lexer* lexer) {
  size_t i, len;

  lexer_getc(lexer);
  if (lexer->by == EOF || lexer->by == 0)
    return -1;  // EOF or interior null byte
  else if (lexer->by < 0x80) {
    // this is ASCII
    lexer->chr = lexer->by;
    return 1;
  }

  if ((lexer->by & 0xE0) == 0xC0) {
    lexer->chr = lexer->by & 0x1F;
    len = 1;
  } else if ((lexer->by & 0xF0) == 0xE0) {
    lexer->chr = lexer->by & 0x0F;
    len = 2;
  } else if ((lexer->by & 0xF8) == 0xF0) {
    lexer->chr = lexer->by & 0x07;
    len = 3;
  } else {
    return -1;
  }

  for (i = 0; i < len; i++) {
    lexer_getc(lexer);
    if (lexer->by == EOF)
      return -1;
    else if ((lexer->by & 0xC0) != 0x80)
      return -1;

    lexer->chr = (lexer->chr << 6) | (lexer->by & 0x3F);
  }

  if (lexer->chr >= 0x110000 || lexer->chr - 0xD800 < 0x0200)
    return -1;

  return len;
}

// process a char from the file
static void lexer_next(L_Lexer* lexer) {
  size_t len;

  if (lexer->peek != U_PEEK) {
    lexer->chr = lexer->peek;
    lexer->peek = U_PEEK;
    return;
  }

  // Just ignore all CRs. We either filter them out or error on them,
  // so it complicates things less to go past them.
  do {
    len = lexer_utf8_char(lexer);
    if (len == -1)
      lexer->chr = U_EOF;
    else if (lexer->chr == '\n') {
      lexer->loc.line += 1;
      lexer->loc.col = 0;
    } else {
      lexer->loc.col += len;
    }
  } while (lexer->chr == '\r');
}

// Peek a char from the file.
static void lexer_peek(L_Lexer* lexer) {
  uint32_t prev;

  prev = lexer->chr;
  lexer_next(lexer);
  lexer->peek = lexer->chr;
  lexer->chr = prev;
}

// Tell if what we've captured matches this string.
static bool lexer_captured(const L_Lexer* lexer,
                           const char* str,
                           size_t hashtag_count) {
  size_t len = strlen(str), i;
  uint8_t* buf = lexer->buf;

  if (len + hashtag_count != U_len(buf))
    return false;

  if (memcmp(str, buf, len) != 0)
    return false;

  for (i = len; i < len + hashtag_count; i++) {
    if (buf[i] != '#')
      return false;
  }

  return true;
}

// Is this character a starter for an identifier?
static bool is_xid_start(uint32_t chr) {
  return ('A' <= chr && chr <= 'Z') || ('a' <= chr && chr <= 'z');
}

// Does this character continue an identifier?
static bool is_xid_continue(uint32_t chr) {
  return is_xid_start(chr) || ('0' <= chr && chr <= '9') || chr == '_';
}

// read to the end of the line
static void lexer_eol(L_Lexer* lexer) {
  while (lexer->chr != '\n' && lexer->chr != U_EOF)
    lexer_next(lexer);
}

// read a comment from the lexer
// skips doc comments too, we don't really care about those
static bool lexer_comment(L_Lexer* lexer) {
  uint32_t last, level = 1;

  switch (lexer->chr) {
    case '/':
      lexer_eol(lexer);
      break;
    case '*':
      // handle nested comments
      lexer_next(lexer);
      do {
        last = lexer->chr;
        lexer_next(lexer);
        if (lexer->chr == U_EOF)
          T_error(&lexer->loc, "EOF in comment");
        else if (last == '*' && lexer->chr == '/')
          level -= 1;
        else if (last == '/' && lexer->chr == '*')
          level += 1;
      } while (level > 0);
      lexer_next(lexer);
      break;
    default:
      return false;
  }

  return true;
}

// a punctuation token that starts with a char, and then maybe has an equals
// sign after it
static T_TokenKind lexer_punct_maybe_eq(L_Lexer* lexer,
                                        T_TokenKind no_eq,
                                        T_TokenKind has_eq) {
  lexer_next(lexer);

  if (lexer->chr != '=')
    return no_eq;

  lexer_next(lexer);
  return has_eq;
}

// a punctuation token that starts with a char, and then has the same char after
// or an equals sign
static T_TokenKind lexer_punct_maybe_equals_or_repeat(L_Lexer* lexer,
                                                      T_TokenKind no_repeat,
                                                      T_TokenKind has_eq,
                                                      T_TokenKind has_repeat) {
  uint32_t ch = lexer->chr;

  lexer_next(lexer);
  if (lexer->chr == ch) {
    lexer_next(lexer);
    return has_repeat;
  } else if (lexer->chr != '=')
    return no_repeat;

  lexer_next(lexer);
  return has_eq;
}

// a punctuation token that is one of:
//
// - just a char
// - a repeated char
// - then an equals sign
// - a repeat then a both
static T_TokenKind lexer_punct_maybe_equals_or_repeat_or_both(
    L_Lexer* lexer,
    T_TokenKind blank,
    T_TokenKind has_equals,
    T_TokenKind has_repeat,
    T_TokenKind has_both) {
  uint32_t ch = lexer->chr;

  lexer_next(lexer);
  if (lexer->chr == '=') {
    lexer_next(lexer);
    return has_equals;
  } else if (lexer->chr != ch)
    return blank;

  return lexer_punct_maybe_eq(lexer, has_repeat, has_both);
}

// Read the remainder of an identifer.
static void lexer_ident_or_keyword(L_Lexer* lexer) {
  lexer->capture = true;

  while (is_xid_continue(lexer->chr))
    lexer_next(lexer);
}

// Read a suffix.
static void lexer_suffix(L_Lexer* lexer, enum no_e_start start) {
  if (!is_xid_start(lexer->chr) ||
      (start == FORBID_E && (lexer->chr == 'e' || lexer->chr == 'E')))
    return;

  lexer_ident_or_keyword(lexer);
}

static bool lexer_number_matches_base(uint32_t chr, uint32_t base) {
  if (base == 16)
    return ('0' <= chr && chr <= '9') || ('a' <= chr && chr <= 'f') ||
           ('A' <= chr && chr <= 'F');
  else
    return '0' <= chr && chr <= ('0' + base - 1);
}

// Read the exponent for a float literal.
static bool lexer_float_exponent(L_Lexer* lexer) {
  T_Location start = lexer->loc;
  bool non_underscore = false;

  if (lexer->chr == 'e' || lexer->chr == 'E') {
    lexer_next(lexer);

    if (lexer->chr == '+' || lexer->chr == '-')
      lexer_next(lexer);

    // Read out the exponent.
    non_underscore = false;
    while (lexer_number_matches_base(lexer->chr, 10) || lexer->chr == '_') {
      if (isdigit(lexer->chr))
        non_underscore = true;
      lexer_next(lexer);
    }
    if (!non_underscore)
      T_error(&start,
              "encountered an exponent made up entirely of underscores");

    return true;
  }

  return false;
}

// Read a number from the file.
static T_TokenKind lexer_number(L_Lexer* lexer) {
  T_TokenKind tok = T_INTEGER;
  enum no_e_start forbid_e = FORBID_E;
  uint32_t base = 10;
  bool non_underscore = true;
  T_Location start = lexer->loc;

  lexer->capture = true;

  if (lexer->chr == '0') {
    lexer_next(lexer);
    switch (lexer->chr) {
      case 'x':
        base = 16;
        break;
      case 'o':
        base = 8;
        break;
      case 'b':
        base = 2;
        break;
    }
    if (base != 10) {
      lexer_next(lexer);
      non_underscore = false;
    }
  }

  while (lexer_number_matches_base(lexer->chr, base) || lexer->chr == '_') {
    if (isdigit(lexer->chr))
      non_underscore = true;
    lexer_next(lexer);
  }

  if (!non_underscore)
    T_error(&start, "encountered a number made up entirely of underscores");

  // Check for E suffix.
  if (base == 10 && lexer_float_exponent(lexer)) {
    lexer_suffix(lexer, ALLOW_E);
    return T_FLOAT;
  }

  // Handle floats.
  if (base == 10 && lexer->chr == '.') {
    // Peek ahead and see if this is valid.
    lexer_peek(lexer);
    if (lexer->peek == '.' || lexer->peek == '_' || is_xid_start(lexer->peek)) {
      // This form is either a range (2..3) or someone calling a method on an
      // integer literal. Either way, trim the last dot and return the integer
      // literal.
      lexer->trim_last_dot = true;
      return T_INTEGER;
    } else if (lexer_number_matches_base(lexer->peek, 10)) {
      // This is a float literal.
      lexer_next(lexer);
      tok = T_FLOAT;

      // Read a decimal literal.
      while (lexer_number_matches_base(lexer->chr, 10) || lexer->chr == '_')
        lexer_next(lexer);

      // Read an exponent.
      if (lexer_float_exponent(lexer))
        forbid_e = ALLOW_E;
    } else {
      // Something else entirely. This is a float, but with no suffix.
      lexer_next(lexer);
      return T_FLOAT;
    }
  }

  lexer_suffix(lexer, forbid_e);
  return tok;
}

// Handle a unicode escape.
static void lexer_unicode(L_Lexer* lexer, bool forbid_zeroes) {
  bool seen_non_zero = false;
  size_t i = 0;

  lexer_next(lexer);
  if (lexer->chr != '{')
    T_error(&lexer->loc, "invalid \\u escape sequence without brace");
  lexer_next(lexer);

  if (!lexer_number_matches_base(lexer->chr, 16))
    T_error(&lexer->loc, "invalid \\u escape sequence");

  while (1) {
    if (lexer->chr == '}') {
      lexer_next(lexer);
      if (forbid_zeroes && !seen_non_zero)
        T_error(&lexer->loc,
                "forbidden '\\u{%0*d}' escape sequence in C string", i, 0);
      else if (i == 0)
        T_error(&lexer->loc, "invalid empty \\u escape sequence");
      return;
    } else if (lexer->chr == '_')
      lexer_next(lexer);
    else if (lexer_number_matches_base(lexer->chr, 16)) {
      i++;
      if (i > 6)
        T_error(&lexer->loc, "more than 6 digits in \\u escape sequence");
      if (lexer->chr != '0')
        seen_non_zero = true;
      lexer_next(lexer);
    } else
      T_error(&lexer->loc, "invalid \\u escape sequence");
  }
}

// Handle an escape.
static void lexer_escape(L_Lexer* lexer, enum allowed_escapes escape) {
  bool first_digit_was_zero = false;

  lexer_next(lexer);

  if ((escape & ASCII_ESCAPES) && strchr("'\"nrt\\0", lexer->chr)) {
    if ((escape & FORBID_ZEROES) && lexer->chr == '0')
      T_error(&lexer->loc, "forbidden '\\0' escape sequence in C string");
    lexer_next(lexer);
  } else if ((escape & ASCII_ESCAPES) && lexer->chr == 'x') {
    lexer_next(lexer);

    if (lexer->chr == '0')
      first_digit_was_zero = true;
    if (((escape & BYTE_ESCAPES) &&
         lexer_number_matches_base(lexer->chr, 16)) ||
        lexer_number_matches_base(lexer->chr, 8))
      lexer_next(lexer);
    else
      T_error(&lexer->loc, "first digit of \\x escape sequence is invalid");

    if (lexer->chr == '0' && first_digit_was_zero && (escape & FORBID_ZEROES))
      T_error(&lexer->loc, "forbidden ''\\x00' escape sequence in C string");
    if (!lexer_number_matches_base(lexer->chr, 16))
      T_error(&lexer->loc, "second digit of \\x escape sequence is invalid");

    lexer_next(lexer);
  } else if ((escape & UNICODE_ESCAPES) && lexer->chr == 'u')
    lexer_unicode(lexer, (escape & FORBID_ZEROES) != 0);
  else if ((escape & STRING_CONTINUE) && lexer->chr == '\n')
    lexer_next(lexer);
  else
    T_error(&lexer->loc, "invalid escape sequence");
}

// Read a character literal or a lifetime.
static T_TokenKind lexer_char_literal_or_lifetime(L_Lexer* lexer,
                                                  enum char_lit_type ty) {
  bool could_be_a_lifetime = false;
  enum allowed_escapes escape;

  escape = ty == CHAR_LITERAL ? ASCII_ESCAPES | UNICODE_ESCAPES
                              : ASCII_ESCAPES | BYTE_ESCAPES;

  lexer->capture = true;
  lexer_next(lexer);

  // Read one char.
  switch (lexer->chr) {
    case '\\':
      lexer_escape(lexer, escape);
      could_be_a_lifetime = false;
      break;
    case '\'':
      T_error(&lexer->loc, "empty char literal");
      break;
    case '\n':
      T_error(&lexer->loc, "newline in char constant");
      break;
    case U_EOF:
      T_error(&lexer->loc, "EOF in char constant");
      break;
    default:
      if (ty == BYTE_LITERAL && lexer->chr > 127)
        T_error(&lexer->loc, "non ascii in byte literal");
      could_be_a_lifetime = is_xid_start(lexer->chr);
      lexer_next(lexer);
  }

  if (ty == BYTE_LITERAL)
    could_be_a_lifetime = false;

  // See if this is the end of the constant.
  if (lexer->chr != '\'')
    if (could_be_a_lifetime) {
      lexer_ident_or_keyword(lexer);
      return T_LIFETIME;
    } else
      T_error(&lexer->loc, "char literal with more than one character");

  lexer_next(lexer);
  lexer_suffix(lexer, ALLOW_E);
  return ty == CHAR_LITERAL ? T_CHAR_LITERAL : T_BYTE_LITERAL;
}

// Read a string literal.
static void lexer_string(L_Lexer* lexer, enum str_lit_type ty) {
  enum allowed_escapes escape;
  if (ty == REGULAR_STR)
    escape = ASCII_ESCAPES | UNICODE_ESCAPES;
  else if (ty == BYTE_STR)
    escape = ASCII_ESCAPES | BYTE_ESCAPES;
  else  // C_STR
    escape = ASCII_ESCAPES | BYTE_ESCAPES | UNICODE_ESCAPES | FORBID_ZEROES;
  bool was_escape = false;

  lexer->capture = true;

  do {
    if (!was_escape) {
      lexer_next(lexer);
    } else {
      was_escape = false;
    }

    if (lexer->chr == '\\') {
      lexer_escape(lexer, escape | STRING_CONTINUE);
      was_escape = true;
    } else if (ty == C_STR && lexer->chr == '\0')
      T_error(&lexer->loc, "forbidden zero byte in C string");
    else if (ty == BYTE_STR && lexer->chr > 127)
      T_error(&lexer->loc, "non ascii char in byte string");
    else if (lexer->chr == U_EOF)
      T_error(&lexer->loc, "unclosed string");
  } while (lexer->chr != '"');

  lexer_next(lexer);
  lexer_suffix(lexer, ALLOW_E);
}

// Read a raw string literal.
static void lexer_raw_string(L_Lexer* lexer,
                             size_t hashtag_count,
                             enum str_lit_type ty) {
  size_t i;
  bool just_read_hashtag = false;

  lexer->capture = true;

  do {
    if (!just_read_hashtag)
      lexer_next(lexer);
    else
      just_read_hashtag = false;

    if (ty == C_STR && lexer->chr == '\0')
      T_error(&lexer->loc, "forbidden zero byte in raw C string");
    else if (ty == BYTE_STR != 0 && lexer->chr > 127)
      T_error(&lexer->loc, "non ascii char in raw byte string");
    else if (lexer->chr == '"') {
      for (i = 0; i < hashtag_count; i++) {
        lexer_next(lexer);
        if (lexer->chr != '#')
          break;
      }

      if (hashtag_count <= i) {
        lexer_next(lexer);
        return;
      }

      just_read_hashtag = true;
    } else if (lexer->chr == U_EOF)
      T_error(&lexer->loc, "unclosed raw string");
  } while (1);
}

// Read a token kind from the file.
static T_TokenKind lexer_kind(L_Lexer* lexer) {
  T_TokenKind tok;
  size_t hashtag_count;

  while (1) {
    switch (lexer->chr) {
      case U_EOF:
        return T_EOF;
      case ' ':
      case '\t':
      case '\n':
      case 0x000B:
      case 0x000C:
      case 0x000D:
      case 0x0085:
      case 0x200E:
      case 0x200F:
      case 0x2028:
      case 0x2029:
        // whitespace
        lexer_next(lexer);
        break;
      case '+':
        return lexer_punct_maybe_eq(lexer, T_PLUS, T_PLUSEQ);
      case '-':
        tok = lexer_punct_maybe_eq(lexer, T_MINUS, T_MINUSEQ);

        if (tok == T_MINUS && lexer->chr == '>') {
          lexer_next(lexer);
          return T_RARROW;
        }
        return tok;
      case '*':
        return lexer_punct_maybe_eq(lexer, T_STAR, T_STAREQ);
      case '/':
        tok = lexer_punct_maybe_eq(lexer, T_SLASH, T_SLASHEQ);
        if (tok == T_SLASH && lexer_comment(lexer)) {
          // Eat the comment.
          continue;
        }
        return tok;
      case '%':
        return lexer_punct_maybe_eq(lexer, T_PERCENT, T_PERCENTEQ);
      case '^':
        return lexer_punct_maybe_eq(lexer, T_CARET, T_CARETEQ);
      case '!':
        return lexer_punct_maybe_eq(lexer, T_NOT, T_NE);
      case '=':
        tok = lexer_punct_maybe_eq(lexer, T_EQ, T_EQEQ);

        if (tok == T_EQ && lexer->chr == '>') {
          lexer_next(lexer);
          return T_FATARROW;
        }
        return tok;
      case '&':
        return lexer_punct_maybe_equals_or_repeat(lexer, T_AND, T_ANDEQ,
                                                  T_ANDAND);
      case '|':
        return lexer_punct_maybe_equals_or_repeat(lexer, T_OR, T_OROR, T_OREQ);
      case '<':
        tok = lexer_punct_maybe_equals_or_repeat_or_both(lexer, T_LT, T_LE,
                                                         T_SHL, T_SHLEQ);

        if (tok == T_LT && lexer->chr == '-') {
          lexer_next(lexer);
          return T_LARROW;
        }
        return tok;
      case '>':
        return lexer_punct_maybe_equals_or_repeat_or_both(lexer, T_GT, T_GE,
                                                          T_SHR, T_SHREQ);
      case '.':
        lexer_next(lexer);
        if (lexer->chr != '.')
          return T_DOT;

        return lexer_punct_maybe_equals_or_repeat(lexer, T_DOTDOT, T_DOTDOTEQ,
                                                  T_DOTDOTDOT);
      case ',':
        lexer_next(lexer);
        return T_COMMA;
      case ';':
        lexer_next(lexer);
        return T_SEMI;
      case ':':
        lexer_next(lexer);
        if (lexer->chr != ':')
          return T_COLON;

        lexer_next(lexer);
        return T_PATHSEP;
      case '@':
        lexer_next(lexer);
        return T_AT;
      case '#':
        lexer_next(lexer);
        return T_POUND;
      case '$':
        lexer_next(lexer);
        return T_DOLLAR;
      case '?':
        lexer_next(lexer);
        return T_QUESTION;
      case '~':
        lexer_next(lexer);
        return T_TILDE;
      case '{':
        lexer_next(lexer);
        return T_LBRACE;
      case '}':
        lexer_next(lexer);
        return T_RBRACE;
      case '[':
        lexer_next(lexer);
        return T_LBRACKET;
      case ']':
        lexer_next(lexer);
        return T_RBRACKET;
      case '(':
        lexer_next(lexer);
        return T_LPAREN;
      case ')':
        lexer_next(lexer);
        return T_RPAREN;
      case '\'':
        return lexer_char_literal_or_lifetime(lexer, CHAR_LITERAL);
      case '"':
        lexer_string(lexer, REGULAR_STR);
        return T_STRING_LITERAL;
      default:
        if (isdigit(lexer->chr)) {
          return lexer_number(lexer);
        }

        if (is_xid_start(lexer->chr) || lexer->chr == '_') {
          lexer_ident_or_keyword(lexer);

          // Read any hashtags if this is a raw literal.
          hashtag_count = 0;
          if (lexer->chr == '#' && lexer->buf[U_len(lexer->buf) - 1] == 'r') {
            do {
              hashtag_count += 1;
              lexer_next(lexer);
            } while (lexer->chr == '#');
          }

          // Maybe there is a string or char after.
          if (lexer->chr == '\'') {
            if (lexer_captured(lexer, "b", 0)) {
              return lexer_char_literal_or_lifetime(lexer, BYTE_LITERAL);
            } else {
              T_error(&lexer->loc, "invalid char prefix: %s",
                      string_get(&lexer->buf));
            }
          } else if (lexer->chr == '\"') {
            if (lexer_captured(lexer, "r", hashtag_count)) {
              lexer_raw_string(lexer, hashtag_count, REGULAR_STR);
              return T_RAW_STRING_LITERAL;
            } else if (lexer_captured(lexer, "b", 0)) {
              lexer_string(lexer, BYTE_STR);
              return T_BYTE_STRING_LITERAL;
            } else if (lexer_captured(lexer, "br", hashtag_count)) {
              lexer_raw_string(lexer, hashtag_count, BYTE_STR);
              return T_RAW_BYTE_STRING_LITERAL;
            } else if (lexer_captured(lexer, "c", 0)) {
              lexer_string(lexer, C_STR);
              return T_C_STRING_LITERAL;
            } else if (lexer_captured(lexer, "cr", hashtag_count)) {
              lexer_raw_string(lexer, hashtag_count, C_STR);
              return T_RAW_C_STRING_LITERAL;
            } else {
              T_error(&lexer->loc, "invalid str prefix: %s",
                      string_get(&lexer->buf));
            }
          } else if (lexer_captured(lexer, "r", 1) &&
                     (is_xid_start(lexer->chr) || lexer->chr == '_')) {
            // Raw identifier.
            lexer_ident_or_keyword(lexer);
          }

          return T_IDENT;
        }

        T_error(&lexer->loc, "unexpected character '%c' (0x%x)", lexer->chr,
                lexer->chr);
    }
  }
}

L_Lexer* L_mklexer(const char* source_file, T_Edition ed) {
  L_Lexer* lexer;

  lexer = U_xmalloc(sizeof(*lexer));
  lexer->file = fopen(source_file, "rb");
  if (!lexer->file)
    U_die("fopen %s:", source_file);
  lexer->ed = ed;

  lexer->by = 0;
  lexer->chr = 0;
  lexer->peek = U_PEEK;
  lexer->loc.source_file = source_file;
  lexer->loc.line = 1;
  lexer->loc.col = 0;

  lexer->buf = U_mkarray(NULL, 128, 1);

  lexer->capture = false;
  lexer->trim_last_dot = false;

  lexer_next(lexer);

  // Read FEFF byte if needed.
  if (lexer->chr == 0xFEFF)
    lexer_next(lexer);

  // Read past shebang.
  if (lexer->chr == '#') {
    lexer_peek(lexer);
    if (lexer->peek == '!')
      lexer_eol(lexer);
  }

  return lexer;
}

void L_freelexer(L_Lexer* lexer) {
  fclose(lexer->file);
  U_freearray(lexer->buf);
  free(lexer);
}

void L_lex(L_Lexer* lexer, T_Token* tok) {
  tok->loc = lexer->loc;
  tok->kind = lexer_kind(lexer);

  // Read out the buffer if we used it.
  if (lexer->capture) {
    tok->literal = string_get(&lexer->buf);
    lexer->capture = false;

    if (lexer->trim_last_dot) {
      trim_last_dot(tok->literal);
      lexer->trim_last_dot = false;
    }
  } else {
    tok->literal = NULL;
  }

  T_keyword(tok, lexer->ed);
}
