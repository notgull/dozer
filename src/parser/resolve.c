// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

#include "dozer.h"
#include "util/util.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Context exists on a per-block basis.
struct context {
  struct context* prev;
  M_Map locals;  // maps name to E_Expr pointer
  G_Sym* module;
  bool inherit;
};

static struct context* mkcontext(struct context* prev,
                                 G_Sym* module,
                                 bool inherit) {
  struct context* ctx = U_xmalloc(sizeof(struct context));
  ctx->prev = prev;
  ctx->module = module;
  ctx->inherit = inherit;
  M_init(&ctx->locals, 8);
  return ctx;
}

static void finicontext(struct context** ctx) {
  struct context* slot = *ctx;

  if (!slot)
    U_die("tried to de-alloc non-existant context");

  M_free(&slot->locals, NULL);
  *ctx = slot->prev;
  free(slot);
}

static void addlocal(struct context* ctx, const char* name, E_Expr* expr) {
  M_Key key;
  E_Expr** slot;

  M_initkey(&key, name, strlen(name));
  slot = (E_Expr**)M_insert(&ctx->locals, &key);
  *slot = expr;
}

static E_Expr* getlocal(struct context* ctx, const char* name) {
  M_Key key;
  E_Expr* expr;

  M_initkey(&key, name, strlen(name));
  while (ctx) {
    expr = M_get(&ctx->locals, &key);
    if (expr)
      return expr;

    if (ctx->inherit)
      ctx = ctx->prev;
    else
      return NULL;
  }

  return NULL;
}

static G_Sym* getglobal(struct context* ctx, const PT_Path* path) {
  M_Key key;
  G_Sym* sym;

  while (ctx) {
    sym = G_lookup(ctx->module->subitems, path);
    if (sym)
      return sym;

    if (ctx->module->kind != GS_CRATE && ctx->module->kind != GS_MODULE)
      ctx = ctx->prev;
    else
      return NULL;
  }

  return NULL;
}

static bool expr_is_block(const E_Expr* expr, bool* inherit) {
  switch (expr->kind) {
    case E_BLOCK:
      *inherit = true;
      return true;
    default:
      return false;
  }
}

static void mkstatus(R_TypeStatus* rts, G_Sym* type) {
  rts->kind = RT_SPECIFIED;
  rts->sym = type;
}

static R_TypeStatus typecheck_lit(char* lit, int kind) {
  const char* suffix = NULL;
  R_TypeStatus type_status;
  PT_Path path;
  uint64_t slot;

  switch (kind) {
    case EL_INT:
      suffix = U_compile_intlit(lit, &slot);

      if (suffix && strlen(suffix)) {
        path = PT_single(suffix, false);
        mkstatus(&type_status, G_lookup(NULL, &path));
      } else
        type_status.kind = RT_INTEGER;
      break;
    case EL_FLOAT:
      suffix = U_compile_floatlit(lit, &slot);
      if (suffix && strlen(suffix)) {
        path = PT_single(suffix, false);
        mkstatus(&type_status, G_lookup(NULL, &path));
      } else
        type_status.kind = RT_INTEGER;

      break;
    case EL_STR:
    case EL_BYTE_STR:
    case EL_C_STR:
      // TODO: Make this &[u8] once we have proper references and slice types.
      mkstatus(&type_status, G_mkpointer(G_primitive(GP_U8), false));
      break;
    case EL_BOOL:
      mkstatus(&type_status, G_primitive(GP_BOOL));
      break;
    case EL_BYTE:
      mkstatus(&type_status, G_primitive(GP_U8));
      break;
    case EL_CHAR:
      mkstatus(&type_status, G_primitive(GP_CHAR));
      break;
    default:
      U_die("not handling this literal type yet");
  }

  return type_status;
}

static R_TypeStatus* root_type(R_TypeStatus* rts) {
  while (rts->kind == RT_REF)
    rts = rts->other;
  return rts;
}

static bool type_is_more_specific(R_TypeStatus* more, R_TypeStatus* than) {
  more = root_type(more);
  than = root_type(than);

  if (more->kind == RT_UNKNOWN)
    return false;
  else
    return than->kind != RT_SPECIFIED && than->kind != RT_DOESNT_MATTER;
}

enum primkind { PAny, PInt, PFloat };

static bool is_primitive(R_TypeStatus* status, enum primkind primkind) {
  G_TypeNode* tn;

  status = root_type(status);
  switch (status->kind) {
    case RT_INTEGER:
      return primkind != PFloat;
    case RT_FLOAT:
      return primkind != PInt;
    case RT_SPECIFIED:
      tn = status->sym->type;
      if (tn->kind != GT_PRIMITIVE)
        return false;
      if (primkind == PInt)
        return !tn->primitive.is_float;
      else if (primkind == PFloat)
        return tn->primitive.is_float;
      else
        return true;
    default:
      return false;
  }
}

static void mkref(R_TypeStatus* hole, R_TypeStatus* peg) {
  hole->kind = RT_REF;
  hole->other = peg;
}

static void type_must_fit(R_TypeStatus* hole, R_TypeStatus* peg) {
  R_TypeStatus *freespace, *holeroot;
  bool will_fit;

  if (type_is_more_specific(hole, peg)) {
    freespace = hole;
    hole = peg;
    peg = freespace;
  }

  peg = root_type(peg);
  holeroot = root_type(hole);
  switch (holeroot->kind) {
    case RT_UNKNOWN:
    case RT_DOESNT_MATTER:
      will_fit = true;
      break;
    case RT_INTEGER:
      will_fit = is_primitive(peg, PInt);
      break;
    case RT_FLOAT:
      will_fit = is_primitive(peg, PFloat);
      break;
    case RT_SPECIFIED:
      // Because of type_is_more_specific, peg can only be RT_SPECIFIED.
      will_fit = holeroot->sym == peg->sym;
      break;
    case RT_REF:
      U_warn(
          "type slot was RT_REF, means slot was typechecked twice, could be a "
          "bug?");
      return;
  }

  if (!will_fit)
    // TODO: Better diagnostics.
    U_die("cannot fit round pegs in square holes");

  // Convert type into RT_REF for the peg.
  mkref(hole, peg);
}

static void type_must_be(R_TypeStatus* hole, G_Sym* ty) {
  R_TypeStatus* st;

  st = U_xmalloc(sizeof(R_TypeStatus));
  st->kind = RT_SPECIFIED;
  st->sym = ty;
  type_must_fit(hole, st);
}

static G_Sym* typeof_sym(G_Sym* sym) {
  G_Sym* result = NULL;

  switch (sym->kind) {
    case GS_FN:
      result = sym->fn.fn_type;
      break;
    default:
      PT_dump(stderr, &sym->full_path);
      U_die("cannot get typeof above symbol of type %d", sym->kind);
  }

  if (!result)
    U_die("symbol %s has no resolved type", sym->name);
  return result;
}

static void typecheck_fnptr(E_Expr* expr,
                            G_TypeNode* tn,
                            E_Expr** inputs,
                            bool skip_receiver) {
  G_Sym** args = tn->fnptr.arguments;

  if (skip_receiver) {
    if ((*args)->kind == GS_NONE)
      U_die("receiver typecheck has GS_NONE as first argument?");
    args++;
  }

  for (; (*args)->kind != GS_NONE; args++, inputs++) {
    if ((*inputs)->kind == E_NONE)
      U_die("fewer input arguments than expected");

    // Make sure the type matches up.
    type_must_be(&(*inputs)->status, *args);
  }
  if ((*inputs)->kind != E_NONE)
    U_die("more arguments than expected");

  if (tn->fnptr.return_type)
    type_must_be(&expr->status, tn->fnptr.return_type);
  else
    type_must_be(&expr->status, G_primitive(GP_UNIT));
}

static void resolve_expr(struct context* ctx, E_Expr* expr) {
  char* name;
  R_TypeStatus status, *statref;
  size_t i;
  E_Expr** inputs;
  ST_Stmt* last;
  E_Expr* next;
  I_Signature* sig;
  G_Sym *sym, **args;
  G_TypeNode* tn;
  PT_Path path;

  switch (expr->kind) {
    case E_NONE:
    case E_EMPTY:
      expr->status.kind = RT_DOESNT_MATTER;
      break;
    case E_PAREN:
      mkref(&expr->status, &expr->paren.expr->status);
      break;
    case E_LIT:
      expr->status = typecheck_lit(expr->lit.lit, expr->lit.kind);
      break;
    case E_BINARY:
      expr->status.kind = RT_UNKNOWN;
      if (is_primitive(&expr->binary.left->status, PAny) &&
          is_primitive(&expr->binary.right->status, PAny)) {
        type_must_fit(&expr->binary.left->status, &expr->binary.right->status);
        type_must_fit(&expr->status, &expr->binary.left->status);
        break;
      }

      U_die("TODO: Add trait not yet supported");
      break;
    case E_PATH:
      // Check if this is a local.
      // TODO: QSelf
      if (expr->path.qself.position < 0 &&
          (name = PT_single_segment(&expr->path.path))) {
        // Try to resolve the local from the scope.
        next = getlocal(ctx, name);
        if (next) {
          // We can now set the target.
          expr->path.kind = PTK_LOCAL;
          expr->path.local = next;
          type_must_fit(&expr->status, &expr->path.local->status);
          break;
        }
      }

      // Try for an item.
      expr->path.kind = PTK_GLOBAL;
      expr->path.sym = getglobal(ctx, &expr->path.path);
      if (!expr->path.sym) {
        PT_dump(stderr, &expr->path.path);
        U_die("could not find local or global with the above name");
      }
      mkstatus(&expr->status, typeof_sym(expr->path.sym));
      break;
    case E_CALL:
      statref = root_type(&expr->call.func->status);
      if (statref->kind != RT_SPECIFIED)
        U_die("TODO: handle non-specified objects as callees");
      if (statref->sym->kind != GS_TYPE &&
          statref->sym->type->kind != GT_FNPTR) {
        PT_dump(stderr, &statref->sym->full_path);
        U_die("TODO: Fn() family of traits, not supported");
      }
      typecheck_fnptr(expr, statref->sym->type, expr->call.args, false);

      break;
    case E_METHOD_CALL:
      statref = root_type(&expr->method_call.receiver->status);
      if (statref->kind != RT_SPECIFIED)
        U_die(
            "type of expr must be specified before it can be used in method "
            "call");

      // Look for the method.
      sym = statref->sym;
      path = PT_single(expr->method_call.method, false);
      if (!(sym = G_lookup(sym->subitems, &path)) || sym->kind != GS_FN)
        U_die("impl fn named %s does not exist", expr->method_call.method);

      // Should be a `self` function.
      if (sym->fn.receiver == GRECV_NONE)
        U_die("function %s does not have a 'self' parameter", sym->obj_name);

      // Typecheck with it.
      typecheck_fnptr(expr, sym->fn.fn_type->type, expr->method_call.args,
                      true);
      expr->method_call.method_sym = sym;

      break;
    case E_BLOCK:
      last = ST_evalexpr(expr->block.stmts);
      if (last) {
        expr->block.evals_to = last->expr.expr;
        type_must_fit(&expr->status, &last->expr.expr->status);
      } else {
        expr->block.evals_to = NULL;
        type_must_be(&expr->status, G_primitive(GP_UNIT));
      }
      break;
    default:
      U_die("expr not supported for typecheck yet");
  }
}

static void resolve_stmt(struct context* ctx, ST_Stmt* stmt) {
  char* name;
  if (stmt->kind != ST_LOCAL)
    return;

  // TODO: Decompose pattern properly
  if (stmt->local.pat->kind != PN_IDENT || stmt->local.pat->ident.by_ref ||
      stmt->local.pat->ident.subpattern)
    U_die("we don't support non-ident pats");
  name = stmt->local.pat->ident.ident;

  if (stmt->local.type)
    U_die("TODO: support typecheck");

  addlocal(ctx, name, stmt->local.expr);
}

static void add_arglocals(struct context* ctx, const I_Signature* sig) {
  // TODO: Don't leak E_Expr
  E_Expr* local;
  I_FnArg* args;
  uint32_t reg = 0;
  char* name;
  G_Sym* type;

  for (args = sig->fn_args; args->kind != FNARG_NONE; args++) {
    local = U_xmalloc(sizeof(E_Expr));
    local->kind = E_EMPTY;

    switch (args->kind) {
      case FNARG_TYPED:
        // TODO: Decompose pattern properly
        if (args->typed.pat->kind != PN_IDENT ||
            args->typed.pat->ident.by_ref || args->typed.pat->ident.subpattern)
          U_die("we don't support non-ident pats");
        name = args->typed.pat->ident.ident;
        type = args->typed.ty->sym;
        break;
      case FNARG_RECEIVER:
        name = "self";
        type = args->receiver.ty->sym;
        break;
    }

    mkstatus(&local->status, type);
    local->reg = reg++;
    B_mkwriter(&local->dir);

    // TODO: Properly support patterns.
    addlocal(ctx, name, local);
  }
}

static void resolve(AST_Kind kind, void* ast, void* c) {
  struct context** context = c;
  I_Item* item;
  I_ImplItem* ii;
  enum { AAddCtx, AAddBlock, ARmCtx, ARmBlock } action = AAddCtx;
  G_Sym* sym = NULL;
  bool inherit;
  I_Signature* sig = NULL;

  switch (kind) {
    case AST_ITEM_B:
      item = ast;
      sym = item->sym;
      if (item->kind == I_FN)
        sig = &item->fn.sig;
      break;
    case AST_ITEM:
      item = ast;
      sym = item->sym;
      action = ARmCtx;
      break;
    case AST_IMPL_ITEM_B:
      ii = ast;
      sym = ii->sym;
      if (ii->kind == II_FN)
        sig = &ii->fn.sig;
      break;
    case AST_IMPL_ITEM:
      ii = ast;
      sym = ii->sym;
      action = ARmCtx;
      break;
    case AST_EXPR_B:
      if (expr_is_block(ast, &inherit))
        action = AAddBlock;
      break;
    case AST_EXPR:
      resolve_expr(*context, ast);
      if (expr_is_block(ast, &inherit))
        action = ARmBlock;
      break;
    case AST_STMT:
      resolve_stmt(*context, ast);
      break;
  }

  // Based on the action derived above, perform an action.
  switch (action) {
    case AAddCtx:
      // NULL sym is an alias for no action.
      if (sym && sym->subitems)
        *context = mkcontext(*context, sym, false);
      break;
    case AAddBlock:
      if (!*context)
        U_die("tried to add block on root level?");
      *context = mkcontext(*context, (*context)->module, inherit);
      break;
    case ARmCtx:
    case ARmBlock:
      if (action == ARmBlock || (sym && sym->subitems))
        finicontext(context);
      break;
  }

  if (sig)
    add_arglocals(*context, sig);
}

void R_resolve(P_SourceFile* sf) {
  struct context *context = mkcontext(NULL, G_currentcrate(), false), *original;
  original = context;

  AST_traverse(sf, resolve, &context);
  if (context != original)
    U_die("scopes were improperly nested");
  finicontext(&original);
}
