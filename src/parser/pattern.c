// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

#include "dozer.h"
#include "util/util.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define cur P_cur(parser)

PTR_BUFFER_FUNCS(pat, PN_Pat, PN_NONE)

static bool continues_ident_as_path(T_TokenKind tk) {
  switch (tk) {
    case T_PATHSEP:
    case T_NOT:
    case T_LBRACE:
    case T_LPAREN:
    case T_DOTDOT:
    case T_DOTDOTEQ:
      return true;
    default:
      return false;
  }
}

static void pat_ident(P_Parser* parser, PN_Pat* pat) {
  pat->kind = PN_IDENT;
  pat->ident.by_ref = P_consume(parser, T_REF);
  pat->ident.mutability = P_consume(parser, T_MUT);

  if (cur->kind == T_SELFVALUE)
    pat->ident.ident = strdup("self");
  else if (cur->kind == T_IDENT)
    pat->ident.ident = strdup(cur->literal);
  else
    T_error(&cur->loc, "expected `self` or ident");
  P_next(parser);

  if (P_consume(parser, T_AT))
    pat->ident.subpattern = PN_read_one(parser);
  else
    pat->ident.subpattern = NULL;
}

PN_Pat* PN_read_one(P_Parser* parser) {
  PN_Pat* pat = U_palloc(sizeof(PN_Pat));
  PT_QSelf qself;
  PT_Path path;

  switch (cur->kind) {
    case T_IDENT:
      if (!continues_ident_as_path(P_peek(parser)->kind)) {
        pat_ident(parser, pat);
        return pat;
      }
    case T_SELFVALUE:
      if (P_peek(parser)->kind != T_PATHSEP) {
        pat_ident(parser, pat);
        return pat;
      }
    case T_PATHSEP:
    case T_LT:
    case T_SELFTYPE:
    case T_SUPER:
    case T_CRATE:
      PT_qpath(parser, &qself, &path, true);

      if (qself.position < 0 && cur->kind == T_NOT && PT_is_modstyle(&path)) {
        P_next(parser);
        U_die("TODO: implement macros");
      }

      if (cur->kind == T_LBRACE)
        U_die("TODO: implement struct");
      else if (cur->kind == T_LPAREN)
        U_die("TODO: implement tuple struct");
      else if (cur->kind == T_DOTDOT || cur->kind == T_DOTDOTEQ)
        U_die("TODO: implement ranges");
      else {
        pat->kind = PN_PATH;
        pat->path.qself = qself;
        pat->path.path = path;
      }

      break;
    case T_REF:
    case T_MUT:
      pat_ident(parser, pat);
      break;
    default:
      T_error(&cur->loc, "expected pattern");
  }

  return pat;
}

PN_Pat* PN_read_multi(P_Parser* parser, bool leading_vert) {
  PN_Pat *pat, *first, **buffer;

  buffer = U_pmkarray(1, sizeof(*buffer));

  if (leading_vert)
    leading_vert = P_consume(parser, T_OR);

  pat = PN_read_one(parser);
  if (leading_vert || cur->kind == T_OR) {
    first = pat, pat = U_palloc(sizeof(PN_Pat));
    *pat_buffer_expand(&buffer) = first;

    while (P_consume(parser, T_OR)) {
      *pat_buffer_expand(&buffer) = PN_read_one(parser);
    }

    pat_buffer_finish(&buffer);
    pat->kind = PN_OR;
    pat->or.patterns = buffer;
  }

  return pat;
}

PN_PatType PN_read_pattype(P_Parser* parser) {
  PN_PatType pt;

  pt.pat = PN_read_one(parser);
  P_expect(parser, T_COLON, "expected colon");
  pt.ty = TY_read(parser);

  return pt;
}

void PN_dump(FILE* f, const PN_Pat* pat, int indent) {
  PN_Pat** subpat;

  U_indent(f, indent);

  switch (pat->kind) {
    case PN_NONE:
      fputs("PN_NONE\n", f);
      break;
    case PN_IDENT:
      fprintf(f, "PN_IDENT: %s ", pat->ident.ident);
      if (pat->ident.subpattern) {
        fputs("\n", f);
        PN_dump(f, pat->ident.subpattern, indent + 1);
      } else
        fputs("NO_SUBPATTERN\n", f);
      break;
    case PN_OR:
      fputs("PN_OR:\n", f);
      for (subpat = pat->or.patterns; (*subpat)->kind != PN_NONE; subpat++)
        PN_dump(f, *subpat, indent + 1);
      break;
    case PN_PATH:
      fprintf(f, "E_PATH: ");
      if (pat->path.qself.position >= 0)
        U_die("TODO: print qself");
      PT_dump(f, &pat->path.path);
      break;
  }
}
