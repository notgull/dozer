// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

#include "dozer.h"
#include "util/util.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define cur P_cur(parser)

enum precedence {
  JUMP = 0,
  ASSIGN,
  RANGE,
  OR,
  AND,
  LET,
  COMPARE,
  BITOR,
  BITXOR,
  BITAND,
  SHIFT,
  SUM,
  PRODUCT,
  CAST,
  PREFIX,
  UNAMBIGUOUS
};

PTR_BUFFER_FUNCS(expr, E_Expr, E_NONE)

static void mkexpr(E_Expr* expr, int kind) {
  expr->kind = kind;
  expr->status.kind = RT_UNKNOWN;
  expr->reg = UINT32_MAX;
  B_mkwriter(&expr->dir);
}

static void mklit(E_Expr* expr, int kind, char* lit) {
  mkexpr(expr, E_LIT);
  expr->lit.kind = kind;
  expr->lit.lit = strdup(lit);
}

// Can this token begin an expression?
static bool can_begin_expr(T_TokenKind tk) {
  switch (tk) {
    case T_IDENT:
    case T_SELFVALUE:
    case T_LPAREN:
    case T_LBRACKET:
    case T_LBRACE:
    case T_CHAR_LITERAL:
    case T_BYTE_LITERAL:
    case T_STRING_LITERAL:
    case T_RAW_STRING_LITERAL:
    case T_BYTE_STRING_LITERAL:
    case T_RAW_BYTE_STRING_LITERAL:
    case T_C_STRING_LITERAL:
    case T_RAW_C_STRING_LITERAL:
    case T_INTEGER:
    case T_FLOAT:
    case T_TRUE:
    case T_FALSE:
    case T_NOT:
    case T_MINUS:
    case T_STAR:
    case T_OR:
    case T_AND:
    case T_DOTDOT:
    case T_DOTDOTDOT:
    case T_DOTDOTEQ:
    case T_LT:
    case T_PATHSEP:
    case T_LIFETIME:
    case T_POUND:
      return true;
    default:
      return false;
  }
}

static bool is_binop(T_TokenKind tk) {
  switch (tk) {
    case T_PLUS:
    case T_MINUS:
    case T_STAR:
    case T_SLASH:
    case T_PERCENT:
    case T_ANDAND:
    case T_OROR:
    case T_CARET:
    case T_AND:
    case T_OR:
    case T_SHL:
    case T_SHR:
    case T_EQEQ:
    case T_LT:
    case T_GT:
    case T_LE:
    case T_GE:
    case T_NE:
    case T_PLUSEQ:
    case T_MINUSEQ:
    case T_STAREQ:
    case T_SLASHEQ:
    case T_PERCENTEQ:
    case T_CARETEQ:
    case T_ANDEQ:
    case T_OREQ:
    case T_SHLEQ:
    case T_SHREQ:
      return true;
    default:
      return false;
  }
}

static enum precedence binop_precedence(T_TokenKind tk) {
  switch (tk) {
    case T_PLUS:
    case T_MINUS:
      return SUM;
    case T_STAR:
    case T_SLASH:
    case T_PERCENT:
      return PRODUCT;
    case T_ANDAND:
      return AND;
    case T_OROR:
      return OR;
    case T_CARET:
      return BITXOR;
    case T_AND:
      return BITAND;
    case T_OR:
      return BITOR;
    case T_SHL:
    case T_SHR:
      return SHIFT;

    case T_EQEQ:
    case T_LT:
    case T_LE:
    case T_NE:
    case T_GE:
    case T_GT:
      return COMPARE;

    default:
      return ASSIGN;
  }
}

static enum precedence peek_precedence(const P_Parser* parser) {
  if (is_binop(cur->kind))
    return binop_precedence(cur->kind);
  else if (cur->kind == T_EQ)
    return ASSIGN;
  else if (cur->kind == T_DOTDOT || cur->kind == T_DOTDOTEQ ||
           cur->kind == T_DOTDOTDOT)
    return RANGE;
  else if (cur->kind == T_AS)
    return CAST;
  else
    return 0;
}

static void parse_lt_label(P_Parser* parser, char** label) {
  if (cur->kind == T_LIFETIME) {
    *label = strdup(cur->literal);
    P_next(parser);
  } else
    *label = NULL;
}

static void mkblock(E_Expr* expr, bool is_const, bool is_unsafe) {
  mkexpr(expr, E_BLOCK);
  expr->block.is_const = is_const;
  expr->block.is_unsafe = is_unsafe;
  expr->block.label = NULL;
}

static void expr_block(P_Parser* parser, E_Expr* expr) {
  // TODO: Attributes

  mkblock(expr, false, false);

  parse_lt_label(parser, &expr->block.label);
  expr->block.stmts = ST_block(parser);
}

// allow_struct can be set to false to ignore the leading left brace, for loops.
E_Expr* expr_ambiguous(P_Parser* parser, bool allow_struct);

static void expr_while(P_Parser* parser, E_Expr* expr) {
  // TODO: attributes

  mkexpr(expr, E_BLOCK);
  parse_lt_label(parser, &expr->while_.label);
  P_expect(parser, T_WHILE, "while");
  expr->while_.cond = expr_ambiguous(parser, false);
  expr->while_.stmts = ST_block(parser);
}

static void expr_loop(P_Parser* parser, E_Expr* expr) {
  // TODO: attributes

  mkexpr(expr, E_LOOP);
  parse_lt_label(parser, &expr->loop.label);
  P_expect(parser, T_LOOP, "loop");
  expr->loop.stmts = ST_block(parser);
}

static void expr_for_loop(P_Parser* parser, E_Expr* expr) {
  // TODO: Attributes

  mkexpr(expr, E_FOR_LOOP);
  parse_lt_label(parser, &expr->for_loop.label);
  P_expect(parser, T_FOR, "for");

  expr->for_loop.pat = PN_read_multi(parser, true);
  P_expect(parser, T_IN, "in");
  expr->for_loop.expr = expr_ambiguous(parser, false);
  expr->for_loop.stmts = ST_block(parser);
}

// Take an atomic expression.
static void expr_primary(P_Parser* parser, E_Expr* expr, bool allow_struct) {
  PT_Path path;
  PT_QSelf qself;
  E_Expr *subexpr, **slot, **buffer;

  switch (cur->kind) {
    case T_STRING_LITERAL:
    case T_RAW_STRING_LITERAL:
      mklit(expr, EL_STR, cur->literal);
      P_next(parser);
      break;
    case T_BYTE_STRING_LITERAL:
    case T_RAW_BYTE_STRING_LITERAL:
      mklit(expr, EL_BYTE_STR, cur->literal);
      P_next(parser);
      break;
    case T_C_STRING_LITERAL:
    case T_RAW_C_STRING_LITERAL:
      mklit(expr, EL_C_STR, cur->literal);
      P_next(parser);
      break;
    case T_BYTE_LITERAL:
      mklit(expr, EL_BYTE, cur->literal);
      P_next(parser);
      break;
    case T_CHAR_LITERAL:
      mklit(expr, EL_CHAR, cur->literal);
      P_next(parser);
      break;
    case T_INTEGER:
      mklit(expr, EL_INT, cur->literal);
      P_next(parser);
      break;
    case T_FLOAT:
      mklit(expr, EL_FLOAT, cur->literal);
      P_next(parser);
      break;
    case T_TRUE:
      mklit(expr, EL_BOOL, "true");
      P_next(parser);
      break;
    case T_FALSE:
      mklit(expr, EL_BOOL, "false");
      P_next(parser);
      break;
    case T_BREAK:
      mkexpr(expr, E_BREAK);

      P_next(parser);
      if (cur->kind == T_LIFETIME) {
        expr->break_.label = strdup(cur->literal);
        P_next(parser);
      }

      if (can_begin_expr(cur->kind) &&
          (allow_struct || P_peek(parser)->kind != T_LBRACE)) {
        expr->break_.expr = E_read(parser);
      } else {
        expr->break_.expr = NULL;
      }
      break;
    case T_CONTINUE:
      mkexpr(expr, E_CONTINUE);

      P_next(parser);
      if (cur->kind == T_LIFETIME) {
        expr->continue_.label = strdup(cur->literal);
        P_next(parser);
      } else
        expr->continue_.label = NULL;
      break;
    case T_RETURN:
      mkexpr(expr, E_RETURN);

      P_next(parser);
      if (can_begin_expr(cur->kind))
        expr->return_.expr = E_read(parser);
      else
        expr->return_.expr = NULL;
      break;
    case T_ASYNC:
      P_next(parser);
      mkexpr(expr, E_ASYNC);
      expr->async.capture = P_consume(parser, T_MOVE);
      expr->async.stmts = ST_block(parser);
      break;
    case T_CONST:
      P_next(parser);
      mkblock(expr, true, false);
      expr->block.stmts = ST_block(parser);
      break;
    case T_UNSAFE:
      P_next(parser);
      mkblock(expr, false, true);
      expr->block.stmts = ST_block(parser);
      break;
    case T_TRY:
      if (P_peek(parser)->kind == T_LBRACE) {
        P_next(parser);
        mkexpr(expr, E_TRY_BLOCK);
        expr->try_block.stmts = ST_block(parser);
        return;
      }
      if (P_peek(parser)->kind != T_NOT && P_peek(parser)->kind != T_PATHSEP)
        break;

      // Fallthrough to path.
    case T_IDENT:
    case T_PATHSEP:
    case T_LT:
    case T_SELFVALUE:
    case T_SELFTYPE:
    case T_SUPER:
    case T_CRATE:
      PT_qpath(parser, &qself, &path, true);

      if (qself.position < 0 && PT_is_modstyle(&path) &&
          P_consume(parser, T_NOT)) {
        // This is certainly a macro.
        mkexpr(expr, E_MACRO);
        U_die("TODO: implement macros");
      }

      if (allow_struct && cur->kind == T_LBRACE) {
        U_die("TODO: implement struct");
      }

      // In all other cases assume this is a path.
      mkexpr(expr, E_PATH);
      expr->path.qself = qself;
      expr->path.path = path;

      break;
    case T_LBRACE:
      expr_block(parser, expr);
      break;
    case T_WHILE:
      expr_while(parser, expr);
      break;
    case T_LOOP:
      expr_loop(parser, expr);
      break;
    case T_FOR:
      expr_for_loop(parser, expr);
      break;
    case T_LPAREN:
      P_next(parser);

      if (cur->kind == T_RPAREN) {
        // Empty tuple.
        mkexpr(expr, E_TUPLE);
        buffer = U_pmkarray(1, sizeof(*buffer));
        expr_buffer_finish(&buffer);
        expr->tuple.elems = buffer;

        P_next(parser);
        return;
      }

      subexpr = E_read(parser);
      if (cur->kind == T_RPAREN) {
        // Parenthesized single expression.
        mkexpr(expr, E_PAREN);
        expr->paren.expr = subexpr;

        P_next(parser);
        return;
      }

      // Tuple.
      buffer = U_pmkarray(2, sizeof(*buffer));
      *expr_buffer_expand(&buffer) = subexpr;

      while (cur->kind != T_RPAREN) {
        P_expect(parser, T_COMMA, "comma in tuple");
        if (cur->kind == T_RPAREN) {
          break;
        }

        *expr_buffer_expand(&buffer) = E_read(parser);
      }
      P_next(parser);

      mkexpr(expr, E_TUPLE);
      expr_buffer_finish(&buffer);
      expr->tuple.elems = buffer;

      break;
    case T_LBRACKET:
      P_next(parser);
      if (cur->kind == T_RBRACKET) {
        // Empty array.
        P_next(parser);

        mkexpr(expr, E_ARRAY);
        buffer = U_pmkarray(1, sizeof(*buffer));
        expr_buffer_finish(&buffer);
        expr->array.elems = buffer;
        return;
      }

      subexpr = E_read(parser);

      if (cur->kind == T_RBRACKET || cur->kind == T_COMMA) {
        // Array.
        buffer = U_pmkarray(2, sizeof(*buffer));
        *expr_buffer_expand(&buffer) = subexpr;

        while (cur->kind != T_RBRACKET) {
          P_expect(parser, T_COMMA, "comma in array");
          if (cur->kind == T_RBRACKET)
            break;
          *expr_buffer_expand(&buffer) = E_read(parser);
        }

        P_next(parser);

        mkexpr(expr, E_ARRAY);
        expr_buffer_finish(&buffer);
        expr->array.elems = buffer;
      } else if (cur->kind == T_SEMI) {
        // Repeating expression `[0; 5]`.
        P_next(parser);

        mkexpr(expr, E_REPEAT);
        expr->repeat.expr = subexpr;
        expr->repeat.len = E_read(parser);
        P_expect(parser, T_RBRACKET, "bracket at end of repeat expression");
      } else
        T_error(&cur->loc, "expected `,` or `;`");

      break;
    default:
      T_error(&cur->loc, "expected primary expression");
  }
}

// Read a member, i.e. the path after a "dot" field.
static E_Member member_read(P_Parser* parser) {
  E_Member member;

  switch (cur->kind) {
    case T_IDENT:
      member.kind = MEM_NAME;
      member.name = strdup(cur->literal);
      break;
    case T_INTEGER:
      member.kind = MEM_TUPLE;
      member.tuple = atoi(cur->literal);
      break;
    default:
      T_error(&cur->loc, "expected identifier or integer");
  }

  P_next(parser);
  return member;
}

// Read a trailer, like a field, call or .await.
static void trailer_helper(P_Parser* parser, E_Expr** expr) {
  E_Expr *lhs, **buffer;
  E_Member member;

  while (1) {
    lhs = *expr;
    buffer = U_pmkarray(1, sizeof(*buffer));

    switch (cur->kind) {
      case T_LPAREN:
        // Function call.
        P_next(parser);
        *expr = U_palloc(sizeof(E_Expr));
        mkexpr(*expr, E_CALL);
        (*expr)->call.func = lhs;

        while (cur->kind != T_RPAREN) {
          *expr_buffer_expand(&buffer) = E_read(parser);
          if (cur->kind == T_RPAREN)
            break;
          P_expect(parser, T_COMMA, "expected `,` in argument list");
        }

        expr_buffer_finish(&buffer);
        (*expr)->call.args = buffer;
        P_next(parser);
        break;
      case T_DOT:
        P_next(parser);
        // TODO: multi-index

        if (cur->kind == T_AWAIT) {
          // .await expr
          P_next(parser);
          *expr = U_palloc(sizeof(E_Expr));
          mkexpr(*expr, E_AWAIT);
          (*expr)->await.expr = lhs;
          continue;
        }

        // This is a field or a method call.
        member = member_read(parser);
        // TODO: parser turbofish

        if (P_consume(parser, T_LPAREN) && member.kind == MEM_NAME) {
          // Member call.
          *expr = U_palloc(sizeof(E_Expr));
          mkexpr(*expr, E_METHOD_CALL);
          (*expr)->method_call.receiver = lhs;
          (*expr)->method_call.method = member.name;

          while (cur->kind != T_RPAREN) {
            *expr_buffer_expand(&buffer) = E_read(parser);
            if (cur->kind == T_RPAREN)
              break;
            P_expect(parser, T_COMMA, "expected `,` in argument list");
          }

          expr_buffer_finish(&buffer);
          (*expr)->method_call.args = buffer;
          P_next(parser);
          break;
        }

        // Field.
        *expr = U_palloc(sizeof(E_Expr));
        mkexpr(*expr, E_FIELD);
        (*expr)->field.expr = lhs;
        (*expr)->field.member = member;
        break;
      default:
        return;
    }

    // TODO: other exprs
  }
}

static void expr_trailer(P_Parser* parser, E_Expr** expr, bool allow_struct) {
  expr_primary(parser, *expr, allow_struct);
  trailer_helper(parser, expr);
}

// Read a trailer'd expression, and see if there are any unary operators.
static void expr_unary(P_Parser* parser, E_Expr** expr, bool allow_struct) {
  switch (cur->kind) {
    case T_AND:
      // Reference.
      mkexpr(*expr, E_REFERENCE);
      // TODO: Handle raw references
      P_next(parser);
      if (cur->kind == T_MUT) {
        (*expr)->reference.mut = true;
        P_next(parser);
      } else
        (*expr)->reference.mut = false;

      (*expr)->reference.expr = U_palloc(sizeof(E_Expr));
      expr_unary(parser, &(*expr)->reference.expr, allow_struct);
      break;
    case T_STAR:
    case T_NOT:
    case T_MINUS:
      // Unary arithmetic op or deref.
      mkexpr(*expr, E_UNARY);
      (*expr)->unary.op = cur->kind;
      (*expr)->unary.expr = U_palloc(sizeof(E_Expr));

      P_next(parser);
      expr_unary(parser, &(*expr)->unary.expr, allow_struct);
      break;
    default:
      expr_trailer(parser, expr, allow_struct);
  }
}

static void parse_expr(P_Parser* parser,
                       E_Expr** expr,
                       bool allow_struct,
                       enum precedence base);

// Parse the right-hand side of a binary expression.
static void parse_rhs(P_Parser* parser,
                      E_Expr** rhs,
                      bool allow_struct,
                      enum precedence precedence) {
  enum precedence next;

  expr_unary(parser, rhs, allow_struct);
  while (1) {
    next = peek_precedence(parser);
    if (next > precedence || (next == precedence && precedence == ASSIGN)) {
      parse_expr(parser, rhs, allow_struct, next);
    } else
      break;
  }
}

// Parse the end of a range.
static void parse_range_end(P_Parser* parser,
                            E_Expr** expr,
                            bool allow_struct,
                            bool is_closed) {
  if (!is_closed &&
      (cur->kind == T_EOF || cur->kind == T_COMMA || cur->kind == T_SEMI ||
       cur->kind == T_DOT || (!allow_struct && cur->kind == T_LBRACE))) {
    *expr = NULL;
  } else {
    *expr = U_palloc(sizeof(E_Expr));
    parse_rhs(parser, expr, allow_struct, RANGE);
  }
}

// Read an expression.
static void parse_expr(P_Parser* parser,
                       E_Expr** expr,
                       bool allow_struct,
                       enum precedence base) {
  enum precedence precedence;
  T_TokenKind binop;

  while (1) {
    E_Expr* lhs = *expr;

    if (lhs->kind == E_RANGE && lhs->range.right) {
      // A range with an upper bound cannot be the left hand side of an
      // expression.
      return;
    } else if (is_binop(cur->kind)) {
      binop = cur->kind;

      precedence = binop_precedence(binop);
      if (precedence < base ||
          (precedence == COMPARE && lhs->kind == E_BINARY &&
           binop_precedence(lhs->binary.op) == COMPARE))
        break;

      P_next(parser);

      *expr = U_palloc(sizeof(E_Expr));
      mkexpr(*expr, E_BINARY);
      (*expr)->binary.left = lhs;
      (*expr)->binary.op = binop;
      (*expr)->binary.right = U_palloc(sizeof(E_Expr));
      parse_rhs(parser, &(*expr)->binary.right, allow_struct, precedence);
    } else if (ASSIGN >= base && cur->kind == T_EQ) {
      P_next(parser);

      *expr = U_palloc(sizeof(E_Expr));
      mkexpr(*expr, E_ASSIGN);
      (*expr)->assign.left = lhs;
      (*expr)->assign.right = U_palloc(sizeof(E_Expr));
      parse_rhs(parser, &(*expr)->assign.right, allow_struct, ASSIGN);
    } else if (RANGE >= base &&
               (cur->kind == T_DOTDOT || cur->kind == T_DOTDOTEQ)) {
      *expr = U_palloc(sizeof(E_Expr));
      mkexpr(*expr, E_RANGE);
      (*expr)->range.left = lhs;
      (*expr)->range.is_closed = cur->kind == T_DOTDOTEQ;

      P_next(parser);
      parse_range_end(parser, &(*expr)->range.right, allow_struct,
                      (*expr)->range.is_closed);
    } else if (CAST >= base && cur->kind == T_AS) {
      P_next(parser);

      *expr = U_palloc(sizeof(E_Expr));
      mkexpr(*expr, E_CAST);
      (*expr)->cast.expr = lhs;
      (*expr)->cast.type = TY_read_caststyle(parser);
    } else
      return;

    // TODO: Other binary expressions.
  }
}

E_Expr* expr_ambiguous(P_Parser* parser, bool allow_struct) {
  E_Expr* expr = U_palloc(sizeof(E_Expr));
  expr_unary(parser, &expr, allow_struct);
  parse_expr(parser, &expr, allow_struct, 0);
  return expr;
}

E_Expr* E_read(P_Parser* parser) {
  return expr_ambiguous(parser, true);
}

E_Expr* E_read_earlier_boundary_rule(P_Parser* parser) {
  // TODO: Read attributes

  E_Expr* expr = U_palloc(sizeof(E_Expr));
  bool keep_parsing = false;

  switch (cur->kind) {
    case T_CONST:
      P_next(parser);
      mkblock(expr, true, false);
      expr->block.stmts = ST_block(parser);
      break;
    case T_UNSAFE:
      P_next(parser);
      mkblock(expr, false, true);
      expr->block.stmts = ST_block(parser);
      break;
    case T_LBRACE:
      expr_block(parser, expr);
      break;
    case T_WHILE:
      expr_while(parser, expr);
      break;
    case T_LOOP:
      expr_loop(parser, expr);
      break;
    default:
      expr_unary(parser, &expr, true);
      keep_parsing = true;
  }

  if (keep_parsing) {
    parse_expr(parser, &expr, true, 0);
    return expr;
  }

  // TODO: trailers
  return expr;
}

bool E_requires_semi_to_be_stmt(const E_Expr* expr) {
  switch (expr->kind) {
    default:
      return E_requires_comma_to_be_match_arm(expr);
  }
}

bool E_requires_comma_to_be_match_arm(const E_Expr* expr) {
  switch (expr->kind) {
    default:
      return true;
  }
}

static void member_dump(FILE* f, E_Member method) {
  switch (method.kind) {
    case MEM_NAME:
      fprintf(f, "MEM_NAME %s", method.name);
      break;
    case MEM_TUPLE:
      fprintf(f, "MEM_TUPLE %lu", method.tuple);
      break;
  }
}

void E_dump(FILE* f, const E_Expr* expr, int indent) {
  E_Expr** subexpr;

  U_indent(f, indent);
  switch (expr->kind) {
    case E_NONE:
      fputs("E_NONE\n", f);
      break;
    case E_ARRAY:
      fputs("E_ARRAY:\n", f);
      for (subexpr = expr->array.elems; (*subexpr)->kind != E_NONE; subexpr++)
        E_dump(f, *subexpr, indent + 1);
      break;
    case E_ASSIGN:
      fputs("E_ASSIGN:\n", f);
      E_dump(f, expr->assign.left, indent + 1);
      E_dump(f, expr->assign.right, indent + 1);
      break;
    case E_ASYNC:
      fprintf(f, "E_ASYNC: ");
      if (expr->async.capture)
        fputs("NO_CAPTURE\n", f);
      else
        fputs("CAPTURE\n", f);
      ST_dumpblock(f, expr->async.stmts, indent + 1);
      break;
    case E_AWAIT:
      fputs("E_AWAIT:\n", f);
      E_dump(f, expr->await.expr, indent + 1);
      break;
    case E_BINARY:
      fprintf(f, "E_BINARY: %s\n", T_desc(expr->binary.op));
      E_dump(f, expr->binary.left, indent + 1);
      E_dump(f, expr->binary.right, indent + 1);
      break;
    case E_BLOCK:
      fprintf(f, "E_BLOCK: ");
      if (expr->block.is_const)
        fprintf(f, "IS_CONST ");
      if (expr->block.is_unsafe)
        fprintf(f, "IS_UNSAFE ");
      if (expr->block.label)
        fprintf(f, "%s\n", expr->block.label);
      else
        fputs("NO_LABEL\n", f);
      ST_dumpblock(f, expr->block.stmts, indent + 1);
      break;
    case E_BREAK:
      fprintf(f, "E_BREAK: ");
      if (expr->break_.label)
        fprintf(f, "%s ", expr->break_.label);
      else
        fprintf(f, "NO_LABEL ");
      if (expr->break_.expr) {
        fputs("\n", f);
        E_dump(f, expr->break_.expr, indent + 1);
      } else
        fputs("NO_EXPR\n", f);
      break;
    case E_CALL:
      fputs("E_CALL\n", f);
      E_dump(f, expr->call.func, indent + 1);
      U_indent(f, indent);
      fputs("---ARGS---\n", f);
      for (subexpr = expr->call.args; (*subexpr)->kind != E_NONE; subexpr++)
        E_dump(f, *subexpr, indent + 1);
      break;
    case E_CAST:
      fputs("E_CAST\n", f);
      E_dump(f, expr->cast.expr, indent + 1);
      TY_dump(f, expr->cast.type, indent + 1);
      break;
    case E_CONTINUE:
      fprintf(f, "E_CONTINUE: ");
      if (expr->continue_.label)
        fprintf(f, "%s\n", expr->continue_.label);
      else
        fputs("NO_LABEL\n", f);
      break;
    case E_FIELD:
      fprintf(f, "E_FIELD: ");
      member_dump(f, expr->field.member);
      fputs("\n", f);
      E_dump(f, expr->field.expr, indent + 1);
      break;
    case E_FOR_LOOP:
      fprintf(f, "E_FOR_LOOP: ");
      if (expr->for_loop.label)
        fprintf(f, "%s ", expr->for_loop.label);
      else
        fprintf(f, "NO_LABEL ");

      fputs("\n", f);
      PN_dump(f, expr->for_loop.pat, indent + 1);
      E_dump(f, expr->for_loop.expr, indent + 1);
      ST_dumpblock(f, expr->for_loop.stmts, indent + 1);
      break;
    case E_LIT:
      fprintf(f, "E_LIT: %s\n", expr->lit.lit);
      break;
    case E_LOOP:
      fprintf(f, "E_LOOP:");
      if (expr->loop.label)
        fprintf(f, "%s\n", expr->loop.label);
      else
        fputs("NO_LABEL\n", f);
      ST_dumpblock(f, expr->loop.stmts, indent + 1);
      break;
    case E_PAREN:
      fputs("E_PAREN:\n", f);
      E_dump(f, expr->paren.expr, indent + 1);
      break;
    case E_PATH:
      fprintf(f, "E_PATH: ");
      if (expr->path.qself.position >= 0)
        U_die("TODO: print qself");
      PT_dump(f, &expr->path.path);
      break;
    case E_RANGE:
      fprintf(f, "E_RANGE: ");
      if (expr->range.is_closed)
        fputs("CLOSED\n", f);
      else
        fputs("OPEN\n", f);

      E_dump(f, expr->range.left, indent + 1);
      E_dump(f, expr->range.right, indent + 1);
      break;
    case E_REFERENCE:
      fprintf(f, "E_REFERENCE: ");
      if (expr->reference.mut)
        fputs("MUT\n", f);
      else
        fputs("NOT MUT\n", f);
      E_dump(f, expr->reference.expr, indent + 1);
      break;
    case E_REPEAT:
      fputs("E_REPEAT:\n", f);
      E_dump(f, expr->repeat.expr, indent + 1);
      E_dump(f, expr->repeat.len, indent + 1);
      break;
    case E_RETURN:
      fprintf(f, "E_RETURN: ");
      if (expr->return_.expr) {
        fputs("\n", f);
        E_dump(f, expr->return_.expr, indent + 1);
      } else
        fputs("NO_EXPR\n", f);
      break;
    case E_TUPLE:
      fputs("E_TUPLE:\n", f);
      for (subexpr = expr->tuple.elems; (*subexpr)->kind != E_NONE; subexpr++)
        E_dump(f, *subexpr, indent + 1);
      break;
    case E_TRY_BLOCK:
      fputs("E_TRY_BLOCK:\n", f);
      ST_dumpblock(f, expr->try_block.stmts, indent + 1);
      break;
    case E_UNARY:
      fprintf(f, "E_UNARY: %s\n", T_desc(expr->unary.op));
      E_dump(f, expr->unary.expr, indent + 1);
      break;
    case E_WHILE:
      fprintf(f, "E_WHILE:");
      if (expr->while_.label)
        fprintf(f, "%s\n", expr->while_.label);
      else
        fputs("NO_LABEL\n", f);
      E_dump(f, expr->while_.cond, indent + 1);
      ST_dumpblock(f, expr->while_.stmts, indent + 1);
      break;
  }
}
