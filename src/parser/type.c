// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

#include "dozer.h"
#include "util/util.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define cur P_cur(parser)

PTR_BUFFER_FUNCS(type, TY_Type, TY_NONE)

static void type_path_like(P_Parser* parser, TY_Type* ty) {
  PT_QSelf qself;
  PT_Path path;

  PT_qpath(parser, &qself, &path, false);

  if (qself.position >= 0) {
    ty->kind = TY_PATH;
    ty->path.qself = qself;
    ty->path.path = path;
    return;
  } else if (cur->kind == T_NOT && PT_is_modstyle(&path))
    U_die("support macros");

  // By default this is a regular path.
  ty->kind = TY_PATH;
  ty->path.qself = qself;
  ty->path.path = path;
}

static TY_Type* type_ambiguous(P_Parser* parser,
                               bool allow_plus,
                               bool allow_group) {
  TY_Type *ty = U_palloc(sizeof(*ty)), *subtype, **buffer;
  ty->sym = NULL;

  switch (cur->kind) {
    case T_LPAREN:
      P_next(parser);
      if (P_consume(parser, T_RPAREN)) {
        // Empty tuple.
        buffer = U_pmkarray(1, sizeof(*buffer));
        ty->kind = TY_TUPLE;
        type_buffer_finish(&buffer);
        ty->tuple.elems = buffer;
        break;
      } else if (cur->kind == T_LIFETIME)
        U_die("TODO: trait objects");
      else if (cur->kind == T_QUESTION)
        U_die("TODO: trait object sized bound");

      subtype = TY_read(parser);
      if (cur->kind == T_COMMA) {
        // Tuple.
        ty->kind = TY_TUPLE;
        buffer = U_pmkarray(1, sizeof(*buffer));
        *type_buffer_expand(&buffer) = subtype;
        P_next(parser);

        while (cur->kind != T_RPAREN) {
          *type_buffer_expand(&buffer) = TY_read(parser);
          if (cur->kind == T_RPAREN)
            break;
          P_expect(parser, T_COMMA, "comma in type tuple");
        }

        P_next(parser);
        type_buffer_finish(&buffer);
        ty->tuple.elems = buffer;
      } else if (allow_plus && cur->kind == T_PLUS) {
        U_die("TODO: trait bounds");
      } else {
        ty->kind = TY_PAREN;
        ty->paren.ty = subtype;
      }

      break;
    case T_IDENT:
    case T_SUPER:
    case T_SELFVALUE:
    case T_SELFTYPE:
    case T_CRATE:
    case T_PATHSEP:
    case T_LT:
      type_path_like(parser, ty);
      break;
    case T_LBRACKET:
      P_next(parser);
      subtype = TY_read(parser);

      if (P_consume(parser, T_SEMI)) {
        ty->kind = TY_ARRAY;
        ty->array.ty = subtype;
        ty->array.len = E_read(parser);
        P_expect(parser, T_RBRACKET, "end of array type");
      } else if (P_consume(parser, T_RBRACKET)) {
        ty->kind = TY_SLICE;
        ty->slice.ty = subtype;
      } else
        T_error(&cur->loc,
                "expected right bracket or semicolon for array type");
      break;
    case T_STAR:
      P_next(parser);

      if (P_consume(parser, T_CONST))
        ty->ptr.mutability = TYP_CONST;
      else if (P_consume(parser, T_MUT))
        ty->ptr.mutability = TYP_MUT;
      else
        T_error(&cur->loc, "expected 'const' or 'mut'");

      ty->kind = TY_PTR;
      ty->ptr.ty = TY_read(parser);
      break;
    default:
      T_error(&cur->loc, "expected type");
  }

  return ty;
}

TY_Type* TY_read_caststyle(P_Parser* parser) {
  return type_ambiguous(parser, false, false);
}

TY_Type* TY_read(P_Parser* parser) {
  return type_ambiguous(parser, true, true);
}

static TY_Type* parse_return_type(P_Parser* parser, bool allow_plus) {
  if (P_consume(parser, T_RARROW))
    return type_ambiguous(parser, allow_plus, true);
  else
    return NULL;
}

TY_Type* TY_returntype_read(P_Parser* parser) {
  return parse_return_type(parser, true);
}

TY_Type* TY_returntype_read_noplus(P_Parser* parser) {
  return parse_return_type(parser, false);
}

void TY_dump(FILE* f, const TY_Type* ty, int indent) {
  TY_Type** slot;

  U_indent(f, indent);
  switch (ty->kind) {
    case TY_NONE:
      fputs("TY_NONE\n", f);
      break;
    case TY_ARRAY:
      fputs("TY_ARRAY\n", f);
      TY_dump(f, ty->array.ty, indent + 1);
      E_dump(f, ty->array.len, indent + 1);
      break;
    case TY_PAREN:
      fputs("TY_PAREN\n", f);
      TY_dump(f, ty->paren.ty, indent + 1);
      break;
    case TY_PATH:
      fprintf(f, "TY_PATH ");
      if (ty->path.qself.position >= 0)
        U_die("TODO: print qself");
      PT_dump(f, &ty->path.path);
      break;
    case TY_PTR:
      fprintf(f, "TY_PTR %s\n",
              ty->ptr.mutability == TYP_CONST ? "TYP_CONST" : "TYP_MUT");
      TY_dump(f, ty->ptr.ty, indent + 1);
      break;
    case TY_SLICE:
      fputs("TY_SLICE\n", f);
      TY_dump(f, ty->slice.ty, indent + 1);
      break;
    case TY_TUPLE:
      fputs("TY_TUPLE\n", f);
      for (slot = ty->tuple.elems; (*slot)->kind != TY_NONE; slot++)
        TY_dump(f, *slot, indent + 1);
      break;
  }
}
