// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

#include "dozer.h"
#include "util/util.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define cur P_cur(parser)

static void parse_local(P_Parser* parser, ST_Stmt* stmt) {
  P_expect(parser, T_LET, "expected let");
  stmt->local.pat = PN_read_one(parser);

  if (P_consume(parser, T_COLON))
    stmt->local.type = TY_read(parser);
  else
    stmt->local.type = NULL;

  if (P_consume(parser, T_EQ)) {
    stmt->local.expr = E_read(parser);
  } else
    stmt->local.expr = NULL;

  P_expect(parser, T_SEMI, "expected semicolon");
}

static void parse_stmt(P_Parser* parser,
                       ST_Stmt* stmt,
                       bool allow_nosemi,
                       bool* has_semi) {
  // TODO: Parse item
  E_Expr* expr;

  if (cur->kind == T_LET) {
    stmt->kind = ST_LOCAL;
    parse_local(parser, stmt);
    if (has_semi)
      *has_semi = true;
    return;
  }

  expr = E_read_earlier_boundary_rule(parser);

  // TODO: deal with attributes

  if (cur->kind == T_SEMI) {
    if (has_semi)
      *has_semi = true;
    P_next(parser);
  } else if (!allow_nosemi && E_requires_semi_to_be_stmt(expr))
    T_error(&cur->loc, "expected semicolon");

  stmt->kind = ST_EXPR;
  stmt->expr.expr = expr;
  stmt->expr.has_semicolon = has_semi ? *has_semi : false;
}

ST_Stmt* ST_block(P_Parser* parser) {
  ST_Stmt *stmt, *stmts;
  bool has_semi, requires_semi;

  stmts = U_pmkarray(1, sizeof(*stmts));
  P_expect(parser, T_LBRACE, "opening for block");

  while (1) {
    // Eat empty semicolons.
    while (cur->kind == T_SEMI) {
      stmt = U_expand(&stmts, 1);
      stmt->kind = ST_EXPR;
      stmt->expr.expr = U_palloc(sizeof(E_Expr));
      stmt->expr.expr->kind = E_EMPTY;

      P_next(parser);
    }

    // Eat the end of the block if possible.
    if (cur->kind == T_RBRACE)
      break;

    has_semi = false;
    stmt = U_expand(&stmts, 1);
    parse_stmt(parser, stmt, true, &has_semi);

    switch (stmt->kind) {
      case ST_EXPR:
        requires_semi =
            !has_semi && E_requires_semi_to_be_stmt(stmt->expr.expr);
        break;
      default:
        requires_semi = false;
    }

    if (cur->kind == T_RBRACE)
      break;
    else if (requires_semi)
      T_error(&cur->loc, "unexpected token, expected `;`, got %s",
              T_desc(cur->kind));
  }

  P_next(parser);
  stmt = U_expand(&stmts, 1);
  stmt->kind = ST_NONE;
  return stmts;
}

ST_Stmt* ST_evalexpr(ST_Stmt* stmt) {
  ST_Stmt* found = NULL;

  for (; stmt->kind != ST_NONE; stmt++)
    if (stmt->kind == ST_EXPR)
      if (stmt->expr.expr->kind == E_EMPTY)
        found = NULL;
      else
        found = stmt;

  return found;
}

void ST_dumpblock(FILE* f, const ST_Stmt* stmts, int indent) {
  const ST_Stmt* s;

  for (s = stmts; s->kind != ST_NONE; s++) {
    U_indent(f, indent);
    switch (s->kind) {
      case ST_LOCAL:
        fputs("ST_LOCAL: TODO\n", f);
        break;
      case ST_EXPR:
        fputs("ST_EXPR\n", f);
        E_dump(f, s->expr.expr, indent);
        break;
    }
  }
}
