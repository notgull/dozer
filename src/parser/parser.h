#ifndef DOZER_PARSER_H
#define DOZER_PARSER_H

#include <stddef.h>
#include "util/util.h"

typedef struct item I_Item;
typedef struct parser P_Parser;
typedef struct sourcefile P_SourceFile;

typedef struct signature I_Signature;

typedef struct generic_item I_GenericItem;
typedef struct generics I_Generics;
typedef struct fn_arg I_FnArg;
typedef struct foreign_item I_ForeignItem;
typedef struct impl_item I_ImplItem;
typedef struct trait_item I_TraitItem;
typedef struct struct_field I_StructField;
typedef struct vis I_Vis;
typedef struct enum_variant I_EnumVariant;

typedef struct pattern PN_Pat;
typedef struct stmt ST_Stmt;
typedef struct type TY_Type;
typedef enum binop E_BinOp;
typedef struct member E_Member;

typedef struct expr E_Expr;
typedef struct typestatus R_TypeStatus;

struct member {
  enum { MEM_NAME = 0, MEM_TUPLE } kind;
  union {
    char* name;
    uint64_t tuple;
  };
};

typedef struct path PT_Path;
typedef struct pathseg PT_PathSegment;
typedef struct qself PT_QSelf;

struct qself {
  // TODO: type
  int position;
};

struct path {
  bool has_leading_colon;
  PT_PathSegment* segs;  // Array of PT_PathSegment
};

struct pathseg {
  char* ident;
  enum { PTY_NONE = 0, PTY_BASIC, PTY_ANGLE_BRACKETED, PTY_PARENTHESIZED } kind;
};

struct stmt {
  enum {
    ST_NONE = 0,
    ST_LOCAL,
    ST_EXPR,
  } kind;
  union {
    struct {
      PN_Pat* pat;
      TY_Type* type;
      E_Expr* expr;
      // TODO: Do we want to handle else-if?
    } local;
    struct {
      E_Expr* expr;
      bool has_semicolon;
    } expr;
  };
};

typedef enum primitive G_Primitive;
typedef struct symbol G_Sym;
typedef struct symbol_table G_SymTable;
typedef struct typenode G_TypeNode;

#define UNKNOWN (-1)

enum primitive {
  GP_UNIT = 0,
  GP_I8,
  GP_U8,
  GP_I16,
  GP_U16,
  GP_I32,
  GP_U32,
  GP_I64,
  GP_U64,
  GP_I128,
  GP_U128,
  GP_ISIZE,
  GP_USIZE,
  GP_F16,
  GP_F32,
  GP_F64,
  GP_F128,
  GP_BOOL,
  GP_CHAR
};

struct typenode {
  enum {
    GT_NONE = 0,

    GT_FNPTR,
    GT_PRIMITIVE,
    GT_REFERENCE
  } kind;

  size_t size;  // UNKNOWN if not known
  size_t align;
  G_Sym* owner;

  M_Map trait_impls;  // map between trait objname and G_Sym*

  union {
    struct {
      I_Signature* sig;
      G_Sym* return_type;
      G_Sym** arguments;
    } fnptr;
    struct {
      bool is_signed;
      bool is_float;
      G_Primitive kind;
    } primitive;
    struct {
      bool is_mut;
      G_Sym* subtype;
    } reference;
  };

  // TODO:
  // - known impl items
  // - known impl'd traits
};

struct symbol {
  enum {
    GS_NONE = 0,
    GS_TEMPPATH,

    GS_CRATE,
    GS_MODULE,
    GS_FN,
    GS_TRAIT,
    GS_TRAIT_IMPL,
    GS_TYPE
  } kind;

  char *name, *obj_name;
  PT_Path full_path;
  void* origin_ast;
  G_Sym* parent;
  G_SymTable* subitems;

  union {
    struct {
      I_Signature* sig;
      G_Sym* fn_type;
      enum {
        GRECV_NONE = 0,
        GRECV_SELF,
      } receiver;
    } fn;
    G_TypeNode* type;
  };
};

struct typestatus {
  enum {
    RT_UNKNOWN = 0,
    RT_DOESNT_MATTER,
    RT_INTEGER,
    RT_FLOAT,
    RT_SPECIFIED,
    RT_REF
  } kind;
  union {
    G_Sym* sym;
    struct typestatus* other;
  };
};

struct expr {
  enum {
    E_NONE = 0,
    E_EMPTY,

    E_ARRAY,
    E_ASSIGN,
    E_ASYNC,
    E_AWAIT,
    E_BINARY,
    E_BLOCK,
    E_BREAK,
    E_CALL,
    E_CAST,
    E_CONTINUE,
    E_FIELD,
    E_FOR_LOOP,
    E_MACRO,
    E_METHOD_CALL,
    E_LIT,
    E_LOOP,
    E_PAREN,
    E_PATH,
    E_RANGE,
    E_REFERENCE,
    E_REPEAT,
    E_RETURN,
    E_TUPLE,
    E_TRY_BLOCK,
    E_UNARY,
    E_WHILE
  } kind;
  union {
    struct {
      E_Expr** elems;
    } array;
    struct {
      E_Expr* left;
      E_Expr* right;
    } assign;
    struct {
      E_Expr* expr;
    } await;
    struct {
      bool capture;
      ST_Stmt* stmts;
    } async;
    struct {
      E_Expr* left;
      T_TokenKind op;
      E_Expr* right;
    } binary;
    struct {
      bool is_unsafe;
      bool is_const;
      char* label;
      ST_Stmt* stmts;
      E_Expr* evals_to;
    } block;
    struct {
      char* label;
      E_Expr* expr;
    } break_;
    struct {
      E_Expr* func;
      E_Expr** args;
    } call;
    struct {
      E_Expr* expr;
      TY_Type* type;
    } cast;
    struct {
      char* label;
    } continue_;
    struct {
      E_Expr* expr;
      E_Member member;
    } field;
    struct {
      char* label;
      PN_Pat* pat;
      E_Expr* expr;
      ST_Stmt* stmts;
    } for_loop;
    struct {
      char* lit;
      enum {
        EL_STR,
        EL_BYTE_STR,
        EL_C_STR,
        EL_BYTE,
        EL_CHAR,
        EL_INT,
        EL_FLOAT,
        EL_BOOL
      } kind;
    } lit;
    struct {
      char* label;
      ST_Stmt* stmts;
    } loop;
    struct {
      E_Expr* receiver;
      char* method;
      // TODO: Turbofish!
      E_Expr** args;
      G_Sym* method_sym;
    } method_call;
    struct {
      E_Expr* expr;
    } paren;
    struct {
      PT_QSelf qself;
      PT_Path path;

      enum { PTK_LOCAL, PTK_GLOBAL } kind;
      union {
        E_Expr* local;
        G_Sym* sym;
      };
    } path;
    struct {
      E_Expr* left;
      bool is_closed;
      E_Expr* right;
    } range;
    struct {
      bool mut;
      E_Expr* expr;
    } reference;
    struct {
      E_Expr* expr;
      E_Expr* len;
    } repeat;
    struct {
      E_Expr* expr;
    } return_;
    struct {
      E_Expr** elems;
    } tuple;
    struct {
      ST_Stmt* stmts;
    } try_block;
    struct {
      T_TokenKind op;
      E_Expr* expr;
    } unary;
    struct {
      char* label;
      E_Expr* cond;
      ST_Stmt* stmts;
    } while_;
  };

  R_TypeStatus status;

  // Compiled bytecode.
  uint32_t reg;
  B_Writer dir;
};

struct type {
  enum {
    TY_NONE = 0,

    TY_ARRAY,
    TY_PAREN,
    TY_PATH,
    TY_PTR,
    TY_SLICE,
    TY_TUPLE
  } kind;
  G_Sym* sym;
  union {
    struct {
      TY_Type* ty;
      E_Expr* len;
    } array;
    struct {
      TY_Type* ty;
    } paren;
    struct {
      PT_QSelf qself;
      PT_Path path;
    } path;
    struct {
      enum { TYP_CONST = 0, TYP_MUT } mutability;
      TY_Type* ty;
    } ptr;
    struct {
      TY_Type* ty;
    } slice;
    struct {
      TY_Type** elems;
    } tuple;
  };
};

struct generic_item {
  enum { GI_NONE = 0, GI_TYPE, GI_LIFETIME, GI_CONST } kind;
  union {
    struct {
      char* name;
    } type;
    struct {
      char* name;
    } lifetime;
    struct {
      char* name;
      TY_Type* type;
    } const_;
  };
};

struct generics {
  I_GenericItem* items;
};

typedef struct pattern_type PN_PatType;

struct pattern_type {
  PN_Pat* pat;
  TY_Type* ty;
};

struct pattern {
  enum {
    PN_NONE = 0,

    PN_IDENT,
    PN_OR,
    PN_PATH,
  } kind;
  union {
    struct {
      bool by_ref;
      bool mutability;
      char* ident;
      PN_Pat* subpattern;
    } ident;
    struct {
      PN_Pat** patterns;
    } or ;
    struct {
      PT_QSelf qself;
      PT_Path path;
    } path;
  };
};

struct fn_arg {
  enum { FNARG_NONE = 0, FNARG_RECEIVER, FNARG_TYPED } kind;
  union {
    struct {
      // TODO: Attributes
      bool is_reference;
      char* lifetime;
      bool is_mut;
      TY_Type* ty;
    } receiver;
    PN_PatType typed;
  };
};

struct vis {
  enum { VIS_INHERITED, VIS_PUBLIC, VIS_RESTRICTED } kind;
  PT_Path path;
};

struct signature {
  bool is_const;
  bool is_async;
  bool is_unsafe;
  bool is_extern;
  char* abi;
  char* name;
  I_Generics generics;
  I_FnArg* fn_args;
  bool has_variadic;
  PN_Pat* variadic;
  TY_Type* return_type;
};

struct foreign_item {
  enum {
    FI_NONE = 0,

    FI_FN
  } kind;
  G_Sym* sym;
  union {
    struct {
      I_Vis vis;
      I_Signature sig;
    } fn;
  };
};

struct impl_item {
  enum {
    II_NONE = 0,

    II_FN
  } kind;
  G_Sym* sym;
  union {
    struct {
      I_Vis vis;
      bool is_default;
      I_Signature sig;
      ST_Stmt* stmts;
    } fn;
  };
};

struct item {
  enum {
    I_NONE = 0,

    I_FN,
    I_FOREIGN_MOD,
    I_IMPL,
    I_TRAIT,
    I_ITEM_STRUCT,
    I_TUPLE_STRUCT,
    I_UNIT_STRUCT,
    I_ENUM,
  } kind;
  G_Sym* sym;
  G_Sym* parent;
  union {
    struct {
      I_Vis vis;
      I_Signature sig;
      ST_Stmt* stmts;
    } fn;
    struct {
      char* abi;
      I_ForeignItem* items;
    } foreign_mod;
    struct {
      bool is_default;
      bool is_unsafe;
      I_Generics generics;
      bool is_not;
      PT_Path trait;
      TY_Type* self_ty;
      I_ImplItem* items;
    } impl;
    struct {
      I_Vis vis;
      char* name;
      I_Generics generics;
      I_TraitItem* items;
    } trait;
    struct {
      I_Vis vis;
      char* name;
      I_Generics generics;
      I_StructField* fields;
    } item_struct;
    struct {
      I_Vis vis;
      char* name;
      I_Generics generics;
      TY_Type** fields;
    } tuple_struct;
    struct {
      I_Vis vis;
      char* name;
    } unit_struct;
    struct {
      I_Vis vis;
      char* name;
      I_Generics generics;
      I_EnumVariant* variants;
    } enum_;
  };
};

struct trait_item {
  enum {
    TI_NONE = 0,

    TI_FN,
  } kind;
  union {
    struct {
      I_Signature sig;
      ST_Stmt* defaults;
    } fn;
  };
};

struct struct_field {
  char* name;
  TY_Type* type;
};

struct enum_variant {
  char* name;
  E_Expr* disc;
  enum {
    IE_NONE = 0,
    IE_UNIT,
    IE_TUPLE,
    IE_STRUCT,
  } kind;
  union {
    TY_Type** tuple_fields;
    I_StructField* struct_fields;
  };
};

extern U_Arena* parsearena;
#define U_palloc(len) U_aalloc(parsearena, len)
#define U_pmkarray(cap, item_size) U_mkarray(parsearena, cap, item_size)

struct sourcefile {
  const char* filepath;
  I_Item** items;
  U_Arena* arena;
};

// Read in a source file.
P_SourceFile P_readfile(const char* name);
void P_freefile(P_SourceFile sf);

#endif /* DOZER_PARSER_H */
