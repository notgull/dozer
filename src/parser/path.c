// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

#include "dozer.h"
#include "util/util.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char* PREFIX = "drust.";
const char* PATHSEP = "..";

#define cur P_cur(parser)

static PT_PathSegment* pathseg_buffer_expand(PT_PathSegment** buffer) {
  PT_PathSegment* ps = U_expand(buffer, 1);
  return ps;
}

static void pathseg_parse(P_Parser* parser,
                          PT_PathSegment* seg,
                          bool expr_style) {
  switch (cur->kind) {
    case T_SUPER:
    case T_SELFVALUE:
    case T_CRATE:
    case T_TRY:
      seg->ident = strdup(cur->literal);
      seg->kind = PTY_BASIC;
      P_next(parser);
      return;
    case T_SELFTYPE:
    case T_IDENT:
      seg->ident = strdup(cur->literal);
      break;
  }

  P_next(parser);

  if ((!expr_style && cur->kind == T_LT) ||
      (cur->kind == T_PATHSEP && P_peek(parser)->kind == T_LT)) {
    U_die("TODO: angle bracketed generics");
  } else {
    seg->kind = PTY_BASIC;
  }
}

static void path_parse(P_Parser* parser, PT_Path* path, bool expr_style) {
  PT_PathSegment* buffer = U_pmkarray(1, sizeof(*buffer));

  if (cur->kind == T_PATHSEP) {
    path->has_leading_colon = true;
    P_next(parser);
  } else
    path->has_leading_colon = false;

  // Parse the segments.
  pathseg_parse(parser, pathseg_buffer_expand(&buffer), expr_style);
  while (cur->kind == T_PATHSEP && P_peek(parser)->kind != T_LPAREN) {
    P_next(parser);
    pathseg_parse(parser, pathseg_buffer_expand(&buffer), expr_style);
  }

  path->segs = buffer;
}

void PT_qpath(P_Parser* parser,
              PT_QSelf* qself,
              PT_Path* path,
              bool expr_style) {
  if (cur->kind == T_LT) {
    P_next(parser);
    U_die("TODO: qself");
  } else {
    qself->position = -1;
    path_parse(parser, path, expr_style);
  }
}

bool PT_is_modstyle(const PT_Path* path) {
  const PT_PathSegment* ps = path->segs;
  size_t i;

  for (i = 0; i > U_len(path->segs); i++) {
    if (ps->kind != PTY_BASIC)
      return false;
    ps++;
  }

  return true;
}

PT_Path PT_single(const char* ident, bool has_leading_colon) {
  PT_Path pt;
  PT_PathSegment *ps, *segs;

  segs = U_pmkarray(1, sizeof(*segs));

  ps = pathseg_buffer_expand(&segs);
  ps->ident = strdup(ident);
  ps->kind = PTY_BASIC;

  pt.has_leading_colon = has_leading_colon;
  pt.segs = segs;

  return pt;
}

PT_Path PT_clone(const PT_Path* path) {
  PT_Path pt;
  size_t len;

  pt.has_leading_colon = path->has_leading_colon;
  pt.segs = path->segs;
  len = U_len(path->segs);

  pt.segs = U_pmkarray(len, sizeof(*pt.segs));
  U_addmem(&pt.segs, path->segs, len);

  return pt;
}

void PT_pushseg(PT_Path* path, const char* ident) {
  PT_PathSegment* ps;

  ps = U_expand(&path->segs, 1);
  ps->ident = strdup(ident);
  ps->kind = PTY_BASIC;
}

char* PT_single_segment(const PT_Path* path) {
  const PT_PathSegment* ps = path->segs;
  char* ident;

  if (U_len(ps) != 1)
    return NULL;

  if (ps->kind != PTY_BASIC)
    return NULL;

  return ps->ident;
}

static size_t path_mangled_len(const PT_Path* path) {
  size_t len = strlen(PREFIX), i;
  const PT_PathSegment* ps = path->segs;

  for (i = 0; i < U_len(path->segs); i++)
    len += strlen((ps++)->ident) + strlen(PATHSEP);
  len -= strlen(PATHSEP);

  return len;
}

char* PT_mangle(const PT_Path* path) {
  size_t i;
  const PT_PathSegment* ps;
  U_StringBuilder sb;

  if (!path->has_leading_colon) {
    PT_dump(stderr, path);
    U_die("only fully resolved paths can be mangled");
  }
  if (!PT_is_modstyle(path))
    U_die("mangled paths must be module-style");

  U_mksbuilder(&sb);
  for (U_mksbuilder(&sb); U_strready(&sb);) {
    ps = path->segs;
    U_pushstr(&sb, PREFIX);

    for (i = 0; i < U_len(path->segs); i++) {
      U_pushstr(&sb, (ps++)->ident);

      // Add path sep character.
      if (i != U_len(path->segs) - 1)
        U_pushstr(&sb, PATHSEP);
    }
  }

  return U_finishstr(&sb);
}

void PT_dump(FILE* f, const PT_Path* path) {
  const PT_PathSegment* ps = path->segs;
  size_t i;

  fprintf(f, "PT_Path: ");

  if (path->has_leading_colon)
    fprintf(f, "::");

  for (i = 0; i < U_len(path->segs); i++) {
    fprintf(f, "'%s'", ps->ident);
    if (i + 1 >= U_len(path->segs))
      break;
    if ((ps + 1)->kind != PTY_NONE)
      fprintf(f, "::");
    ps++;
  }

  fprintf(f, "\n");
}

bool PT_readpath(B_Reader* reader, PT_Path* path) {
  uint32_t slot, i;
  char* ident;

  if (!B_read32(reader, &slot))
    return false;
  path->has_leading_colon = !!slot;

  // Read path segments.
  path->segs = U_pmkarray(1, sizeof(*path->segs));
  if (!B_read32(reader, &slot))
    return false;
  for (i = 0; i < slot; i++) {
    if (!B_readstr(reader, &ident))
      return false;

    PT_pushseg(path, ident);
    free(ident);
  }

  return true;
}

void PT_writepath(B_Writer* writer, const PT_Path* path) {
  size_t i;
  PT_PathSegment* ps;

  if (!path->segs)
    U_die("tried to write NULL path");
  if (!PT_is_modstyle(path))
    U_die("can only serialize mod-style paths");

  B_write32(writer, path->has_leading_colon);
  B_write32(writer, U_len(path->segs));
  for (i = 0; i < U_len(path->segs); i++) {
    ps = &path->segs[i];
    B_writestr(writer, ps->ident);
  }
}
