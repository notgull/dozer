// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

#include "dozer.h"
#include "util/util.h"

#include <stdlib.h>
#include <string.h>

// TODO: This implementation is bogus! Every token in a source file is allocated
// at once. Figure out a better way of doing it that isn't rediculously
// complicated.

PTR_BUFFER_FUNCS(item, I_Item, I_NONE)

U_Arena* parsearena;

// Shared token buffer
struct tokens {
  // TODO: Don't allocate every token at once.
  size_t refcount;
  T_Token* tokens;
};

struct parser {
  struct tokens* tokens;
  size_t cursor;
};

P_Parser* P_mkparser(L_Lexer* lexer) {
  P_Parser* parser = U_xmalloc(sizeof(P_Parser));
  struct tokens* tokens = U_xmalloc(sizeof(struct tokens));
  T_Token token;

  tokens->refcount = 1;
  tokens->tokens = U_mkarray(NULL, 256, sizeof(*tokens->tokens));

  parser->tokens = tokens;
  parser->cursor = 0;

  do {
    L_lex(lexer, &token);
    U_addmem(&tokens->tokens, &token, 1);
  } while (token.kind != T_EOF);

  return parser;
}

void P_freeparser(P_Parser* parser) {
  T_Token* tokens = parser->tokens->tokens;

  if (parser->tokens->refcount--) {
    free(parser);
    return;
  }

  for (; tokens->kind != T_EOF; tokens++)
    if (tokens->literal)
      free(tokens->literal);

  free(parser->tokens);
  free(parser);
}

const T_Token* P_cur(const P_Parser* parser) {
  T_Token* tokens = parser->tokens->tokens;
  return &tokens[parser->cursor];
}

void P_next(P_Parser* parser) {
  if (P_cur(parser)->kind != T_EOF)
    parser->cursor++;
}

const T_Token* P_peek(const P_Parser* parser) {
  T_Token* tokens = parser->tokens->tokens;

  if (P_cur(parser)->kind != T_EOF)
    return &tokens[parser->cursor + 1];
  else
    return P_cur(parser);
}

const T_Token* P_peek2(const P_Parser* parser) {
  T_Token* tokens = parser->tokens->tokens;

  if (P_peek(parser)->kind != T_EOF)
    return &tokens[parser->cursor + 2];
  else
    return P_peek(parser);
}

bool P_consume(P_Parser* parser, T_TokenKind tk) {
  if (P_cur(parser)->kind != tk)
    return false;

  P_next(parser);
  return true;
}

char* P_expect(P_Parser* parser, T_TokenKind tk, const char* msg) {
  char* lit = P_cur(parser)->literal;
  if (!P_consume(parser, tk))
    T_error(&P_cur(parser)->loc, "%s: expected %s, found %s", msg, T_desc(tk),
            T_desc(P_cur(parser)->kind));

  return lit;
}

P_Parser* P_fork(P_Parser* parser) {
  P_Parser* forked = U_xmalloc(sizeof(*parser));
  forked->tokens = parser->tokens;
  forked->cursor = parser->cursor;
  forked->tokens->refcount++;
  return forked;
}

void P_unfork(P_Parser* src, P_Parser* forked) {
  if (forked->cursor < src->cursor)
    U_die("forked cursor behind source cursor");

  src->cursor = forked->cursor;
  P_freeparser(forked);
}

P_SourceFile P_readfile(const char* name) {
  P_SourceFile sf;
  P_Parser* parser;
  L_Lexer* lexer;
  I_Item** items;

  sf.filepath = name;
  sf.arena = U_mkarena();
  lexer = L_mklexer(name, ED_2021);
  parser = P_mkparser(lexer);
  items = U_pmkarray(1, sizeof(*items));

  while (P_cur(parser)->kind != T_EOF)
    *item_buffer_expand(&items) = I_read(parser);
  item_buffer_finish(&items);

  sf.items = items;
  P_freeparser(parser);
  L_freelexer(lexer);
  return sf;
}

void P_freefile(P_SourceFile sf) {
  U_freeall(sf.arena);
}
