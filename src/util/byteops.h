// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

#ifndef BYTEOPS_H
#define BYTEOPS_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef struct reader B_Reader;
typedef struct writer B_Writer;

struct reader {
  uint8_t* data;
  size_t len;
};

struct writer {
  uint8_t* data;
  size_t cap, len;
};

void B_mkreader(B_Reader* reader, uint8_t* data, size_t len);
void B_mkwriter(B_Writer* writer);
void B_freewriter(B_Writer* writer);

bool B_read(B_Reader* reader, void* buf, size_t len);
bool B_read32(B_Reader* reader, uint32_t* out);
bool B_read64(B_Reader* reader, uint64_t* out);
bool B_readstr(B_Reader* reader, char** out);

void B_write(B_Writer* writer, const void* buf, size_t len);
void B_write32(B_Writer* writer, uint32_t inp);
void B_write64(B_Writer* writer, uint64_t inp);
void B_writestr(B_Writer* writer, const char* str);

#endif
