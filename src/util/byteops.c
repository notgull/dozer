// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

// Byte operations on buffers.

#include "byteops.h"
#include "util/util.h"

#include <stdlib.h>
#include <string.h>

void B_mkreader(B_Reader* reader, uint8_t* data, size_t len) {
  reader->data = data;
  reader->len = len;
}

void B_mkwriter(B_Writer* writer) {
  writer->data = NULL;
  writer->cap = 0;
  writer->len = 0;
}

void B_freewriter(B_Writer* writer) {
  if (writer->data)
    free(writer->data);
}

bool B_read(B_Reader* reader, void* buf, size_t len) {
  if (reader->len < len)
    return false;

  memcpy(buf, reader->data, len);
  reader->data += len;
  reader->len -= len;

  return true;
}

#define READ_INT(size)                                       \
  bool B_read##size(B_Reader* reader, uint##size##_t* out) { \
    if (!B_read(reader, out, sizeof(*out)))                  \
      return false;                                          \
    if (U_bigendian())                                       \
      U_byteswap##size(out);                                 \
    return true;                                             \
  }

READ_INT(32)
READ_INT(64)

static uint64_t pad_to_4(uint32_t in) {
  return ((in + 3) & ~3) - in;
}

bool B_readstr(B_Reader* reader, char** out) {
  uint32_t len;
  uint8_t buffer[4];

  if (!B_read32(reader, &len))
    return false;
  *out = U_xmalloc(len + 1);
  if (!B_read(reader, *out, len))
    return false;
  (*out)[len] = '\0';

  // Pad to 4 bytes.
  if (!B_read(reader, buffer, pad_to_4(len)))
    return false;

  return true;
}

void B_write(B_Writer* writer, const void* buf, size_t len) {
  while (writer->cap - writer->len < len) {
    writer->cap = writer->cap ? writer->cap * 2 : 64;
    writer->data = U_xrealloc(writer->data, writer->cap);
  }

  memcpy(writer->data + writer->len, buf, len);
  writer->len += len;
}

#define WRITE_INT(size)                                      \
  void B_write##size(B_Writer* writer, uint##size##_t inp) { \
    if (U_bigendian())                                       \
      U_byteswap##size(&inp);                                \
    B_write(writer, &inp, sizeof(inp));                      \
  }

WRITE_INT(32)
WRITE_INT(64)

void B_writestr(B_Writer* writer, const char* str) {
  uint32_t len = strlen(str);
  uint8_t buffer[4] = {0};

  B_write32(writer, len);
  B_write(writer, str, len);
  B_write(writer, buffer, pad_to_4(len));
}
