// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

#include "util.h"

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARENA_LEN 128
#define ARRAY_MAGIC 0xDEADBEEF

char* program_name;

typedef struct array array;

struct arena {
  void** pool;
  size_t cursor;
};

struct array {
  uint64_t magic;
  U_Arena* arena;
  size_t len, cap, item_size;
  union {
    long long ll;
    long double ld;
    void* ptr;
  } align[];
};

char* U_parse_program_name(char* name) {
  char* slash;

  if (!name)
    return NULL;

  // cut out the folder name
  slash = strrchr(name, '/');
  return slash ? slash + 1 : name;
}

static void vwarn(const char* fmt, va_list ap) {
  fprintf(stderr, "%s: ", program_name);
  vfprintf(stderr, fmt, ap);

  // print errno message if applicable
  if (fmt[0] && fmt[strlen(fmt) - 1] == ':') {
    fputc(' ', stderr);
    perror(NULL);
  } else {
    fputc('\n', stderr);
  }
}

void U_warn(const char* fmt, ...) {
  va_list ap;

  va_start(ap, fmt);
  vwarn(fmt, ap);
  va_end(ap);
}

void U_die(const char* fmt, ...) {
  va_list ap;

  va_start(ap, fmt);
  vwarn(fmt, ap);
  va_end(ap);
  exit(1);
}

void* U_xmalloc(size_t len) {
  void* buffer;

  buffer = malloc(len);
  if (!buffer)
    U_die("malloc:");

  return buffer;
}

void U_xfree(void* buf) {
  if (buf)
    free(buf);
}

void* U_xrealloc(void* buf, size_t len) {
  void* buffer;

  buffer = realloc(buf, len);
  if (!buffer)
    U_die("realloc:");

  return buffer;
}

void U_indent(FILE* f, int indent) {
  int i;

  for (i = 0; i < indent; i++)
    fputs("  ", f);
}

U_Arena* U_mkarena() {
  U_Arena* arena;

  arena = U_xmalloc(sizeof(U_Arena));
  arena->pool = U_xmalloc(ARENA_LEN * sizeof(void*));
  memset(arena->pool, 0, ARENA_LEN * sizeof(void*));
  arena->cursor = 1;

  return arena;
}

void* U_aalloc(U_Arena* arena, size_t len) {
  void *ptr, **pool;

  if (len == 0)
    return 0;
  if (arena->cursor >= ARENA_LEN) {
    // Time to increase the capacity of the arena.
    pool = U_xmalloc(ARENA_LEN * sizeof(void*));
    memset(pool, 0, ARENA_LEN * sizeof(void*));
    pool[0] = arena->pool;
    arena->pool = pool;
    arena->cursor = 1;
  }

  ptr = U_xmalloc(len);
  arena->pool[arena->cursor++] = ptr;
  return ptr;
}

void U_freeall(U_Arena* arena) {
  void** pool;

  while (1) {
    for (pool = &arena->pool[1]; pool < &arena->pool[arena->cursor]; pool++)
      U_xfree(*pool);

    // Recurse to the last set of pointers.
    pool = arena->pool[0];
    if (!pool)
      break;

    U_xfree(arena->pool);
    arena->pool = pool;
    arena->cursor = ARENA_LEN;
  }

  U_xfree(arena);
}

// Alloc on the heap instead of an arena.
static void* heap_alloc(U_Arena* arena, size_t len) {
  (void)arena;
  return U_xmalloc(len);
}

static array* mkarray(U_Arena* arena,
                      size_t len,
                      size_t cap,
                      size_t item_size) {
  void* (*alloc)(U_Arena*, size_t);
  array* arr;
  size_t real_cap;

  assert(cap >= len);

  alloc = arena ? U_aalloc : heap_alloc;
  real_cap = 2;
  while (real_cap < cap)
    real_cap *= 2;

  arr = alloc(arena, sizeof(array) + (real_cap * item_size));
  arr->magic = ARRAY_MAGIC;
  arr->arena = arena;
  arr->len = len;
  arr->cap = real_cap;
  arr->item_size = item_size;

  return arr;
}

void* U_mkarray(U_Arena* arena, size_t cap, size_t item_size) {
  array* arr;

  arr = mkarray(arena, 0, cap, item_size);
  return arr + 1;
}

size_t U_len(const void* ptr) {
  const array* arr;

  arr = (const array*)ptr - 1;
  assert(arr->magic == ARRAY_MAGIC);
  return arr->len;
}

void U_freearray(void* ptr) {
  array* arr;

  arr = (array*)ptr - 1;
  assert(arr->magic == ARRAY_MAGIC);
  if (!arr->arena) {
    arr->magic = 0;
    U_xfree(arr);
  }
}

void* U_expand(void* ptr, size_t n) {
  array *arr, *new_arr;
  void* result;

  arr = *(array**)ptr - 1;
  assert(arr + 1 && arr->magic == ARRAY_MAGIC);

  if (arr->cap - arr->len < n) {
    do
      arr->cap *= 2;
    while (arr->cap - arr->len < n);

    new_arr = mkarray(arr->arena, arr->len, arr->cap, arr->item_size);
    memcpy(new_arr + 1, arr + 1, arr->len * arr->item_size);
    U_freearray(arr + 1);
    *(array**)ptr = new_arr + 1;
    arr = new_arr;
  }

  // HACK: fails to compile on cproc, you can't add to a void* apparently.
  result = (void*)((uint8_t*)(arr + 1) + (arr->len * arr->item_size));
  arr->len += n;
  return result;
}

void U_contract(void* ptr, size_t n) {
  array* arr;

  arr = *(array**)ptr - 1;
  assert(arr + 1 && arr->magic == ARRAY_MAGIC);

  arr->len -= n;
}

void U_addbyte(void* array, uint8_t by) {
  uint8_t* slot = U_expand(array, 1);
  *slot = by;
}

void* U_addmem(void* ptr, void* buf, size_t len) {
  array* arr;
  void* slot;
  size_t item_size;

  arr = *(array**)ptr - 1;
  assert(arr + 1 && arr->magic == ARRAY_MAGIC);
  item_size = arr->item_size;

  slot = U_expand(ptr, len);
  memcpy(slot, buf, len * item_size);
  return slot;
}

void** U_addptr(void* ptr, void* data) {
  array* arr;
  void** slot;

  arr = *(array**)ptr - 1;
  assert(arr + 1 && arr->magic == ARRAY_MAGIC &&
         arr->item_size == sizeof(void*));

  slot = U_expand(ptr, 1);
  *slot = data;
  return slot;
}

void U_mksbuilder(struct stringbuilder* sb) {
  sb->buffer = sb->cursor = NULL;
  sb->remaining = 0;
}

void U_pushstr(struct stringbuilder* sb, const char* src) {
  size_t srclen = strlen(src);

  if (!sb->cursor) {
    // Not ready yet.
    sb->remaining += srclen;
    return;
  }

  // We are ready, push it in.
  if (srclen > sb->remaining)
    U_die("tried to push string of length %u into buffer of size %u", srclen,
          sb->remaining);

  memcpy(sb->cursor, src, srclen);
  sb->remaining -= srclen;
  sb->cursor += srclen;
}

// Meant to be used in the clause of a for loop.
bool U_strready(U_StringBuilder* sb) {
  if (sb->cursor)
    return false;
  if (sb->remaining == 0)
    return true;

  sb->buffer = sb->cursor = U_xmalloc(sb->remaining + 1);
  return true;
}

char* U_finishstr(struct stringbuilder* sb) {
  *sb->cursor = '\0';
  return sb->buffer;
}

bool U_bigendian() {
  int n = 1;
  return *(char*)&n != 1;
}

void U_byteswap32(uint32_t* slot) {
  uint32_t x = *slot;
  *slot = (x >> 24) | ((x << 16) & 0x00FF0000) | ((x >> 16) & 0x0000FF00) |
          (x << 24);
}

void U_byteswap64(uint64_t* slot) {
  uint64_t x = *slot;
  *slot = (x >> 56) | ((x << 40) & 0x00FF000000000000) |
          ((x << 24) & 0x0000FF0000000000) | ((x << 8) & 0x000000FF00000000) |
          ((x >> 8) & 0x00000000FF000000) | ((x >> 24) & 0x0000000000FF0000) |
          ((x >> 40) & 0x000000000000FF00) | (x << 56);
}

const char* U_compile_intlit(const char* lit, uint64_t* value) {
  char* suffix = "";

  if (strncmp(lit, "0x", 2) == 0)
    *value = strtoull(lit + 2, &suffix, 16);
  else if (strncmp(lit, "0o", 2) == 0)
    *value = strtoull(lit + 2, &suffix, 8);
  else if (strncmp(lit, "0b", 2) == 0)
    *value = strtoull(lit + 2, &suffix, 2);
  else
    *value = strtoull(lit, &suffix, 10);

  return suffix;
}

const char* U_compile_floatlit(const char* lit, uint64_t* value) {
  char* suffix = "";

  union {
    uint64_t u64;
    double f64;
  } x;
  x.f64 = strtod(lit, &suffix);
  *value = x.u64;

  return suffix;
}

static uint8_t hex_digit_parse(char c) {
  uint8_t res;
  if (c >= 'a' && c <= 'f')
    res = c - 'a' + 0xa;
  else if (c >= 'A' && c <= 'F')
    res = c - 'A' + 0xA;
  else if (c >= '0' && c <= '9')
    res = c - '0';
  else
    U_die("invalid hex digit: %c", c);
  return res;
}

size_t U_decode_char(const uint8_t* str, uint32_t* value) {
  const uint8_t* s = str;
  uint32_t res = 0;

  if (*s == '\\') {
    s++;  // skip '\'
    switch (*s) {
      case '\'':
      case '\"':
      case '\\':
        res = *s++;
        break;
      case 'n':
        s++;
        res = '\n';
        break;
      case 'r':
        s++;
        res = '\r';
        break;
      case 't':
        s++;
        res = '\t';
        break;
      case '0':
        s++;
        res = 0;
        break;
      case 'x':
        s++;  // skip 'x'
        res = hex_digit_parse(*s++);
        res = res * 16 + hex_digit_parse(*s++);
        break;
      case 'u':
        s++;  // skip 'u'
        s++;  // first '{'
        while (*s != '}')
          if (*s == '_') {
            s++;
            continue;
          } else
            res = res * 16 + hex_digit_parse(*s++);
        s++;  // last '}'
        break;
      case '\n':
        while (*s == '\t' || *s == '\n' || *s == '\r' || *s == ' ')
          s++;
        res = UINT32_MAX;  // string continue
    }
  } else                                 // utf-8 handling
    if (*s < 0x80)                       // 0x00000000 - 0x0000007F:
      res = *s++;                        //    0xxxxxxx
    else if ((*s & 0xE0) == 0xC0) {      // 0x00000080 - 0x000007FF:
      res = *s++ & 0x1F;                 //    110xxxxx
      res = (res << 6) + (*s++ & 0x3F);  //    10xxxxxx
    } else if ((*s & 0xF0) == 0xE0) {    // 0x00000800 - 0x0000FFFF:
      res = *s++ & 0x0F;                 //    1110xxxx
      res = (res << 6) + (*s++ & 0x3F);  //    10xxxxxx
      res = (res << 6) + (*s++ & 0x3F);  //    10xxxxxx
    } else if ((*s & 0xF8) == 0xF0) {    // 0x00010000 - 0x001FFFFF:
      res = *s++ & 0x07;                 //    11110xxx
      res = (res << 6) + (*s++ & 0x3F);  //    10xxxxxx
      res = (res << 6) + (*s++ & 0x3F);  //    10xxxxxx
      res = (res << 6) + (*s++ & 0x3F);  //    10xxxxxx
    }

  *value = res;
  return (size_t)s - (size_t)str;
}

size_t U_encode_char_utf8(uint32_t chr, uint8_t buf[4]) {
  uint8_t* out = buf;

  if (chr < 0x80) {
    *out++ = chr;
  } else if (chr < 0x800) {
    *out++ = 0xC0 | (chr >> 6);
    *out++ = 0x80 | (chr & 0x3F);
  } else if (chr < 0x10000) {
    *out++ = 0xE0 | (chr >> 12);
    *out++ = 0x80 | ((chr >> 6) & 0x3F);
    *out++ = 0x80 | (chr & 0x3F);
  } else if (chr < 0x200000) {
    *out++ = 0xF0 | (chr >> 18);
    *out++ = 0x80 | ((chr >> 12) & 0x3F);
    *out++ = 0x80 | ((chr >> 6) & 0x3F);
    *out++ = 0x80 | (chr & 0x3F);
  }

  return (size_t)out - (size_t)buf;
}
