// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

// This code is mostly taken from cproc's map implementation, licensed under
// ISC.

#include "util/util.h"

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

static uint64_t hash(const void* str, size_t len) {
  uint64_t h;
  const char *pos, *end;

  // Implements the FNV algorithm.
  h = 0x811C9DC5;
  for (pos = str, end = pos + len; pos != end; pos++)
    h = (h ^ *pos) * 0x1000193;

  return h;
}

void M_initkey(M_Key* key, const void* str, size_t len) {
  key->str = str;
  key->len = len;
  key->hash = hash(str, len);
}

void M_init(M_Map* map, size_t cap) {
  size_t i;

  if (cap & (cap - 1))
    U_die("cap for map must be power of two");

  map->len = 0;
  map->cap = cap;
  map->keys = U_xmalloc(cap * sizeof(M_Key));
  map->values = U_xmalloc(cap * sizeof(void*));

  for (i = 0; i < cap; i++)
    map->keys[i].str = NULL;
}

void M_free(M_Map* map, void del(void*)) {
  size_t i;

  if (del) {
    for (i = 0; i < map->cap; i++)
      if (map->keys[i].str)
        del(map->values[i]);
  }

  free(map->keys);
  free(map->values);
}

static bool key_equal(const M_Key* left, const M_Key* right) {
  if (left->hash != right->hash || left->len != right->len)
    return false;

  return memcmp(left->str, right->str, left->len) == 0;
}

static size_t key_index(const M_Map* map, const M_Key* key) {
  size_t i;

  i = key->hash & map->cap - 1;
  while (map->keys[i].str && !key_equal(&map->keys[i], key))
    i = i + 1 & map->cap - 1;

  return i;
}

void** M_insert(M_Map* map, const M_Key* key) {
  M_Key* old_keys;
  void** old_values;
  size_t i, j, old_cap;

  if (map->cap / 2 < map->len) {
    old_keys = map->keys;
    old_values = map->values;
    old_cap = map->cap;

    map->cap *= 2;
    map->keys = U_xmalloc(map->cap * sizeof(M_Key));
    map->values = U_xmalloc(map->cap * sizeof(void*));

    for (i = 0; i < map->cap; i++)
      map->keys[i].str = NULL;
    for (i = 0; i < old_cap; i++)
      if (old_keys[i].str) {
        j = key_index(map, &old_keys[i]);
        map->keys[j] = old_keys[i];
        map->values[j] = old_values[i];
      }

    free(old_keys);
    free(old_values);
  }

  i = key_index(map, key);
  if (!map->keys[i].str) {
    map->keys[i] = *key;
    map->values[i] = NULL;
    map->len += 1;
  }

  return &map->values[i];
}

void* M_get(const M_Map* map, const M_Key* key) {
  size_t i;

  i = key_index(map, key);
  return map->keys[i].str ? map->values[i] : NULL;
}
