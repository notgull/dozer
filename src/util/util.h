// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

#ifndef DOZER_UTIL_H
#define DOZER_UTIL_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

//>> error handling (util.c)

// program name emitted on error
extern char* program_name;

// parse the program name
char* U_parse_program_name(char* name);

// die with a fatal error
void U_die(const char* fmt, ...);
void U_warn(const char* fmt, ...);

//>> alloc functions (util.c)

// allocate memory and bail out if we fail
void* U_xmalloc(size_t len);
void U_xfree(void* buf);

// reallocate memory and bail out if we fail
void* U_xrealloc(void* buf, size_t len);

void U_indent(FILE* f, int indent);

//>> endianness

bool U_bigendian();
void U_byteswap32(uint32_t* x);
void U_byteswap64(uint64_t* x);

//>> arena allocation
//
// Arenas keep track of all allocations made in their name and then frees them
// all at once once "U_freeall" is called.

typedef struct arena U_Arena;

U_Arena* U_mkarena();
void* U_aalloc(U_Arena* arena, size_t len);
void U_freeall(U_Arena* arena);

//>> flexible arrays
//
// These are usually created as "T*", where "T" is the type contained in the
// array. This has the advantage that usual C indexing works properly without
// much fuss. There is a header for the array stored before the start of the
// "T" items, so it needs to be freed with U_freearray.
//
// (or, if you pass in an arena, just wait for the arena to free it)

void* U_mkarray(U_Arena* arena, size_t cap, size_t item_size);
size_t U_len(const void* array);
void* U_expand(void* array, size_t n);
void U_contract(void* array, size_t n);
void U_addbyte(void* array, uint8_t by);
// Points to the new region of memory.
void* U_addmem(void* array, void* buf, size_t len);
void** U_addptr(void* array, void* ptr);
void U_freearray(void* array);

#define PTR_BUFFER_FUNCS(prefix, ty, none)                    \
  static ty** prefix##_buffer_expand(ty*** buffer) {          \
    ty** slot = (ty**)U_addptr(buffer, NULL);                 \
    return slot;                                              \
  }                                                           \
  static void prefix##_buffer_finish(ty*** buffer) {          \
    ty** slot = (ty**)U_addptr(buffer, U_palloc(sizeof(ty))); \
    (*slot)->kind = none;                                     \
  }

//>> UTF-8 decoding

// Returns the suffix.
const char* U_compile_intlit(const char* lit, uint64_t* value);
const char* U_compile_floatlit(const char* lit, uint64_t* value);

// Returns the size in bytes of the decoded (possibly escaped) character.
// if no character was decoded (for instance, with a string continuation),
// *value will contain UINT32_MAX.
size_t U_decode_char(const uint8_t* str, uint32_t* value);
// Returns the size in bytes written to the buffer (at most 4)
size_t U_encode_char_utf8(uint32_t chr, uint8_t* buf);

//>> string builders

typedef struct stringbuilder U_StringBuilder;

struct stringbuilder {
  char *buffer, *cursor;
  size_t remaining;
};

void U_mksbuilder(U_StringBuilder* sb);
// Before ready: just expands the capacity
// After ready: actually pushes the string.
void U_pushstr(U_StringBuilder* sb, const char* src);
bool U_strready(U_StringBuilder* sb);
char* U_finishstr(U_StringBuilder* sb);

//>> maps (map.c)

typedef struct map M_Map;
typedef struct mapkey M_Key;

struct map {
  size_t len, cap;
  struct mapkey* keys;
  void** values;
};

struct mapkey {
  uint64_t hash;
  const void* str;
  size_t len;
};

void M_initkey(M_Key* key, const void* str, size_t len);
void M_init(M_Map* map, size_t cap);
void M_free(M_Map* map, void del(void*));

void** M_insert(M_Map* map, const M_Key* key);
void* M_get(const M_Map* map, const M_Key* key);

#endif
