// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

/*

Dozer Intermediate Representation (DIR), a simple bytecode for representing
partially compiled Rust code. It has the following design goals:

- Simple enough to be parsed without any complex parser code.
- Can be interpreted in a bytecode VM.
- Can be trivially compiled into QBE IR.

TODO: Describe the rest

*/

#ifndef DOZER_DIR_H
#define DOZER_DIR_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "util/byteops.h"

#define DI_MAX_ARGS 4

typedef struct bytecode DI_Code;
typedef enum bytecode_opcode DI_Opcode;
typedef uint32_t DI_Register;

enum bytecode_opcode {
  DI_NOP = 0,

  // Load literal types.
  // One argument is the 64-bit literal.
  DI_LOAD,

  // Binary op.
  // First two arguments are input registers, next is output register, next is
  // 32-bit binop.
  DI_BINOP,

  // Force a register to exist on the stack.
  // This is needed before a ref can be taken to it.
  // Some registers are forced on the stack anyways (e.g. structs that don't fit
  // in a register) First argument is the register.
  DI_MAKESTACK,

  // Load a global.
  // First argument is the name of the global.
  DI_GLOBAL,

  // Return the provided register from the current function.
  DI_RET,
  DI_RETNONE,

  // Call a provided function.
  DI_CALL,
  DI_CALLRET
};

struct bytecode {
  DI_Opcode opcode;
  union {
    DI_Register reg;
    int optype;
    uint64_t literal;
    char* name;
    struct {
      DI_Register* items;
      uint32_t len;
    } regs;
  } args[DI_MAX_ARGS];
};

// Read from the data, returns false if we are EOF
bool DI_readcode(B_Reader* reader, DI_Code* code);
// Write to the data, assumes it is malloc'd and realloc's if out of room.
void DI_writecode(B_Writer* writer, DI_Code code);
// Free an opcode.
void DI_free(DI_Code code);

#endif
