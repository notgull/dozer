// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

#include "dir.h"
#include "util/util.h"

#include <stdlib.h>

bool DI_readcode(B_Reader* reader, DI_Code* code) {
  uint32_t kind, optype, reg, i, *regs;

  if (!B_read32(reader, &kind))
    return false;

  switch (kind) {
    case DI_LOAD:
      if (!B_read32(reader, &code->args[0].reg))
        return false;
      if (!B_read64(reader, &code->args[1].literal))
        return false;
      break;
    case DI_RET:
    case DI_MAKESTACK:
      if (!B_read32(reader, &code->args[0].reg))
        return false;
      break;
    case DI_BINOP:
      if (!B_read32(reader, &code->args[0].reg))
        return false;
      if (!B_read32(reader, &code->args[1].reg))
        return false;
      if (!B_read32(reader, &code->args[2].reg))
        return false;
      if (!B_read32(reader, &optype))
        return false;
      code->args[3].optype = (int)optype;
      break;
    case DI_GLOBAL:
      if (!B_read32(reader, &code->args[0].reg))
        return false;
      if (!B_readstr(reader, &code->args[1].name))
        return false;
      break;
    case DI_CALL:
    case DI_CALLRET:
      if (!B_read32(reader, &code->args[0].reg))
        return false;
      if (!B_read32(reader, &code->args[1].regs.len))
        return false;

      // Read list of arguments (registers).
      regs = U_mkarray(NULL, code->args[1].regs.len, sizeof(*regs));
      for (i = 0; i < code->args[1].regs.len; i++) {
        if (!B_read32(reader, &reg))
          return false;
        U_addmem(&regs, &reg, 1);
      }
      code->args[1].regs.items = regs;

      if (!B_read32(reader, &code->args[2].reg))
        return false;

      break;
    case DI_RETNONE:
      break;
    default:
      fprintf(stderr, "invalid DIR kind %d\n", kind);
      return false;
  }

  code->opcode = kind;
  return true;
}

void DI_writecode(B_Writer* writer, DI_Code code) {
  uint32_t i;

  B_write32(writer, code.opcode);
  switch (code.opcode) {
    case DI_LOAD:
      B_write32(writer, code.args[0].reg);
      B_write64(writer, code.args[1].literal);
      break;
    case DI_RET:
    case DI_MAKESTACK:
      B_write32(writer, code.args[0].reg);
      break;
    case DI_BINOP:
      B_write32(writer, code.args[0].reg);
      B_write32(writer, code.args[1].reg);
      B_write32(writer, code.args[2].reg);
      B_write32(writer, (uint32_t)code.args[3].optype);
      break;
    case DI_GLOBAL:
      B_write32(writer, code.args[0].reg);
      B_writestr(writer, code.args[1].name);
      break;
    case DI_CALL:
    case DI_CALLRET:
      B_write32(writer, code.args[0].reg);
      B_write32(writer, code.args[1].regs.len);

      for (i = 0; i < code.args[1].regs.len; i++)
        B_write32(writer, code.args[1].regs.items[i]);

      B_write32(writer, code.args[2].reg);

      break;
    case DI_RETNONE:
      break;
  }
}

void DI_free(DI_Code code) {
  switch (code.opcode) {
    case DI_GLOBAL:
      free(code.args[1].name);
      break;
    case DI_CALL:
    case DI_CALLRET:
      U_freearray(code.args[1].regs.items);
      break;
  }
}
