// This file is part of Dozer.
//
// Dozer is free software; you can redistribute it and/or modify it
// under the terms of one of the following licenses:
//
// - The MIT License
// - The Apache License, version 2.0
//
// Dozer is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the MIT License and the Apache
// License for more details.
//
// You should have received a copy of the MIT License and the Apache
// License 2.0 along with Dozer. If not, see:
//
// - https://opensource.org/license/mit
// - https://www.apache.org/licenses/LICENSE-2.0

#include "dozer.h"
#include "json/json_dump.h"
#include "nhad/nhad.h"
#include "util/util.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static char* input_file = NULL;
static char* output_file = NULL;

char* dozer_cratename = "darkstar";

static struct {
  const char* flag;
  char** value;
} flags[] = {{.flag = "crate-name", .value = &dozer_cratename}};

static void usage() {
  fprintf(stderr, "usage: %s [filename] [outfile] [--crate-name=NAME]\n",
          program_name);
  exit(1);
}

static void parse_args(int argc, char** argv) {
  int index = 0, i;
  bool found;

  while (--argc && ++argv) {
    if (strncmp("--", *argv, strlen("--")) == 0) {
      // This is a flag.
      *argv += strlen("--");
      found = false;
      for (i = 0; i < sizeof(flags) / sizeof(flags[0]); i++)
        if (strncmp(flags[i].flag, *argv, strlen(flags[i].flag)) == 0) {
          // Extract flag.
          found = true;
          *flags[i].value = strchr(*argv, '=');
          if (!*flags[i].value) {
            if (!--argc)
              U_die("flag without value");
            *flags[i].value = *++argv;
          } else
            *flags[i].value += 1;
        }

      if (!found)
        U_die("invalid flag: %s", *argv);
      continue;
    }

    if (index == 0)
      input_file = *argv;
    else if (index == 1)
      output_file = *argv;

    index++;
  }

  if (!input_file || !output_file)
    usage();
}

static void save_crate_symtab(N_Nhad* output) {
  B_Writer writer;

  B_mkwriter(&writer);
  G_writesyms(&writer);
  N_push(output, "SYMTAB", writer.data, writer.len);

  if (writer.data)
    free(writer.data);
}

#if 1
int main(int argc, char** argv) {
  P_SourceFile sf;
  N_Nhad* output;

  program_name = U_parse_program_name(argv[0]);
  if (!program_name)
    program_name = "dozer";

  parse_args(argc, argv);

  // Open a file and parse it.
  parsearena = U_mkarena();
  sf = P_readfile(input_file);

  if (argc > 2 && strcmp(output_file, "parse") == 0)
    JSON_dump(stdout, &sf);
  else {
    // Initialize global symbol table.
    G_init(true);
    G_populate(&sf);

    // Resolve all links in the exprs.
    R_resolve(&sf);

    output = N_new();
    EM_compile(output, &sf);

    // Add metadata lumps.
    N_push(output, "CRATENAME", dozer_cratename, strlen(dozer_cratename));
    save_crate_symtab(output);

    // All items compiled, write out to file.
    N_write(output, output_file);
    G_free();
  }

  U_freeall(parsearena);

  // TODO
  return 0;
}
#endif
